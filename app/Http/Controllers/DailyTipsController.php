<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\DailyTip;
use App\Model\DailytipType;
use App\Model\Operation;
use Validator;

class DailyTipsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $count_with_result_and_winloose = DailyTip::whereNull('result')->whereNull('result')->count();
        //$count_with_result_and_winloose = 0;
        $is_able_to_export = $count_with_result_and_winloose == 0 ? true : false;
        $dailytips = DailyTip::orderBy('created_at','DESC')->paginate(10);
        return View('dailytips.index')
                ->with('dailytips',$dailytips)
                ->with('menu','dailytips')
                ->with('is_able_to_export',$is_able_to_export);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dailytips_types = DailytipType::get();
        return View('dailytips.create')->with('types',$dailytips_types);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $validation = Validator::make($request->all(),[
        //     'horse' => 'required',
        //     'run' => 'required',
        //     'due_date' => 'required'
        // ]);
        $validation = Validator::make($request->all(),[
            'resume' => 'required',
            'dailytips_types_id' => 'required'
        ]);

        if($validation->fails()){
            return redirect()->back()->with('errors' ,$validation->errors()->all());
        }
        else{
            $dailytip = new DailyTip;

            // $dailytip->horse = $request->input('horse');
            // $dailytip->run = $request->input('run');
             $dailytip->dailytips_types_id = $request->has('dailytips_types_id')? $request->input('dailytips_types_id') : null;
            // $dailytip->due_date = $request->input('due_date');
            // $dailytip->odd = $request->input('odd');
            $dailytip->resume = $request->input('resume');
            //$dailytip->result = $request->has('result')? $request->input('result') : '';
            if($request->has('cdate')){
                //dd($request->input('cdate'));
                $dailytip->created_at = $request->input('cdate');
            }
            
    
            $dailytip->save();
    
            return redirect()->route('dailytips.index');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dailytip = DailyTip::find($id);

        return View('dailytips.edit')->with('dailytip',$dailytip);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dailytip = DailyTip::find($id);

        $dailytip->horse = $request->has('horse')? $request->input('horse') : $dailytip->horse;
        $dailytip->run = $request->has('run')? $request->input('run') : $dailytip->run;
        $dailytip->result = $request->has('result')? $request->input('result') : $dailytip->result;
        $dailytip->win_lose = $request->has('win_lose')? $request->input('win_lose') : $dailytip->win_lose;
        $dailytip->due_date = $request->has('due_date')? $request->input('due_date') : $dailytip->due_date;
        $dailytip->odd = $request->has('odd')? $request->input('odd') : $dailytip->odd;


        $dailytip->save();

        return redirect()->route('dailytips.index')->with('success','Tip atualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DailyTip::destroy($id);

        return redirect()->route('dailytips.index')->with('success','Tip removida');
    }

    /**
     * Export dailytips do operations
     */
    public function export(){
        $dailytips = DailyTip::all();
        $dailytips_count = DailyTip::count();

        for($i=0; $i < $dailytips_count; $i++){
            $dailytip = $dailytips[$i];

            $operation = new Operation;
            $operation->result = $dailytip->resume;
            // $operation->horse = $dailytip->horse;
            // $operation->run = $dailytip->run;
            $operation->result = $dailytip->result;
            // $operation->due_date = $dailytip->due_date;
            // $operation->odd = $dailytip->odd;

            $operation->win_lose = $dailytip->win_lose;

            $operation->save();
        }

        return redirect()->route('dailytips.index')->with('success','Dailytips exportadas');
        //DailyTip::delete();
    }

    public function update_api(Request $request, $id){
        $dailytip = DailyTip::find($id);

        $dailytip->horse = $request->has('horse')? $request->input('horse') : $dailytip->horse;
        $dailytip->run = $request->has('run')? $request->input('run') : $dailytip->run;
        $dailytip->result = $request->has('result')? $request->input('result') : $dailytip->result;
        $dailytip->win_lose = $request->has('win_lose')? $request->input('win_lose') : $dailytip->win_lose;
        $dailytip->due_date = $request->has('due_date')? $request->input('due_date') : $dailytip->due_date;
        $dailytip->odd = $request->has('odd')? $request->input('odd') : $dailytip->odd;


        $dailytip->save();

        return response()->json($dailytip);
    }


}
