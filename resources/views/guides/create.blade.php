@extends('layout.panel')

@section('content')
<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('error'))
          <div class="alert danger">
            <b>{{Session::get('error')}}</b>
          </div>
        @endif
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h2>Nova Guia de Pista</h2>
          </div>
          <div class="box-divider m-0"></div>
          <div class="box-body">
            <form action="{{route('racingtracksguides.store')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
              <div class="form-group">
                <label>Nome</label>
                <input type="text" class="form-control" name="title" id="title" placeholder="Belém">
              </div>

              <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                      <label>TimeForm</label>
                      <textarea class="form-control" name="timeform" rows="5"></textarea>
                    </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-3">
                    <div class="form-group">
                      <label>Imagem</label>
                      <div class="form-file">
                        <input type="hidden" name="storage_file">
                        <input type="file" name="img"> <button class="btn white">Select file ...</button>
                      </div>
                    </div>
                </div>
                <div class="col-lg-2">
                  <img class="img-fluid img-thumbnail" id="preview_img">
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <label>Clique no mapa para definir o local da pista</label>
                  <input type="hidden" name="latitude">
                  <input type="hidden" name="longitude">
                  <div id="map" style="width: 100%; height: 320px"></div>
                </div>
              </div>
              
              <button type="submit" class="btn primary" disabled>Salvar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
<script src="https://www.gstatic.com/firebasejs/4.10.1/firebase.js"></script>

<script type="text/javascript">
// Set the configuration for your app
  $(document).ready(function(){
    var config = {
      apiKey: "AIzaSyCpEP6yWsE1fv8vnKfd-ELoAdB_mFJctHQ",
      authDomain: "academy-jockeybit.firebaseapp.com",
      databaseURL: "https://<DATABASE_NAME>.firebaseio.com",
      storageBucket: "gs://academy-jockeybit.appspot.com/",
      messagingSenderId: "<SENDER_ID>",
    };
    firebase.initializeApp(config);

    // Get a reference to the storage service, which is used to create references in your storage bucket
    var storage = firebase.storage().ref();
    var timestamp = Number(new Date());
    var pdfsRef = storage.child('imgs/' + timestamp + ".png");

    $('input[name=img]').change(function(){
      var file_data = $('input[name=img]').prop('files')[0];
      pdfsRef.put(file_data).then(function(snapshot) {
        console.log('Uploaded a blob or file!');
        // console.log(snapshot);
        $('input[name=storage_file]').val(snapshot.downloadURL);
        $('button[type=submit]').removeAttr('disabled');
        $('#preview_img').attr('src', snapshot.downloadURL);
      });
    });

  });
  
</script>

<script src="http://js.api.here.com/v3/3.0/mapsjs-core.js" type="text/javascript" charset="utf-8"></script>
<script src="http://js.api.here.com/v3/3.0/mapsjs-service.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-ui.js"></script>
<script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-mapevents.js"></script>

<script>
  var marker = null;
  var userPosition;
  var map;
  var mapBehavior;

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } else {
    x.innerHTML = "Geolocation is not supported by this browser.";
  }

  function showPosition(position) {
    userPosition = {lat: position.coords.latitude , lng: position.coords.longitude}
    // x.innerHTML = "Latitude: " + position.coords.latitude + 
    // "<br>Longitude: " + position.coords.longitude; 
    setUpMap();
  }

  function setUpClickListener(map) {
    // Attach an event listener to map display
    // obtain the coordinates and display in an alert box.
    map.addEventListener('tap', function (evt) {
      var coord = map.screenToGeo(evt.currentPointer.viewportX, evt.currentPointer.viewportY);
      // var lat = Math.abs(coord.lat.toFixed(4));
      // var lng = Math.abs(coord.lng.toFixed(4));
      var lat = coord.lat;
      var lng = coord.lng;

      console.log("lat: " + lat + ", lng:" + lng);

      $('input[name=latitude]').val(lat);
      $('input[name=longitude]').val(lng);
      
      if(marker==null){
        console.log('marker null');
        marker = new H.map.Marker({lat: lat, lng : lng});
        map.addObject(marker);
      }
      else{
        console.log('marker not null');
        marker.setPosition({lat:lat, lng:lng});
      }
    });
  }

  function setUpMap(){
    var platform = new H.service.Platform({
      'app_id': 'XnhcfLIBJ5g36y2z28p0',
      'app_code': 'KthYwbchlSwxaRW-6ezVXQ'
    });
    // Obtain the default map types from the platform object:
    var defaultLayers = platform.createDefaultLayers();

    // Instantiate (and display) a map object:
    map = new H.Map(
      document.getElementById('map'),
      defaultLayers.normal.map,
      {
        zoom: 12,
        center: userPosition
      });
      //Step 3: make the map interactive
      // MapEvents enables the event system
      // Behavior implements default interactions for pan/zoom (also on mobile touch environments)
    mapBehavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

      setUpClickListener(map);
  }


</script>
@endsection
