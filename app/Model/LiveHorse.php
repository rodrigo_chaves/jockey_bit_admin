<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LiveHorse extends Model
{
    protected $table = 'live_horses';
}
