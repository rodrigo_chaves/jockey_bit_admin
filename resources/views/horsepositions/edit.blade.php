@extends('layout.panel')

@section('content')
<div class="content-main " id="content-main">
  <div class="padding">
    @if(Session::has('error'))
      <div class="row">
        <div class="col-lg-12">
          <div class="alert alert-danger">{{Session::get('error')}}</div>
        </div>
      </div>
    @endif
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h2>Novo Horse Position</h2>
          </div>
          <div class="box-divider m-0"></div>
          <div class="box-body">
            <form action="{{route('horsepositions.update',['id' => $horse_position->id])}}" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="put">
              <div class="row">
                <div class="col-lg-3">
                  <div class="form-group">
                    <label>Data</label>
                    <input type="date" class="form-control" name="date" id="date" placeholder="2018-03-12" value="{{$horse_position->date}}">
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="form-group">
                    <label>Hora</label>
                    <input type="text" class="form-control" name="time" id="time" placeholder="00:00" value="{{$horse_position->time}}">
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-2">
                  <div class="form-group">
                    <label>Pista</label>
                    <select class="form-control" name="racing_track_id">
                      @foreach($tracks as $track)
                        <option value="{{$track->id}}" {{($horse_position->racing_track_id == $track->id)? 'selected' : ''}}>{{$track->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="col-lg-2">
                    <div class="form-group">
                      <label>Cavalo</label>
                      <select class="form-control" name="horse_id">
                        @foreach($horses as $horse)
                          <option value="{{$horse->id}}" {{($horse_position->horse_id == $horse->id)? 'selected' : ''}}>{{$horse->name}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>

                <div class="col-lg-3">
                  <div class="form-group">
                    <label>Front (%)</label>
                    <input type="text" class="form-control" name="front" id="front" placeholder="12" value="{{$horse_position->front}}">
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group">
                    <label>Stalker (%)</label>
                    <input type="text" class="form-control" name="stalker" id="stalker" placeholder="12" value="{{$horse_position->stalker}}">
                  </div>
                </div>
              </div>
              <button type="submit" class="btn primary">Salvar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
