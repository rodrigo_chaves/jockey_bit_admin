<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NextLive extends Model
{
    protected $table = 'next_live';
}
