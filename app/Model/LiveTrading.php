<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LiveTrading extends Model
{
    protected $table = 'live_tradings';

    // public function createEmbedLink($url){
    //     $parts = explode('?v=',$url);
    //     $this->url = 'https://www.youtube.com/embed/' . $parts[count($parts)-1] . '?rel=0';
    // }

    public function createEmbedLink($url){
        if($this->isVimeo($url)){
            $parts = explode('vimeo.com/',$url);
            // $this->url = 'https://player.vimeo.com/video/' . $parts[count($parts)-1] . '?autoplay=1';
            $this->url = 'https://player.vimeo.com/video/' . $parts[count($parts)-1];
        }
        else if($this->isYoutube($url)){
            $parts = explode('?v=',$url);
            // $this->url = 'https://www.youtube.com/embed/' . $parts[count($parts)-1] . '?rel=0';
            $this->url = 'https://www.youtube.com/embed/' . $parts[count($parts)-1];
        }
        
    }
 
    public function isVimeo($url){
        return strpos($url,'vimeo.com/');
    }
 
    public function isYoutube($url){
        return strpos($url,'?v=');
    }
}
