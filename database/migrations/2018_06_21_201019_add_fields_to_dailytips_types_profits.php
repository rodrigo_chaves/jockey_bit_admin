<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToDailytipsTypesProfits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dailytips_types_profits',function(Blueprint $table){
            $table->decimal('accumulated_profit',10,2)->nullable()->after('profit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dailytips_types_profits',function(Blueprint $table){
            $table->dropColumn('accumulated_profit');
        });
    }
}
