<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Answer;
use App\Model\Question as ModuleQuestion;

class AnswersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($question)
    {
        $answers = Answer::byQuestion($question)->get();
        $q = ModuleQuestion::find($question);
        return View('answers.index')->with('answers',$answers)->with('question',$q);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($question)
    {
        return View('answers.create')->with('question',$question);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $question)
    {
        $answer = new Answer;
        $answer->module_question_id = $question;
        $answer->title = $request->input('title');
        if($request->has('correct')){
            Answer::where('module_question_id',$question)->where('correct',true)->update(['correct'=>false]);
            $answer->correct = true;
        }
        
        $answer->save();

        return redirect()->route('answers.index',['question'=>$question])->with('success','Resposta salva');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $answer = Answer::find($id);
        return View('answers.edit')->with('answer',$answer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $answer = Answer::find($id);
        $answer->title = $request->input('title');
        if($request->has('correct')){
            Answer::where('module_question_id',$answer->question->id)->where('correct',true)->update(['correct'=>false]);
            $answer->correct = true;
        }
        
        $answer->save();

        return redirect()->route('answers.index',['question'=>$answer->module_question_id])->with('success','Resposta salva');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $answer = Answer::find($id);
        $question = $answer->question;
        Answer::destroy($id);
        return redirect()->route('answers.index',['question'=>$question->id])->with('success','Resposta removida');
    }
}
