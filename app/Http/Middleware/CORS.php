<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;

class CORS
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Log::info("entrou no CORS");
        Log::info("ip " . $request->ip());
        Log::info(json_encode($request->headers->all()));
        //return $next($request);
        //header("Access-Control-Allow-Origin: *");
        // ALLOW OPTIONS METHOD
        $headers = [
            'Access-Control-Allow-Methods' => 'POST, GET, OPTIONS, PUT, DELETE',
            //'Access-Control-Allow-Headers' => 'Access-Control-*, Content-Type, X-Auth-Token, Origin, Authorization',
            'Access-Control-Allow-Headers' => 'Access-Control-*, Content-Type, X-Auth-Token, Origin, Authorization,  X-Requested-With, Accept',
            'Access-Control-Allow-Origin' => '*',
            'Allow' => 'POST, GET, OPTIONS, PUT, DELETE'
        ];
        if ($request->getMethod() == "OPTIONS") {
            // The client-side application can set only headers allowed in Access-Control-Allow-Headers
            Log::info('method is OPTIONS');
            return \Response::make('OK', 200, $headers);
        }
        $response = $next($request);
        foreach ($headers as $key => $value)
            $response->header($key, $value);
        return $response;
    }
}
