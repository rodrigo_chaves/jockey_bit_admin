<script type="text/javascript">
    var menu = <?php echo isset($menu)? '"' . $menu . '"' : 'null'?> ;
</script>

<div id="aside" class="app-aside fade box-shadow-x nav-expand white" aria-hidden="true">
    <div class="sidenav modal-dialog dk dark" ui-class="dark">
        <div class="navbar lt dark" ui-class="dark">
            <a href="" class="navbar-brand" style="margin: 0 auto;">
                <div align="center">
                    <img src="https://academy.jockeybit.com/img/logo.png" class="img-fluid">
                </div>
            </a>
        </div>
        <div class="flex hide-scroll">
            <div class="scroll">
                <div class="py-3 light lt">
                    <div class="nav-fold px-2">
                        <a class="d-flex p-2"><span class="avatar w-40 rounded grey hide">J</span> <img src="http://admacademy.jockeybit.com/avatar/augusto-queiroz.jpg" alt="..." class="w-40 circle"></a>
                        <div class="hidden-folded flex p-2" style="font-size: 14px;">
                            <div class="d-flex">
                                <a class="mr-auto text-nowrap">Augusto Queiroz<small class="d-block text-muted" style="background: #00000094; padding: 0px 4px; border-radius: 3px;">Professor Jockey</small></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="nav-border b-primary" data-nav="">
                    <ul class="nav bg">
                        <!--<li class="nav-header">
                            <span class="text-xs hidden-folded">Menu do Professor</span>
                        </li>-->
                        <li class="active" data-menu="operations">
                            <a href="/">
                                <span class="nav-icon">
                                    <i class="fas fa-tachometer-alt"></i>
                                </span>
                                <span class="nav-text">Dashboard</span>
                            </a>
                        </li>
                        <li>
                          <a>
                              <span class="nav-caret"><i class="fa fa-caret-down"></i> </span>
                              <span class="nav-icon">
                                        <i class="fa fa-check"></i>
                              </span>
                              <span class="nav-text">Tips</span>
                          </a>
                          <ul class="nav-sub">
                              <li data-menu="operations">
                                <a href="/operations">
                                    
                                    <span class="nav-text">Histórico Tips</span>
                                </a>
                            </li>
                            <li data-menu="dailytips">
                                <a href="{{route('dailytips.index')}}">
                                    <span class="nav-text">Tips do dia</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('dailytips_notice.index')}}">
                                    
                                    <span class="nav-text">Informativos de Tips</span>
                                </a>
                            </li>
                            <li>
                              <a href="{{route('noticevideos.index')}}">
                                <span class="nav-text">Videos de Tips</span>
                              </a>
                            </li>
                            <li>
                                <a href="{{route('dailytipsprofits.index')}}">
                                    <span class="nav-text">Lucros de Tips</span>
                                </a>
                            </li>
                          </ul>
                        </li>
                        
                        <li>
                            <a href="{{url('daily_new/1/edit')}}">
                                <span class="nav-icon">
                                    <i class="fab fa-bitcoin"></i>
                                </span>
                                <span class="nav-text">Informativo diário</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{url('daily_new/2/edit')}}">
                                <span class="nav-icon">
                                    <i class="fa fa-bullhorn"></i>
                                </span>
                                <span class="nav-text">Informativo diario (Turfe)</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{route('official_notice.index')}}">
                                <span class="nav-icon">
                                    <i class="fas fa-microphone-alt"></i>
                                </span>
                                <span class="nav-text">Comunicado Oficial</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{route('livetradings.index')}}">
                                <span class="nav-icon">
                                    <i class="fa fa-play-circle"></i>
                                </span>
                                <span class="nav-text">Live Tradings</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{route('next_live.index')}}">
                                <span class="nav-icon">
                                    <i class="fa fa-play-circle"></i>
                                </span>
                                <span class="nav-text">Próxima live</span>
                            </a>
                        </li>

                        <li>
                          <a>
                            <span class="nav-caret"><i class="fa fa-caret-down"></i> </span>
                            <span class="nav-icon">
                                <i class="fa fa-bars"></i>
                            </span>
                            <span class="nav-text">Extratos</span>
                          </a>
                          <ul class="nav-sub">
                            <li>
                                <a href="{{route('operations_history.index')}}">
                                    <span class="nav-text">Extratos de Operações</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('nextophistory.index')}}">
                                    <span class="nav-text">Próximo extrato de operaçoes</span>
                                </a>
                            </li>
                          </ul>
                        </li>

                        <li>
                          <a>
                              <span class="nav-icon">
                                <i class="fas fa-chalkboard-teacher"></i>
                              </span>
                              <span class="nav-caret"><i class="fa fa-caret-down"></i> </span>
                              <span class="nav-text">Aulões</span>
                          </a>
                          <ul class="nav-sub">
                              <li>
                                  <a href="{{route('week_class.index')}}">
                                      <span class="nav-icon">
                                          <i class="fas fa-chalkboard-teacher"></i>
                                      </span>
                                      <span class="nav-text">Aulas semanais</span>
                                  </a>
                              </li>
                              <li>
                                  <a href="{{route('big_week_class.index')}}">
                                      <span class="nav-icon">
                                          <i class="fas fa-chalkboard-teacher"></i>
                                      </span>
                                      <span class="nav-text">Aulões semanais</span>
                                  </a>
                              </li>
                          </ul>
                        </li>

                        <li>
                            <a href="{{route('chats.index')}}">
                                <span class="nav-icon">
                                    <i class="fas fa-comment"></i>
                                </span>
                                <span class="nav-text">Chat</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{route('modules.index')}}">
                                <span class="nav-icon">
                                    <i class="fas fa-calendar-alt"></i>
                                </span>
                                <span class="nav-text">Modulos</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('modals.index')}}">
                                <span class="nav-icon">
                                    <i class="far fa-window-maximize"></i>
                                </span>
                                <span class="nav-text">Modals</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{route('tickets.index')}}">
                                <span class="nav-icon">
                                    <i class="fas fa-headset"></i>
                                </span>
                                <span class="nav-text">Tickets de Suporte</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{route('bigbets.index')}}">
                                <span class="nav-icon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <span class="nav-text">Bolão</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{route('languages.index')}}">
                                <span class="nav-icon">
                                    <i class="fa fa-language"></i>
                                </span>
                                <span class="nav-text">Idiomas</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{route('racingtracks.index')}}">
                                <span class="nav-icon">
                                    <i class="fa fa-road"></i>
                                </span>
                                <span class="nav-text">Pistas</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('distances.index')}}">
                                <span class="nav-icon">
                                    <i class="fa fa-language"></i>
                                </span>
                                <span class="nav-text">Distancias</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('classes.index')}}">
                                <span class="nav-icon">
                                    <i class="fa fa-language"></i>
                                </span>
                                <span class="nav-text">Classes</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('horses.index')}}">
                                <span class="nav-icon">
                                    <i class="fas fa-horse"></i>
                                </span>
                                <span class="nav-text">Cavalos Pré-live</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{route('livehorses.index')}}">
                                <span class="nav-icon">
                                    <i class="fas fa-horse"></i>
                                </span>
                                <span class="nav-text">Cavalos live</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{route('horsepositions.index')}}">
                                <span class="nav-icon">
                                    <i class="fa fa-language"></i>
                                </span>
                                <span class="nav-text">Trading Largada</span>
                            </a>
                        </li>

                        <li>
                            <a>
                                <span class="nav-caret"><i class="fa fa-caret-down"></i> </span>
                                <span class="nav-icon">
                                    <i class="fas fa-glasses"></i>
                                </span>
                                <span class="nav-text">Análises</span>
                            </a>
                            <ul class="nav-sub">
                                <li>
                                    <a href="{{route('analysis.index')}}">
                                        <span class="nav-text">Todas</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="{{route('analysistips.index')}}">
                                        <span class="nav-text">Análises de Tips</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="{{route('analysisvalidated')}}">
                                        <span class="nav-text">Análises Validadas</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="{{route('tipstar.index')}}">
                                <span class="nav-icon">
                                    <i class="fa fa-star"></i>
                                </span>
                                <span class="nav-text">Tips Star</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('startipvideos.index')}}">
                                <span class="nav-icon">
                                    <i class="fa fa-star"></i>
                                </span>
                                <span class="nav-text">Tips Star (Videos)</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{route('startipsprofits.index')}}">
                                <span class="nav-icon">
                                    <i class="fa fa-star"></i>
                                </span>
                                <span class="nav-text">Tips Star (Lucros)</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{route('avulsevideos.index')}}">
                                <span class="nav-icon">
                                    <i class="fa fa-camera"></i>
                                </span>
                                <span class="nav-text">Videos Avulsos</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{route('racingtracksguides.index')}}">
                                <span class="nav-icon">
                                    <i class="fa fa-star"></i>
                                </span>
                                <span class="nav-text">Guias das pistas</span>
                            </a>
                        </li>

                    </ul>
                </div>

            </div>
        </div>
    </div>
</div>