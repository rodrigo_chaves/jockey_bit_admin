<div class="modal" id="add_string_translation" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Nova tradução</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('contentstrings.store')}}" method="post">
        <div class="modal-body">
          
          {{ csrf_field() }}
            <input type="hidden" name="key" id="cs_key">
            <div class="form-group">
              <label>Idioma</label>
              <select class="form-control" name="language_id">
              </select>
            </div>
            <div class="form-group">
              <label>Texto</label>
              <input type="text" id="cs_text" class="form-control" name="text" required>
            </div>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Salvar</button>
        </div>

      </form>
    </div>
  </div>
</div>