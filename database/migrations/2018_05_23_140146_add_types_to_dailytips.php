<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypesToDailytips extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('daily_tips',function(Blueprint $table){
            $table->integer('dailytips_types_id')->unsigned()->nullable();
            $table->foreign('dailytips_types_id')->references('id')->on('dailytips_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('daily_tips',function(Blueprint $table){
            $table->dropForeign(['dailytips_types_id']);
        });
    }
}
