@extends('layout.panel')

@section('content')

<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Plataforma do Admin</a></li>
          <li class="breadcrumb-item active">analises</li>
        </ol>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Editar</h3>
            </div>
            <div class="box-body">
              <form action="{{route('analysis.update',['id'=> $analyze->id])}}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                  <select class="form-control" name="racing_track_id">
                    @foreach($tracks as $track)
                      <option value="{{$track->id}}">{{$track->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <select class="form-control" name="distance_id">
                    @foreach($distances as $distance)
                      <option value="{{$distance->id}}" {{($analyze->distance_id == $distance->id)? 'selected' : ''}}>{{$distance->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <select class="form-control" name="horse_id">
                    @foreach($horses as $horse)
                      <option value="{{$horse->id}}" {{($analyze->horse_id == $horse->id)? 'selected' : ''}}>{{$horse->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <select class="form-control" name="livehorse_id">
                    @foreach($live_horses as $livehorse)
                      <option value="{{$livehorse->id}}" {{($analyze->livehorse_id == $livehorse->id)? 'selected' : ''}}>{{$livehorse->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <input type="text" name="odd" placeholder="odd" class="form-control" value="{{$analyze->odd}}">
                </div>
                <div class="form-group">
                  <select name="classe_id" class="form-control">
                    @foreach($classes as $classe)
                      <option value="{{$classe->id}}" {{($analyze->classe_id == $classe->id)? 'selected' : ''}}>{{$classe->name}}</option>
                    @endforeach
                  </select>
                </div>

                <div class="form-group mx-sm-2">
                  <button class="btn btn-success">Salvar</button>
                </div>
              </form>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
@endsection
