@extends('layout.panel')

@section('content')

<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <ol class="breadcrumb"><li class="breadcrumb-item"><a href="#">Plataforma do Professor</a></li><li class="breadcrumb-item active">Próximo Extrato de Operações</li></ol>
        
        <table id="" class="table v-middle p-0 m-0 box dataTable no-footer" data-plugin="dataTable" role="grid" aria-describedby="datatable_info" style="width: 1094px;">
          <thead>
            <tr>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Data e horário do próximo Histórico</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Ações</td>
            </tr>
          </thead>
          <tbody>
          @if($nextophistory!=null)
            <tr>
                <td align="center">{{$nextophistory->due_date}}</td>
                
                <td align="center">
                  <a href="{{route('nextophistory.edit',['nextophistory'=>$nextophistory->id])}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                </td>
            </tr>
          @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
  @include('layout.elements.footer')
</div>
@endsection
