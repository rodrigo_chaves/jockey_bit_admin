@extends('layout.panel')

@section('content')

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">

<style>
    .videoWrapper {
        position: relative;
        padding-bottom: 56.25%; /* 16:9 */
        padding-top: 25px;
        height: 0;
    }
    .videoWrapper iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
</style>

<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
        @if(Session::has('error'))
          <div class="alert green">
            <b>{{Session::get('error')}}</b>
          </div>
        @endif
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-lg-3">
          <div class="box list-item">
            <!-- <span class="avatar w-40 text-center rounded success">
              <span class="fa fa-users"></span>
            </span> -->
            <span class="avatar w-40 text-center text-success">
              <i class="fa fa-users"></i>
            </span>
            <div class="list-body">
              <h4 class="m-0 text-md">
                <a href="#">{{$total_users}} <span class="text-sm">Alunos</span></a>
              </h4>
            </div>
          </div>
      </div>
      <div class="col-lg-3">
          <div class="box list-item">
            <!-- <span class="avatar w-40 text-center rounded primary">
              <span class="fas fa-calendar-alt"></span>
            </span> -->
            <span class="avatar w-40 text-primary text-center">
              <i class="fas fa-calendar-alt"></i>
            </span>
            <div class="list-body">
              <h4 class="m-0 text-md">
                <a href="{{route('modules.index')}}">{{$active_modules}} <span class="text-sm">Módulos ativos</span></a>
              </h4>
              <small class="text-muted">{{$total_modules}} total</small>
            </div>
          </div>
      </div>
      <div class="col-lg-3">
          <div class="box list-item">
            <!-- <span class="avatar w-40 text-center rounded warning"> -->
            <span class="avatar text-warning w-40 text-center">
              <i class="fas fa-headset"></i>
            </span>
            <!-- </span> -->
            <div class="list-body">
              <h4 class="m-0 text-md">
                <a href="{{route('tickets.index')}}">{{$open_tickets}} <span class="text-sm">Tickets aguardando</span></a>
              </h4>
              <small class="text-muted">{{$total_tickets}} total</small>
            </div>
          </div>
      </div>
      <div class="col-lg-3">
        <div class="box list-item">
          <!-- <span class="avatar w-40 text-center rounded warning"> -->
          <span class="avatar w-40 text-center" style="color: #49cece !important;">
            <i class="fas fa-star"></i>
          </span>
          <!-- </span> -->
          <div class="list-body">
            <h4 class="m-0 text-md">
              <a href="{{route('tipstar.index')}}">{{$tipstars}} <span class="text-sm">Tips Stars hoje</span></a>
            </h4>
            <small class="text-muted">{{$total_tipstars}} total</small>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-7">
          <div class="box">
              <div class="box-header"><h3>Alunos por modulo</h3></div>
              <div class="box-body p-0">
                <input type="hidden" name="users_modules" value="{{$users_on_modules->toJson()}}">
                <div id="users_module_canvas"></div>
              </div>
          </div>
      </div>
      <div class="col-lg-5">
        <div class="box">
          <div class="box-header">
            <h3>Ultimas provas</h3>
          </div>
          <div class="box-tool">
            <a href="{{route('usertests.index')}}" class="btn btn-primary btn-xs">Ver todos</a>
          </div>
          <div class="box-body table p-0">
            <table class="table table-hover">
              <thead>
                <th>Usuario</th>
                <th class="text-center">Módulo</th>
                <th class="text-center">Nota</th>
                <th class="text-center">Aprovado</th>
              </thead>
              <tbody>
                @foreach($last_tests as $test)
                <tr>
                  <td>{{$test->user->firstName()}}</td>
                  <td class="text-center">{{$test->module->title}}</td>
                  <td class="text-center">{{$test->decimalGrade()}}</td>
                  <td class="text-center">{!!$test->approved? '<span class="badge badge-success">Sim</span>' : '<span class="badge badge-danger">Não</span>'!!}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="row align-items-stretch">
      <div class="col-lg-6">
          <div class="box">
            <div class="box-header"><h3>Últimos tickets</h3></div>
            <div class="box-tool">
              <a href="{{route('tickets.index')}}" class="btn btn-primary btn-xs">Ver todos</a>
            </div>
            <div class="list inset">
              @foreach($last_tickets as $ticket)
                <div class="list-item" data-id="item-9">
                  
                    <div class="list-body">
                      <a class="item-title _500">{{$ticket->user->name}} - <small>{{$ticket->created_at}}</small></a>
                      <div class="item-except text-sm text-muted h-1x">{{$ticket->title}}</div>
                    </div>
                    <div>
                        <div class="item-action">
                          <a href="{{route('tickets.show',['id'=>$ticket->id])}}" class="btn primary" aria-expanded="false">
                            <i class="fa fa-comment"></i>
                          </a>
                        </div>
                    </div>
                </div>
              @endforeach
            </div>
          </div>
          
      </div>
      <div class="col-lg-6">
        <div class="box">
          <div class="box-header">
            <h3>Última Live</h3>
            <small>{{$last_live_trading->title}}</small>
          </div>
          <div class="box-tool">
              <a href="{{route('next_live.index')}}" class="btn btn-primary btn-xs">Próxima live</a>
            </div>
          <div class="list-body">
            <div class="videoWrapper">                                    
              <iframe width="100%" height="auto" src="{{$last_live_trading->url}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection


@section('js')
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
<script type="text/javascript">
var earnings_graph = Morris.Area.prototype.fillForSeries = function (i) {
        var color;
        return "10-#b452c3-#49cece";
    };

    var users_modules_graph = JSON.parse($('input[name=users_modules]').val());
    Morris.Area({
      element:'users_module_canvas',
      pointSize:0,
      lineWidth:0,
      data:users_modules_graph,
      xkey:'module',
      ykeys:['amount'],
      labels:['Alunos'],
      resize:false,
      parseTime : false,
      hideHover:"auto",
      fillOpacity: 0.8,
      gridLineColor:"transparent"
    });
</script>
@endsection
