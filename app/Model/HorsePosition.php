<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HorsePosition extends Model
{
    protected $table = 'horse_positions_start';

    public function track(){
        return $this->belongsTo('App\Model\RacingTrack','racing_track_id','id');
    }

    public function horse(){
        return $this->belongsTo('App\Model\Horse','horse_id','id');
    }
}
