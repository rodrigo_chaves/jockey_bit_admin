<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToTipstars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tips_star',function(Blueprint $table){
            $table->time('hour')->nullable();
            $table->string('run')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tips_star',function(Blueprint $table){
            $table->dropColumn('hour');
            $table->dropColumn('run');
        });
    }
}
