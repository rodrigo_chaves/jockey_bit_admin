<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class DailyTip extends Model
{
    protected $table = 'daily_tips';

    public function type(){
        return $this->belongsTo('App\Model\DailytipType','dailytips_types_id', 'id');
    }

    /** methods */

    public function timeFormated(){
        $carbon = Carbon::createFromFormat('Y-m-d H:i:s', $this->due_date);
        return $carbon->format('H:i');
    }

    public function dateFormated(){
        $carbon = Carbon::createFromFormat('Y-m-d H:i:s', $this->due_date);
        return $carbon->format('d/m/Y');
    }

}
