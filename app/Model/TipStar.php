<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class TipStar extends Model
{
    protected $table = 'tips_star';

    /** scope */
    public function scopeFromToday($query){
        $carbon = Carbon::now();
        return $query->whereDate('created_at',$carbon->toDateString());
    }

    public function createEmbedLink($url){
      if($this->isVimeo($url)){
          $parts = explode('vimeo.com/',$url);
          $this->embed_url = 'https://player.vimeo.com/video/' . $parts[count($parts)-1] . '';
      }
      else if($this->isYoutube($url)){
          $parts = explode('?v=',$url);
          $this->embed_url = 'https://www.youtube.com/embed/' . $parts[count($parts)-1] . '';
      }
    }

    public function isVimeo($url){
      return strpos($url,'vimeo.com/');
    }

    public function isYoutube($url){
      return strpos($url,'?v=');
    }
}
