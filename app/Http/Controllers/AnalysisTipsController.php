<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\AnalyzeTip;
use App\Model\Distance;
use App\Model\RacingTrack;
use App\Model\Horse;
use App\Model\LiveHorse;
use App\Model\AnalyzeClasse;
use Validator;

class AnalysisTipsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $analysis_tips = AnalyzeTip::where('validated',false)->paginate(20);

        return View('analysistips.index')->with('analysis_tips',$analysis_tips);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('analysistips.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[]);

        if($validation->fails()){
            return redirect()->back()->with('error',$validation->errors()->all()[0]);
        }
        else{
            $analyze_tip = new AnalyzeTip;
            $analyze_tip->back_odd = $request->input('back_odd');
            $analyze_tip->high_price = $request->input('high_price');
            $analyze_tip->percent = $request->input('percent');
            $analyze_tip->racingpost = $request->input('racingpost');
            $analyze_tip->position_odd = $request->input('position_odd');
            $analyze_tip->lay_odd = $request->input('lay_odd');
            $analyze_tip->challenge_odd = $request->input('challenge_odd');
            $analyze_tip->save();

            return redirect()->route('analysistips.index')->with('success','Analise salva');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $analyze_tip = AnalyzeTip::find($id);

        $distances = Distance::get();
        $tracks = RacingTrack::get();
        $horses = Horse::get();
        $live_horses = LiveHorse::get();
        $classes = AnalyzeClasse::get();

        return View('analysistips.edit')
        ->with('analyze_tip',$analyze_tip)
        ->with('distances',$distances)
                ->with('tracks',$tracks)
                ->with('horses',$horses)
                ->with('classes',$classes)
                ->with('live_horses',$live_horses);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(),[]);

        if($validation->fails()){
            return redirect()->back()->with('error',$validation->errors()->all()[0]);
        }
        else{
            $analyze_tip = AnalyzeTip::find($id);
            $analyze_tip->back_odd = $request->input('back_odd');
            $analyze_tip->high_price = $request->input('high_price');
            $analyze_tip->percent = $request->input('percent');
            $analyze_tip->racingpost = $request->input('racingpost');
            $analyze_tip->position_odd = $request->input('position_odd');
            $analyze_tip->lay_odd = $request->input('lay_odd');
            $analyze_tip->challenge_odd = $request->input('challenge_odd');
            $analyze_tip->save();

            return redirect()->route('analysistips.index')->with('success','Analise salva');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AnalyzeTip::destroy($id);
        return redirect()->route('analysistips.index')->with('success','Analise removida');

    }

    public function validateTip($id){
        $analyze_tip = AnalyzeTip::find($id);
        $analyze_tip->validated = true;
        $analyze_tip->save();

        return redirect()->route('analysistips.index')->with('success','Analise validada');
    }
}