@extends('layout.panel')

@section('content')
<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h2>Nova Operação</h2>
          </div>
          <div class="box-divider m-0"></div>
          <div class="box-body">
            <form action="{{route('operations_history.update',['id'=>$operation_history->id])}}" method="post">
              <input type="hidden" name="_method" value="put">
                {{ csrf_field() }}
              <div class="form-group">
                <label>Titulo</label>
                <input type="text" class="form-control translate" name="title" id="title" placeholder="Titulo" value="{{$operation_history->title}}">
              </div>

              <div class="form-group">
                <label>Url</label>
                <input type="text" class="form-control" name="url" id="url" placeholder="url" value="{{$operation_history->url}}">
              </div>

              <div class="form-group">
                <label>Imagem</label>
                <input type="text" class="form-control" name="picture" id="result" placeholder="Imagem" value="{{$operation_history->picture}}">
              </div>

              <div class="form-group">
                <label><input type="checkbox" name="active" {{($operation_history->active)? 'checked': ''}}> Ativo</label>
                
              </div>
              
              <button type="submit" class="btn green">Atualizar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
