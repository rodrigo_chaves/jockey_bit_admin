@extends('layout.panel')

@section('content')
<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h2>Novo Comunicado de tipos</h2>
          </div>
          <div class="box-divider m-0"></div>
          <div class="box-body">
            <form action="{{route('dailytips_notice.update',['dailytips_notice' => $notice->id])}}" method="post">
              {{ csrf_field() }}
              <input type="hidden" name="_method" value="put">

              <div class="checkbox">
                <label class="ui-check ui-check-md">
                  <input type="checkbox" name="active" id="active" {{($notice->active)? 'checked' : ''}}> <i class="dark-white"></i> Ativo
                </label>
              </div>

              <div class="form-group">
                <label>Tipo</label>
                <select name="dailytips_types_id" class="form-control">
                  @foreach($types as $type)
                    <option value="{{$type->id}}" {{($type->id==$notice->dailytips_types_id)? 'checked' : ''}}>{{$type->name}}</option>
                  @endforeach
                </select>
              </div>

              <div class="form-group">
                <label>Novo Comunicado</label>
                <textarea id="msg" name="msg" class="form-control">{{$notice->msg}}</textarea>
              </div>

            
              <button type="submit" class="btn primary">Salvar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="/template/libs/tinymce/tinymce.min.js"></script>
<script>tinymce.init({ selector:'#msg' });</script>

@endsection
