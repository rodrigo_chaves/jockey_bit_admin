@extends('layout.panel')

@section('content')
<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <h1 class="display-4 l-s-n-1x">
          <span class="text-muted _300">{{$question->title}}</span>
        
          @if(count($answers)<5)
            <a href="{{route('answers.create',['question'=>$question->id])}}" class="btn btn-fw primary">Nova resposta</a>
          @endif
        </h1>
        <table id="" class="table v-middle p-0 m-0 box dataTable no-footer" data-plugin="dataTable" role="grid" aria-describedby="datatable_info" style="width: 1094px;">
          <thead>
            <tr>
              <td align="center">Resposta</td>
              <td align="center">Correta?</td>              
              <td></td>
            </tr>
          </thead>
          <tbody>
            @foreach($answers as $answer)
              <tr>
                <td>{{$answer->title}}</td>
                <td align="center">{{$answer->correct? 'Sim':''}}</td>
                <td>
                  <form action="{{route('answers.destroy',['answer'=>$answer->id])}}" method="post">
                    <input type="hidden" name="_method" value="delete" />
                    {!! csrf_field() !!}
                    <a href="{{route('answers.edit',['id'=>$answer->id])}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                    <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>
                  </form>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      
    </div>
  </div>
  @include('layout.elements.footer')
</div>
@endsection