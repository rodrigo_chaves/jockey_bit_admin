<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AnalyzeClasse extends Model
{
    protected $table = 'analysis_classes';
}
