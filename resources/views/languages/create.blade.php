@extends('layout.panel')

@section('content')
<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('error'))
          <div class="alert green">
            <b>{{Session::get('error')}}</b>
          </div>
        @endif
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h2>Novo idioma</h2>
          </div>
          <div class="box-divider m-0"></div>
          <div class="box-body">
            <form action="{{route('languages.store')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label>Nome</label>
                      <input type="text" class="form-control" name="name" id="name" placeholder="Nome">
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label>Apelido</label>
                      <input type="text" class="form-control" name="nickname" id="nickname"/>
                    </div>
                  </div>
                </div>
              
              
              <div class="form-group">
                <label>Ícone</label>
                <input class="form-control" type="text" name="icon" placeholder="Icone">
              </div>
              
              <button type="submit" class="btn primary">Salvar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
