<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Model\BigBet;

class BigBetsController extends Controller
{
    public function index(){
        $carbon = Carbon::now();
        //  $bigbets = BigBet::fromToday($carbon->format('Y-m-d'))->latest()->get();
        $bigbets = BigBet::latest()->paginate(30);
        //$bigbets = BigBet::latest()->get();

        return View('bigbets.index')->with('bigbets',$bigbets);
    }
}
