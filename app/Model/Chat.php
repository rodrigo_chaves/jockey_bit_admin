<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $table = 'chats';

    public function messages(){
        return $this->hasMany('App\Model\Message');
    }
}
