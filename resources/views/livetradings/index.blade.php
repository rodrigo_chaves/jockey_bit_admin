@extends('layout.panel')

@section('content')

<div class="content-main " id="content-main">
  <div class="padding">
  
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>

    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <ol class="breadcrumb"><li class="breadcrumb-item"><a href="#">Plataforma do Professor</a></li><li class="breadcrumb-item active">Live Trading</li></ol>
        <a href="{{route('livetradings.create')}}" class="btn btn-fw primary" style="margin-bottom: 40px;"><i class="fa fa-plus"></i> Adicionar nova Live Trading</a>
        <table id="" class="table v-middle p-0 m-0 box dataTable no-footer" data-plugin="dataTable" role="grid" aria-describedby="datatable_info" style="width: 1094px;">
          <thead>
            <tr>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Título</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">URL</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Ação</td>
            </tr>
          </thead>
          <tbody>
            @foreach($livetradings as $live)
              <tr>
                  <td align="center">{{$live->title}}</td>
                  <td align="center">{{$live->url}}</td>
                  <td align="right">
                    <form action="{{route('livetradings.destroy',['livetrading'=>$live->id])}}" method="post">
                      <input type="hidden" name="_method" value="delete">
                      {{csrf_field()}}
                      <a href="{{route('livetradings.edit',['id'=>$live->id])}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                      <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>
                    </form>
                  </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  @include('layout.elements.footer')
</div>
@endsection
