<?php

namespace App\Http\Controllers\Chat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Chat;
use App\Library\PusherLib;
use App\User;
use Validator;

class ChatsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $chats = Chat::paginate(10);

        return View('chats.index')->with('chats',$chats);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validation = Validator::make($request->all(),[
            'name' => 'required'
        ]);

        if($validation->fails()){
            return redirect()->back()->with('error',$validation->errors()->all()[0]);
        }
        else{
            $chat = new Chat;
            $chat->name = $request->input('name');
            $chat->active = $request->has('active')? true : false;
            $chat->save();
    
            return redirect()->route('chats.index')->with('success','Chat salvo');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $chat = Chat::find($id);
        return View('chats.show')->with('chat',$chat);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $chat = Chat::find($id);

        return View('chats.edit')->with('chat',$chat);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(),[
            'name' => 'required'
        ]);

        if($validation->fails()){
            return redirect()->back()->with('error',$validation->errors()->all()[0]);
        }
        else{
            $chat = Chat::find($id);
            $chat->name = $request->input('name');
            $chat->active = $request->has('active')? true : false;
            $chat->save();
    
            return redirect()->route('chats.index')->with('success','Chat salvo');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function block_user(Request $request){
        $user_id = $request->input('user_id');
        $user = User::find($user_id);
        if($user){
            $user->blocked_on_chat = true;
            $user->save();
            
            $data['id'] = $user->id;

            $pusher = PusherLib::instanciate();
            $pusher->trigger(env('CHAT_CHANNEL') , 'UserBlocked', $data);

            return response()->json(['success'=>true]);
        }
    }
}
