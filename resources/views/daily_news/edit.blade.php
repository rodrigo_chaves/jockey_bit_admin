@extends('layout.panel')

@section('content')
  <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'textarea' });</script>
<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">


<div class="content-header white  box-shadow-0" id="content-header">
  <div class="navbar navbar-expand-lg">
    <a class="d-lg-none mx-2" data-toggle="modal" data-target="#aside">
      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 512 512">
        <path d="M80 304h352v16H80zM80 248h352v16H80zM80 192h352v16H80z"></path>
      </svg>
    </a>
  </div>
</div>

<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h2>Informativo Diário</h2>
          </div>
          <div class="box-divider m-0"></div>
          <div class="box-body">
            <form  method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <label>Titulo</label>
                <input type="text" class="form-control translate" value="{{$data->title}}" name="title" id="title" required="">
              </div>
              <div class="form-group">
                <label>Conteúdo</label>
                <textarea class="form-control" name="content" required="">
                  {{$data->content}}
                </textarea>
              </div>
              <div class="form-group">
                <label>Status</label>
                <select class="form-control" name="status" required="">
                  <option value="{{$data->status}}">{{$data->status}}</option>
                  <option value="Active">Ativo</option>
                  <option value="Inactive">Inativo</option>
                </select>
              </div>



              
              <button type="submit" class="btn primary">Salvar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.13/jquery.mask.js"></script>
<script type="text/javascript">
  $(function () {
    $('input[type=datetime]').mask('0000-00-00 00:00');
  });
</script>

@endsection