<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Module;
use Validator;

class ModulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = Module::paginate(10);
        return View('modules.index')->with('modules',$modules);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('modules.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //TODO implement validator
        $module = new Module;
        $module->title = $request->input('title');
        $module->description = $request->input('description');
        $module->active = $request->has('active')? true : false;
        if($request->hasFile('picture')){
            $request->picture->move('storage/modules');
        }
        $module->save();

        return redirect()->route('modules.index')->with('success','Modulo criado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $module = Module::find($id);
        return View('modules.edit')->with('module',$module);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $module = Module::find($id);
        $module->title = $request->input('title');
        $module->description = $request->input('description');
        $module->active = $request->has('active')? true : false;
        $module->save();

        return redirect()->route('modules.index')->with('success','Modulo atualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Module::destroy($id);
        return redirect()->route('modules.index')->with('success','Modulo removido');
    }
}
