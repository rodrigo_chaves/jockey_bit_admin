<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\OfficialNotice;

class OfficialNoticeController extends Controller
{
    public function index(){
      $official_notices = OfficialNotice::latest()->paginate(10);

      return View('official_notice.index')->with('official_notices',$official_notices);
    }

    public function create(){
      return View('official_notice.create');
    }

    public function store(Request $request){

      $official_notice = new OfficialNotice;
      $official_notice->msg = $request->input('msg');
      $official_notice->active = $request->has('active')? true : false;

      if($request->has('active')){
        OfficialNotice::where('active',true)->update(['active'=>false]);
      }
      $official_notice->save();

      return redirect()->route('official_notice.index')->with('success','Comunicado salvo');
    }

    public function edit($id){
      $official_notice = OfficialNotice::find($id);

      return View('official_notice.edit')->with('official_notice',$official_notice);
    }

    public function update(Request $request, $id){
      $official_notice = OfficialNotice::find($id);

      $official_notice->msg = $request->input('msg');
      $official_notice->active = $request->has('active')? true : false;

      if($request->has('active')){
        OfficialNotice::where('active',true)->update(['active'=>false]);
      }

      $official_notice->save();

      return redirect()->route('official_notice.index')->with('success','Comunicado atualizado');
      
    }

    public function deactivate($id){
      $official_notice = OfficialNotice::find($id);
      $official_notice->active = false;
      $official_notice->save();

      return redirect()->route('official_notice.index')->with('success','Comunicado desativado');
    }

    public function destroy($id){
      OfficialNotice::destroy($id);

      return redirect()->route('official_notice.index')->with('success','Comunicado removido');
    }
}
