<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\StarTipVideo;

class StarTipVideosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = StarTipVideo::latest()->paginate(10);

        return View('startip_videos.index')->with('videos', $videos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('startip_videos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $startipvideo = new StarTipVideo;
        $startipvideo->active = $request->has('active')? true: false;
        $startipvideo->title = $request->input('title');
        $startipvideo->msg = $request->input('msg');
        $startipvideo->url = $request->input('url');
        $startipvideo->createEmbedLink($request->input('url'));

        $startipvideo->save();

        return redirect()->route('startipvideos.index')->with('success','Video salvo');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $startipvideo = StarTipVideo::find($id);

        return View('startip_videos.edit')->with('video',$startipvideo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $startipvideo = StarTipVideo::find($id);
        $startipvideo->active = $request->has('active')? true: false;
        $startipvideo->title = $request->input('title');
        $startipvideo->msg = $request->input('msg');
        $startipvideo->url = $request->input('url');
        $startipvideo->createEmbedLink($request->input('url'));

        $startipvideo->save();

        return redirect()->route('startipvideos.index')->with('success','Video atualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $video = StarTipVideo::find($id);
        if($video){
            StarTipVideo::destroy($id);
        }
  
        return redirect()->route('startipvideos.index')->with('success','Video removido');
    }
}
