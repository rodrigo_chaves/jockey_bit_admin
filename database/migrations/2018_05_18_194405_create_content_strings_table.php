<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentStringsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_strings',function(Blueprint $table){
            $table->increments('id');
            $table->string('key');
            $table->string('text');
            $table->integer('language_id')->unsigned();
            $table->foreign('language_id')->references('id')->on('string_languages');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('content_strings');
    }
}
