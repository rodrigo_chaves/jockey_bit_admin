@extends('layout.panel')

@section('content')
<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h2>Nova Live Trading</h2>
          </div>
          <div class="box-divider m-0"></div>
          <div class="box-body">
            <form action="{{route('livetradings.update',['id'=>$live_trading->id])}}" method="post">
              <input type="hidden" name="_method" value="put">
                {{ csrf_field() }}
              <div class="form-group">
                <label>Titulo</label>
                <input type="text" class="form-control translate" name="title" id="title" placeholder="Titulo" value="{{$live_trading->title}}">
              </div>

              <div class="form-group">
                <label>Url</label>
                <input type="text" class="form-control" name="url" id="url" placeholder="url" value="{{$live_trading->url}}">
              </div>

              <div class="form-group">
                <label>Imagem</label>
                <input type="text" class="form-control" name="picture" id="picture" placeholder="Imagem" value="{{$live_trading->picture}}">
              </div>

              <div class="form-group">
                <label><input type="checkbox" name="active" {{($live_trading->active)? 'checked' : '' }}> Ativo</label>
              </div>

              <div class="form-group">
                <label><input type="checkbox" name="active_on_office" {{($live_trading->active_on_office)? 'checked' : '' }}> Ativo no Backoffice</label>
              </div>
              
              <button type="submit" class="btn green">Atualizar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
