$(document).ready(function(){
  var qtd_languages = 0;
  var isLoadingLangs = false;
  var isLoadingTrans = false;

  var load_languages = function(){
    if(!isLoadingLangs){
      isLoadingLangs = true;
      $.get('/api/stringlanguages',function(data){
        //console.log(data);
        if(data.success){
          qtd_languages = data.data.length;
          //console.log('qtd_langs: ' + qtd_languages);
          load_input_translations();
          populate_langs(data.data);
        }
        isLoadingLangs = false;
      });
    }
  };

  var load_translations = function(text, element){
    if(!isLoadingTrans){
      isLoadingTrans = true;
      $.get('/api/contentstrings/'+text,function(data){
        //console.log(data);
        if(data.success){
          var key = '';
          if(data.data.length > 0){
            key = data.data[0].key;
            populate_translations(element,data.data);
          }
          if(data.data.length < qtd_languages){
            add_plus_button(element,key);
          }
        }
        isLoadingTrans = false;
      });
    }
    
  };

  var populate_langs = function(langs){
    for(var i=0;i<langs.length;i++){
      var option = '<option value="'+langs[i].id+'">'+langs[i].name+'</option>';
      $('select[name=language_id]').append(option);
    }
  };

  var populate_translations = function(element, translations){
    //console.log(translations);
    for(var i=0; i< translations.length; i++){
      var lang = translations[i];
      var lang_button = '<a href="#" data-target="#edit_string_translation" class="btn btn-sm info mt-1 mr-1 add_trans">'+lang.language.nickname+'</a>';
      element.after(lang_button);
    }
  };

  var add_plus_button = function(element, key){
    var plus_button = '<a href="#" data-cskey="'+key+'" data-target="#add_string_translation" class="btn btn-sm success mt-1 mr-1 add_trans"><i class="fa fa-plus"></i></a>';
    element.after(plus_button);
  };

  var load_input_translations = function(){
    $('input.translate').each(function(){
      var encodedText = encodeURIComponent($(this).val());
      load_translations(encodedText, $(this));
    });
  };

  

  load_languages();

  $(document).on('click','.add_trans', function(evt){
    evt.preventDefault();
    var modal = $(this).data('target');
    var key = $(this).data('cskey');
    var text = $(this).prev('input').val();
    $(modal).find("#cs_text").val(text);
    $(modal).find('#cs_key').val(key);
    $(modal).modal('show');
    
  });

});

