@extends('layout.panel')

@section('content')

<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Plataforma do Professor</a></li>
          <li class="breadcrumb-item active">Distancias</li>
          </ol>
          <a href="{{route('distances.create')}}" class="btn btn-fw primary" style="margin-bottom: 40px;"><i class="fa fa-plus"></i> Adicionar distancia</a>
        <table id="" class="table v-middle p-0 m-0 box dataTable no-footer">
          <thead>
            <tr>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">#</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Nome</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Ações</td>
            </tr>
          </thead>
          <tbody>
            @foreach($distances as $distance)
              <tr>
                  <td align="center">{{$distance->id}}</td>
                  <td align="center">{{$distance->name}}</td>
                  <td align="right">
                    <form action="{{route('distances.destroy',['id'=>$distance->id])}}" method="post">
                      {{csrf_field()}}

                      <input type="hidden" name="_method" value="delete">
                      <!-- <a href="{{route('distances.edit',['id'=>$distance->id])}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a> -->
                      
                      <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>
                    </form>
                  </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
