<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\TipStar;
use Validator;

class TipStarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tips = TipStar::orderBy('date','DESC')->orderBy('hour','DESC')->paginate(20);

        return View('tips_star.index')->with('tips', $tips);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('tips_star.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'horse' => 'required',
            'odd' => 'required',
            'trust_level' => 'required'
        ]);

        if($validation->fails()){
            return redirect()->back()->with('error', $validation->errors()->all()[0]);
        }
        else {
            $tipStar = new TipStar;
            $tipStar->horse = $request->input('horse');
            $tipStar->odd = $request->input("odd");
            $tipStar->trust_level = $request->input('trust_level');
            $tipStar->img = $request->has('storage_file')? $request->input('storage_file') : null;
            $tipStar->hour = $request->has('hour')? $request->input('hour') : null;
            $tipStar->run = $request->has('run')? $request->input('run'): null;
            $tipStar->date = $request->has('date')? $request->input('date') : null;
            $tipStar->result = $request->input('result');
            $tipStar->url = $request->input('url');
            $tipStar->createEmbedLink($request->input('url'));

            $tipStar->save();

            return redirect()->route('tipstar.index')->with('success','Tip Star salva');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipstar = TipStar::find($id);
        return View('tips_star.edit')->with('tipstar', $tipstar);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(),[
            'horse' => 'required',
            'odd' => 'required',
            'trust_level' => 'required'
        ]);

        if($validation->fails()){
            return redirect()->back()->with('error', $validation->errors()->all()[0]);
        }
        else {
            $tipStar = TipStar::find($id);
            $tipStar->horse = $request->input('horse');
            $tipStar->odd = $request->input("odd");
            $tipStar->trust_level = $request->input('trust_level');

            if($request->has('storage_file')){
                $tipStar->img = $request->input('storage_file');
            }
            
            $tipStar->hour = $request->has('hour')? $request->input('hour') : null;
            $tipStar->run = $request->has('run')? $request->input('run'): null;
            $tipStar->date = $request->has('date')? $request->input('date') : null;
            $tipStar->url = $request->input('url');
            $tipStar->result = $request->input('result');
            $tipStar->createEmbedLink($request->input('url'));

            $tipStar->save();

            return redirect()->route('tipstar.index')->with('success','Tip Star salva');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tipStar = TipStar::find($id);
        if($tipStar){
            TipStar::destroy($id);
            return redirect()->route('tipstar.index')->with('success','Tip Star Removida');
        }
        else{
            return redirect()->back()->with('error', 'Tip Star não encontrada');
        }
    }
}
