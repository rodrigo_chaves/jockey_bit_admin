<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserTest extends Model
{
    protected $table = 'users_has_tests';

    /** relations */
    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }

    public function module(){
        return $this->belongsTo('App\Model\Module','module_id','id');
    }

    /** scopes */
    public function scopeByUser($query, $user_id){
        return $query->where('user_id',$user_id);
    }

    public function scopeByModule($query, $module_id){
        return $query->where('module_id', $module_id);
    }


    /** model methods */
    public function decimalGrade(){
        return $this->grade/10;
    }
}
