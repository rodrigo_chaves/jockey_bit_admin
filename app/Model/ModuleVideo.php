<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ModuleVideo extends Model
{
    protected $table = 'module_videos';

 	
 	public function createEmbedLink($url){
 		//https://vimeo.com/29885705
        // $parts = explode('?v=',$url);
        // $this->embed = 'https://www.youtube.com/embed/' . $parts[count($parts)-1] . '?rel=0';
        if($this->isVimeo($url)){
        	$parts = explode('vimeo.com/',$url);
        	$this->embed = 'https://player.vimeo.com/video/' . $parts[count($parts)-1] . '?autoplay=1';
        }
        else if($this->isYoutube($url)){
        	$parts = explode('?v=',$url);
        	$this->embed = 'https://www.youtube.com/embed/' . $parts[count($parts)-1] . '?rel=0';
        }
        
    }

    public function isVimeo($url){
		return strpos($url,'vimeo.com/');
    }

    public function isYoutube($url){
    	return strpos($url,'?v=');
    }


    public function scopeByModule($query, $module_id){
    	return $query->where('module_id',$module_id);
    }
}
