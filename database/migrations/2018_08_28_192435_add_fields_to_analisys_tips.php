<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToAnalisysTips extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('analysis_tips',function(Blueprint $table){
            $table->integer('racing_track_id')->unsigned()->nullable();
            $table->foreign('racing_track_id')->references('id')->on('racing_tracks');
            $table->integer('distance_id')->unsigned()->nullable();
            $table->foreign('distance_id')->references('id')->on('distances');
            $table->integer('horse_id')->unsigned()->nullable();
            $table->foreign('horse_id')->references('id')->on('horses');
            $table->integer('livehorse_id')->unsigned()->nullable();
            $table->foreign('livehorse_id')->references('id')->on('live_horses');
            $table->integer('classe_id')->unsigned()->nullable();
            $table->foreign('classe_id')->references('id')->on('analysis_classes');
            $table->string('odd')->nullable();
            $table->boolean('validated')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('analysis_tips',function(Blueprint $table){
            $table->dropColumn('validated');
            $table->dropColumn('odd');
            $table->dropColumn('classe');
            $table->dropForeign(['livehorse_id']);
            $table->dropForeign(['horse_id']);
            $table->dropForeign(['distance_id']);
            $table->dropForeign(['racing_track_id']);
            $table->dropForeign(['classe_id']);
            $table->dropColumn('livehorse_id');
            $table->dropColumn('horse_id');
            $table->dropColumn('distance_id');
            $table->dropColumn('racing_track_id');
            $table->dropColumn('classe_id');
        });
    }
}
