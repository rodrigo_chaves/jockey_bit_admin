@extends('layout.panel')

@section('content')
<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h2>Nova Live Trading</h2>
          </div>
          <div class="box-divider m-0"></div>
          <div class="box-body">
            <form action="{{route('livetradings.store')}}" method="post">
                {{ csrf_field() }}
              <div class="form-group">
                <label>Titulo</label>
                <input type="text" class="form-control" name="title" id="horse" placeholder="Titulo">
              </div>

              <div class="form-group">
                <label>Url</label>
                <input type="text" class="form-control" name="url" id="run" placeholder="url">
              </div>

              <div class="form-group">
                <label>Imagem</label>
                <input type="text" class="form-control" name="picture" id="result" placeholder="Imagem">
              </div>

              <div class="form-group">
                <label><input type="checkbox" name="active" checked> Ativo</label>
                
              </div>

              <div class="form-group">
                <label><input type="checkbox" name="active_on_office" checked> Ativo no Backoffice</label>
              </div>
              
              <button type="submit" class="btn primary">Salvar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
