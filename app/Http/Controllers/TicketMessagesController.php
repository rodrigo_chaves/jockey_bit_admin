<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\TicketMessage;
use App\Model\Ticket;
use Validator;

class TicketMessagesController extends Controller
{
    public function store(Request $request){
        $validation = Validator::make($request->all(),[
            'ticket_id' => 'required',
            'msg' => 'required'
        ]);

        if($validation->fails()){
            return redirect()->back()->with('error',$validation->errors()->all()[0]);
        }
        else{
            // $status = TicketStatus::where('title','Answered')->first();
            $ticket = Ticket::find($request->input('ticket_id'));
            $ticket->changeStatus('Answered');
            // $ticket->status_id = $status->id;
            

            $admin = $request->session()->get('admin');
            $message = new TicketMessage;
            $message->admin_id = $admin->id;
            $message->msg = $request->input("msg");
            $message->ticket_id = $request->input('ticket_id');
            $message->save();

            return redirect()->route('tickets.show',['id'=>$request->input('ticket_id')])->with('success',"Resposta enviada");
        }
    }
}
