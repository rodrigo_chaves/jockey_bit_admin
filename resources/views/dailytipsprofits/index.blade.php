@extends('layout.panel')

@section('content')

<div class="content-main " id="content-main">
  <div class="padding">
  
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>

    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Plataforma do Professor</a></li>
          <li class="breadcrumb-item active">Lucro das tips</li></ol>
        <a href="{{route('dailytipsprofits.create')}}" class="btn btn-fw primary" style="margin-bottom: 40px;">
          <i class="fa fa-plus"></i> Adicionar Lucro de Tip</a>
        <table id="" class="table v-middle p-0 m-0 box dataTable no-footer" data-plugin="dataTable" role="grid" aria-describedby="datatable_info">
          <thead>
            <tr>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Dia</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Tipo</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Porcentagem</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Tips distribuidas</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Tips vencedoras</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Tips perderoras</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;"></td>
            </tr>
          </thead>
          <tbody>
            @foreach($dailytipsprofits as $profit)
              <tr>
                  <td align="center">{{$profit->due_date}}</td>
                  <td align="center">{{$profit->type->name}}</td>
                  <td align="center">{{$profit->profit}}</td>
                  <td align="center">{{$profit->sent_tips}}</td>
                  <td align="center">{{$profit->winner_tips}}</td>
                  <td align="center">{{$profit->loose_tips}}</td>
                  <td align="right">
                    <form action="{{route('dailytipsprofits.destroy',['profit'=>$profit->id])}}" method="post">
                      <input type="hidden" name="_method" value="delete">
                      {{csrf_field()}}
                      <a href="{{route('dailytipsprofits.edit',['id'=>$profit->id])}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                      <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>
                    </form>
                  </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        {{$dailytipsprofits->links()}}
      </div>
    </div>
  </div>
</div>
@endsection
