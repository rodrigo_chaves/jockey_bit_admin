<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

use App\Model\TicketStatus;

class Ticket extends Model
{
    protected $table = 'tickets';

    public function status(){
        return $this->belongsTo('App\Model\TicketStatus','status_id','id');
    }

    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }

    public function messages(){
        return $this->hasMany('App\Model\TicketMessage','ticket_id','id');
    }


    public function scopeByUser($query, $user_id){
        return $query->where('user_id',$user_id);
    }

    public function changeStatus($status_name){
        $status = TicketStatus::where('title',$status_name)->first();
        if($status){
            $this->status_id = $status->id;
            $this->save();
        }
    }

    public function mayClose(){
        return ($this->status->title=="Open" || $this->status->title=="Answered");
    }

    public function mayAnswer(){
         return ($this->status->title=="Open" || $this->status->title=="Answered");
    }

    public function mayHide(){
        return ($this->status->title=="Closed");
    }

    public function scopeNotHidden($query){
        return $query->where('hidden_for_admin',false);
    }

    public function scopeOpen($query){
        return $query->where('status_id',1);
    }

    public function scopeAnswered($query){
        return $query->where('status_id',2);
    }
}
