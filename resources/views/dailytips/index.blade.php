@extends('layout.panel')

@section('content')
<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-sm-12">
            <ol class="breadcrumb"><li class="breadcrumb-item"><a href="#">Plataforma do Professor</a></li><li class="breadcrumb-item active">Tips do dia</li></ol>
            <a href="{{route('dailytips.create')}}" class="btn btn-fw primary" style="margin-bottom: 40px;"><i class="fa fa-plus"></i> Adicionar Tips do dia</a>
            @if($is_able_to_export)
              <a href="{{route('dailytips.export')}}" class="btn btn-fw warning" style="margin-bottom: 40px;"><i class="fa fa-link"></i> Exportar</a>
            @endif
        <table id="" class="table v-middle p-0 m-0 box dataTable no-footer" data-plugin="dataTable" role="grid" aria-describedby="datatable_info" style="width: 1094px;">
          <thead>
            <tr>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Resumo</td>
              
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Tipo</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Resultado</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Win/Lose</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Ações</td>
            </tr>
          </thead>
          <tbody>
            @foreach($dailytips as $dailytip)
              <tr>
                <td align="center">{{$dailytip->resume}}</td>
                <td align="center">{{$dailytip->type!=null? $dailytip->type->name : ''}}</td>
                <td align="center">
                  <div class="row">
                    <div class="col-lg-3">
                      <i id="result_{{$dailytip->id}}" class="fa {{($dailytip->result!=null) ?  'fa-check text-success' : 'fa-times text-danger'}}"></i>
                    </div>
                    <div class="col-lg-9">
                      <div class="form-group">
                        <select name="result" data-tip="{{$dailytip->id}}" class="form-control">
                          <option value="null"></option>
                        @foreach(__('numbers.positions') as $position)
                          <option value="{{$position}}" {{($dailytip->result==$position)? 'selected' : ''}}>{{$position}}</option>
                        @endforeach
                        </select>
                      </div>
                    </div>
                  </div>
                  
                </td>
                <td align="center">
                  <div class="row">
                    <div class="col-lg-3">
                      <i id="win_lose_{{$dailytip->id}}" class="fa {{($dailytip->win_lose!=null) ?  'fa-check text-success' : 'fa-times text-danger'}}"></i>
                    </div>
                    <div class="col-lg-9">
                      <div class="form-group">
                        <select name="win_lose" data-tip="{{$dailytip->id}}" class="form-control">
                          <option value="null"></option>
                          <option value="0" {{$dailytip->win_lose == 0 ? 'selected' : ''}}>LOSE</option>
                          <option value="1" {{$dailytip->win_lose == 1 ? 'selected' : ''}}>WIN</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  
                </td>
                <td>
                  <form action="{{route('dailytips.destroy',['dailytip'=>$dailytip->id])}}" method="post">
                    <input type="hidden" name="_method" value="delete" />
                    {!! csrf_field() !!}
                    <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>
                  </form>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <div class="col-xs-12">
        {{$dailytips->links()}}
      </div>
    </div>
  </div>
  @include('layout.elements.footer')
</div>
@endsection

@section('js')
<script type="text/javascript">
  $(document).ready(function(){
    $('select[name=win_lose]').on('change',function(){
      var id = $(this).data('tip');
      var win_lose = $(this).val();
      if(win_lose != 'null'){
        $('#win_lose_'+id).removeClass('fa-times');
        $('#win_lose_'+id).removeClass('fa-check');
        $('#win_lose_'+id).removeClass('text-danger');
        $('#win_lose_'+id).addClass('fa-circle-o-notch fa-spin text-success');
        $.ajax({
          url : '/api/dailytips/' + id,
          type : 'POST',
          data:{
            '_method' : 'put',
            'win_lose' : win_lose
          }
        })
        .done(function(data){
          if(data.id != undefined){
            $('#win_lose_'+id).removeClass('fa-circle-o-notch');
            $('#win_lose_'+id).removeClass('fa-spin');
            $('#win_lose_'+id).addClass('fa-check');
          }
        });
      }
      
    });

    $('select[name=result]').on('change',function(){
      var id = $(this).data('tip');
      var result = $(this).val();
      if(result != 'null'){
        $('#result_'+id).removeClass('fa-times');
        $('#result_'+id).removeClass('fa-check');
        $('#result_'+id).addClass('fa-circle-o-notch fa-spin');
        $.ajax({
          url : '/api/dailytips/' + id,
          type : 'POST',
          data:{
            '_method' : 'put',
            'result' : result
          }
        })
        .done(function(data){
          if(data.id != undefined){
            $('#result_'+id).removeClass('fa-circle-o-notch');
            $('#result_'+id).removeClass('fa-spin');
            $('#result_'+id).removeClass('text-danger');
            $('#result_'+id).addClass('fa-check');
            $('#result_'+id).addClass('text-success');
          }
        });
      }
      
    });
  });
</script>
@endsection