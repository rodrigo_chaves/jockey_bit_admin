@extends('layout.panel')

@section('content')

<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <ol class="breadcrumb"><li class="breadcrumb-item"><a href="#">Plataforma do Professor</a></li><li class="breadcrumb-item active">Comunicado Oficial</li></ol>
        <a href="{{route('official_notice.create')}}" class="btn btn-fw primary" style="margin-bottom: 40px;"><i class="fa fa-plus"></i> Adicionar novo comunicado</a>
        <table id="" class="table v-middle p-0 m-0 box dataTable no-footer">
          <thead>
            <tr>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Conteudo do comunicado</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Data e horario</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Status</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Acoes</td>
              
            </tr>
          </thead>
          <tbody>
                @foreach($official_notices as $official_notice)
                <tr>
                  <td align="center">{{$official_notice->msg}}</td>
                  <td align="center">{{$official_notice->created_at}}</td>
                  <td align="center">{{$official_notice->active? 'Ativo' : 'Inativo'}}</td>
                  <td align="right">
                    
                    <form action="{{route('official_notice.destroy',['id'=>$official_notice->id])}}" method="post">
                      {{csrf_field()}}

                      <input type="hidden" name="_method" value="delete">
                      
                      @if($official_notice->active)
                        <a class="btn btn-warning" href="{{route('deactivate_official_notice',['id'=>$official_notice->id])}}" style="color:#fff"><i class="fa fa-power-off"></i></a>
                      @endif


                      <a href="{{route('official_notice.edit',['id'=>$official_notice->id])}}" class="btn btn-primary" style="color:#fff"><i class="fa fa-pencil"></i></a>

                      <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>

                      
                    </form>
                  </td>
                </tr>
                @endforeach
          </tbody>
        </table>

      </div>
    </div>
  </div>
  @include('layout.elements.footer')
</div>
@endsection
