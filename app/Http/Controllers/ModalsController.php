<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Modal;

class ModalsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modals = Modal::latest()->paginate(10);

        return View('modals.index')->with('modals',$modals);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('modals.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $modal = new Modal;
        $modal->title = $request->has('title')? $request->input('title') : null;
        $modal->body = $request->has('body')? $request->input('body') : null;
        $modal->active = $request->has('active')? true : false;

        if($modal->active){
            Modal::where('active',true)->update(['active'=>false]);
        }

        $modal->save();

        return redirect()->route('modals.index')->with('success','Modal adicionado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $modal = Modal::find($id);

        return View('modals.edit')->with('modal',$modal);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $modal = Modal::find($id);

        $modal->body = $request->input('msg');
        $modal->title = $request->input('title');
        $modal->active = $request->has('active')? true : false;

        if($request->has('active')){
            Modal::where('active',true)->update(['active'=>false]);
        }

        $modal->save();

        return redirect()->route('modals.index')->with('success','Modal atualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Modal::destroy($id);

        return redirect()->route('modals.index')->with('success','Modal removido');
    }

    public function deactivate($id){
        $modal = Modal::find($id);
        $modal->active = false;
        $modal->save();

        return redirect()->route('modals.index')->with('success','Modal desativado');
    }
}
