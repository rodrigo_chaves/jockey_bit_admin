@extends('layout.panel')

@section('content')
<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h2>Novo Modal</h2>
          </div>
          <div class="box-divider m-0"></div>
          <div class="box-body">
            <form action="{{route('modals.store')}}" method="post">
                {{ csrf_field() }}
              
              <div class="checkbox"><label class="ui-check ui-check-md"><input type="checkbox" name="active" id="active"> <i class="dark-white"></i> Ativo</label></div>

              <div class="form-group">
                <label>Ttulo</label>
                <input type="text" name="title" id="title" class="form-control">
              </div>
                
              <div class="form-group">
                <label>Corpo</label>
                <textarea id="body" name="body" class="form-control"></textarea>
              </div>

            
              <button type="submit" class="btn primary">Salvar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="/template/libs/tinymce/tinymce.min.js"></script>
<script>tinymce.init({ selector:'#body' });</script>

@endsection
