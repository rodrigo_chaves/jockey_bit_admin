@extends('layout.panel')

@section('content')

<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Plataforma do Professor</a></li>
          <li class="breadcrumb-item active">Ticket</li>
          </ol>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="box">
          <div class="box-header">
            <h2>
            {{$ticket->title}}
            </h2>
          </div>
          @if($ticket->mayClose())
          <div class="box-tool">
            <a href="{{route('close_ticket',['id'=>$ticket->id])}}" class="btn btn-danger float-right">Fechar ticket</a>
          </div>
          @endif
        </div>

        @foreach($ticket->messages as $message)
          <div class="box">
            <div class="box-header">
              {{$message->user_id? '#'.$message->user->id . ' ' .$message->user->name : 'Resposta do admin'}}
              <span class="float-right">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$message->created_at)->diffForHumans()}}</span>
            </div>
            <div class="box-body">
              <strong>{{$message->msg}}</strong>
            </div>
          </div>
        @endforeach

        @if($ticket->mayAnswer())
        <div class="box">
          <div class="box-body">
            <form action="{{route('ticketmessages.store')}}" method="post">
              {{csrf_field()}}
              <input type="hidden" name="ticket_id" value="{{$ticket->id}}">
              <div class="form-group">
                <textarea name="msg" class="form-control" placeholder="Envie uma Resposta"></textarea>
              </div>
              <button class="btn btn-success">Enviar</button>
            </form>
          </div>
        </div>
        @endif
      </div>
    </div>
  </div>
  @include('layout.elements.footer')
</div>
@endsection
