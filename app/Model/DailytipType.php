<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DailytipType extends Model
{
    protected $table = 'dailytips_types';

    protected $fillable = ['name'];
}
