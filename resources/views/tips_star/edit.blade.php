@extends('layout.panel')

@section('content')
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">

<link rel="stylesheet" href="/plugins/jquery-bar-rating/dist/themes/fontawesome-stars-o.css">
<link rel="stylesheet" href="/template/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('error'))
          <div class="alert danger">
            <b>{{Session::get('error')}}</b>
          </div>
        @endif
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/">Plataforma do Professor</a></li>
          <li class="breadcrumb-item"><a href="{{route('tipstar.index')}}">Tip Stars</a></li>
          <li class="breadcrumb-item active">Editar Tip Star</li>
        </ol>
        <div class="box">
          <div class="box-header">
            <h2>Editar Tip Star</h2>
          </div>
          <div class="box-divider m-0"></div>
          <div class="box-body">
            <form action="{{route('tipstar.update',['tipstar' => $tipstar->id])}}" method="post" enctype="multipart/form-data">
              <input type="hidden" name="_method" value="put">
                {{ csrf_field() }}
              <div class="form-group">
                <label>Cavalo</label>
                <input type="text" class="form-control" name="horse" id="horse" placeholder="Cavalo" value="{{$tipstar->horse}}">
              </div>
              <div class="row">
                <div class="col-lg-3">
                  <div class="form-group">
                    <label>Data</label>
                    <input type="text" name="date" placeholder="2018-09-12" class="form-control date" value="{{$tipstar->date}}">
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group">
                    <label>Hora</label>
                    <input type="text" name="hour" placeholder="00:00" class="form-control time" value="{{$tipstar->hour}}">
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="form-group">
                    <label>Corrida</label>
                    <input type="text" name="run" placeholder="Corrida" class="form-control" value="{{$tipstar->run}}">
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-4">
                    <div class="form-group">
                      <label>Odd</label>
                      <input type="text" class="form-control" name="odd" placeholder="odd" value="{{$tipstar->odd}}">
                    </div>
                </div>

                <div class="col-lg-3">
                  <div class="form-group">
                    <label>Nivel de confiança</label>
                    <select name="trust_level">
                      @for($i=1 ; $i < 6; $i++)
                        <option value="{{$i}}" {{($tipstar->trust_level == $i)? 'selected' : ''}} >{{$i}}</option>
                      @endfor
                    </select>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group">
                    <label>Resultado</label>
                    <input type="text" name="result" class="form-control" value="{{$tipstar->result}}">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <label>Video</label>
                  <input type="text" name="url" class="form-control" placeholder="https://vimeo.com/29885705" value="{{$tipstar->url}}">
                </div>
              </div>
              <div class="row">
                <div class="col-lg-3">
                    <div class="form-group">
                      <img class="img-fluid img-thumbnail" id="preview_img" src="{{$tipstar->img}}">
                      <label>Imagem</label>
                      
                      <div class="form-file">
                        <input type="hidden" name="storage_file">
                        <input type="file" name="img"> <button class="btn white">Select file ...</button>
                      </div>
                    </div>
                </div>
              </div>
              <button type="submit" class="btn primary">Salvar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
<script src="https://www.gstatic.com/firebasejs/4.10.1/firebase.js"></script>

<script src="/plugins/jquery-bar-rating/dist/jquery.barrating.min.js"></script>

<script src="/template/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">
// Set the configuration for your app
  $(document).ready(function(){
    var config = {
      apiKey: "AIzaSyCpEP6yWsE1fv8vnKfd-ELoAdB_mFJctHQ",
      authDomain: "academy-jockeybit.firebaseapp.com",
      databaseURL: "https://<DATABASE_NAME>.firebaseio.com",
      storageBucket: "gs://academy-jockeybit.appspot.com/",
      messagingSenderId: "<SENDER_ID>",
    };
    firebase.initializeApp(config);

    // Get a reference to the storage service, which is used to create references in your storage bucket
    var storage = firebase.storage().ref();
    var timestamp = Number(new Date());
    var pdfsRef = storage.child('imgs/' + timestamp + ".png");

    $('input[name=img]').change(function(){
      var file_data = $('input[name=img]').prop('files')[0];
      pdfsRef.put(file_data).then(function(snapshot) {
        console.log('Uploaded a blob or file!');
        // console.log(snapshot);
        $('input[name=storage_file]').val(snapshot.downloadURL);
        $('button[type=submit]').removeAttr('disabled');
        $('#preview_img').attr('src', snapshot.downloadURL);
      });
    });

  });

  $(function() {
      $('select[name=trust_level]').barrating({
        theme: 'fontawesome-stars-o'
      });

      $('.date').datepicker({
        format: 'yyyy-mm-dd',
      });
   });

  
</script>
@endsection
