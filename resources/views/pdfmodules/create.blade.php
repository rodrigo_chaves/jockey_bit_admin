@extends('layout.panel')

@section('content')
<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h2>Novo modulo</h2>
          </div>
          <div class="box-divider m-0"></div>
          <div class="box-body">
            <form action="{{route('pdf.store',['module_id'=>$module_id])}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
              <div class="form-group">
                <label>Titulo</label>
                <input type="text" class="form-control" name="title" id="title" placeholder="Titulo">
              </div>

              <div class="form-group">
                <label>Descripção</label>
                <textarea class="form-control" name="description"></textarea>
              </div>

              <div class="form-group">
                <label>Arquivo</label>
                <input type="file" name="pdf" class="form-control" accept=".pdf, .PDF">
                <input type="hidden" name="storage_file">
              </div>

              <!-- <div class="form-group">
                <label>Imagem</label>
                <div class="form-file">
                  <input type="file" name="picture"> <button class="btn white">Select file ...</button>
                </div>
              </div> -->
              
              <button type="submit" class="btn primary" disabled>Salvar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="https://www.gstatic.com/firebasejs/4.10.1/firebase.js"></script>

<script type="text/javascript">
// Set the configuration for your app
  $(document).ready(function(){
    var config = {
      apiKey: "AIzaSyCpEP6yWsE1fv8vnKfd-ELoAdB_mFJctHQ",
      authDomain: "academy-jockeybit.firebaseapp.com",
      databaseURL: "https://<DATABASE_NAME>.firebaseio.com",
      storageBucket: "gs://academy-jockeybit.appspot.com/",
      messagingSenderId: "<SENDER_ID>",
    };
    firebase.initializeApp(config);

    // Get a reference to the storage service, which is used to create references in your storage bucket
    var storage = firebase.storage().ref();
    var timestamp = Number(new Date());
    var pdfsRef = storage.child('pdfs/' + timestamp + ".pdf");

    $('input[name=pdf]').change(function(){
      var file_data = $('input[name=pdf]').prop('files')[0];
      pdfsRef.put(file_data).then(function(snapshot) {
        // console.log('Uploaded a blob or file!');
        // console.log(snapshot);
        $('input[name=storage_file]').val(snapshot.downloadURL);
        $('button[type=submit]').removeAttr('disabled');
      });
    });

  });
  // var config = {
  //   apiKey: ' AIzaSyCpEP6yWsE1fv8vnKfd-ELoAdB_mFJctHQ',
  //   authDomain: '<your-auth-domain>',
  //   databaseURL: '<your-database-url>',
  //   storageBucket: 'gs://academy-jockeybit.appspot.com/'
  // };
  // firebase.initializeApp(config);

  
</script>

@endsection
