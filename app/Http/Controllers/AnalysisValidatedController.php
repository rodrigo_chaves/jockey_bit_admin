<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\AnalyzeTip;

class AnalysisValidatedController extends Controller
{
    public function index(Request $request){
    	$analysis_tips = AnalyzeTip::where('validated',true)->paginate(20);

        return View('analysistips.index')->with('analysis_tips',$analysis_tips);
    }
}
