@extends('layout.panel')

@section('content')
<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h2>Novo modulo</h2>
          </div>
          <div class="box-divider m-0"></div>
          <div class="box-body">
            <form action="{{route('modules.update',['module'=>$module->id])}}" method="post" enctype="multipart/form-data">
              <input type="hidden" name="_method" value="put">
                {{ csrf_field() }}
              <div class="form-group">
                <label>Titulo</label>
                <input type="text" class="form-control translate" name="title" id="title" placeholder="Titulo" value="{{$module->title}}">
              </div>

              <div class="form-group">
                <label>Descripção</label>
                <textarea class="form-control" id="msg" name="description">{{$module->description}}</textarea>
              </div>

              <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" name="active" id="active" {{$module->active? 'checked' : ''}}>
                <label class="form-check-label" for="active">Ativo</label>
              </div>

              <!-- <div class="form-group">
                <label>Imagem</label>
                <div class="form-file">
                  <input type="file" name="picture"> <button class="btn white">Select file ...</button>
                </div>
              </div> -->
              
              <button type="submit" class="btn success">Atualizar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="/template/libs/tinymce/tinymce.min.js"></script>
<script>tinymce.init({ selector:'#msg' });</script>

@endsection
