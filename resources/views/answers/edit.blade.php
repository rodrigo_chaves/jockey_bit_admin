@extends('layout.panel')

@section('content')
<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h2>Nova resposta</h2>
          </div>
          <div class="box-divider m-0"></div>
          <div class="box-body">
            <form action="{{route('answers.update',['id'=>$answer->id])}}" method="post">
              <input type="hidden" name="_method" value="put">
              {{ csrf_field() }}
              <div class="form-group">
                <label>Titulo</label>
                <input type="text" class="form-control translate" name="title" id="title" placeholder="Titulo" value="{{$answer->title}}">
              </div>
              <div class="form-group">
                <label><input type="checkbox" name="correct" {{$answer->correct? 'checked':''}}> Resposta correta</label>
              </div>

              <button type="submit" class="btn success">Atualizar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
