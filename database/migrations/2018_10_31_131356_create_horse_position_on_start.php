<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorsePositionOnStart extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horse_positions_start', function(Blueprint $table){
            $table->increments('id');
            $table->date('date');
            $table->time('time');
            $table->integer('racing_track_id')->unsigned();
            $table->foreign('racing_track_id')->references('id')->on('racing_tracks');
            $table->integer('horse_id')->unsigned();
            $table->foreign('horse_id')->references('id')->on('horses');
            $table->integer('front')->nullable();
            $table->integer('stalker')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horse_positions_start');
    }
}
