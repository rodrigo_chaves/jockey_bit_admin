<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStartipsProfitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('startips_profits', function(Blueprint $table){
            $table->increments('id');
            $table->decimal('profit',10,2);
            $table->decimal('accumulated_profit',10,2)->nullable();
            $table->date('due_date');
            $table->integer('sent_tips')->nullable();
            $table->integer('winner_tips')->nullable();
            $table->integer('loose_tips')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('startips_profits');
    }
}
