<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\StringLanguage;

class ContentString extends Model
{
    protected $table = 'content_strings';

    public function language(){
        return $this->belongsTo('App\Model\StringLanguage','language_id','id');
    }

    public function create_key(){
    	$this->key = str_slug(strtolower($this->text), '.');
    }

    public function scopeByKey($query, $key){
    	return $query->where('key',$key);
    }

    public function scopeByLang($query, $lang){
    	$lang_id = StringLanguage::where('nickname', $lang)->pluck('id');

    	return $query->where('language_id',$lang_id);

    }

    public function scopeByText($query, $text){
        return $query->where('text', $text);
    }
}
