<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::match(['get','post'],'/login','AdminsController@login')->name('login');

Route::group(['middleware'=> 'admin.session'], function(){
    Route::prefix('/')->group(function(){
        Route::get('/','DashboardController@index')->name('dashboard');
        Route::get('/operations','OperationsController@index')->name('operations');
    
        Route::get('/daily_new/{d}/edit','DailyNewsController@edit');
        Route::post('/daily_new/{d}/edit','DailyNewsController@update');
    
        Route::prefix('operations')->group(function(){
            Route::get('/','OperationsController@index')->name('operations');
            Route::get('/create','OperationsController@create')->name('create_operation');
            Route::post('/store','OperationsController@store')->name('store_operation');
            Route::get('/{id}','OperationsController@edit')->name('operations.edit');
            Route::put('/{id}','OperationsController@update')->name('operations.update');
            Route::delete('/{id}','OperationsController@destroy')->name('operations.destroy');
        });
    
        Route::resource('dailytips','DailyTipsController');

        Route::resource('dailytipsprofits','DailytipsProfitController');

        Route::get('/dailytipsexport','DailyTipsController@export')->name('dailytips.export');
    
        Route::resource('livetradings','LiveTradingsController');
    
        Route::resource('operations_history','OperationsHistoryController');
    
        Route::resource('next_live','NextLiveController');
    
        Route::resource('week_class','WeekClassController');

        Route::resource('big_week_class','BigWeekClassController');
    
        Route::resource('official_notice','OfficialNoticeController');

        Route::resource('dailytips_notice','DailyTipNoticesController');

        Route::get('/dailytips_notice/deactivate/{id}','DailyTipNoticesController@deactivate')->name('deactivate_dailytips_notice');
    
        Route::get('/official_notice/deactivate/{id}','OfficialNoticeController@deactivate')->name('deactivate_official_notice');

        Route::resource('modules','ModulesController');

        Route::prefix('questions')->group(function(){
            Route::get('/{module}','ModuleQuestionsController@index')->name('questions.index');
            Route::get('/{module}/create','ModuleQuestionsController@create')->name('questions.create');
            Route::post('/{module}','ModuleQuestionsController@store')->name('questions.store');
            Route::get('/{question}/edit','ModuleQuestionsController@edit')->name('questions.edit');
            Route::put('/{question}','ModuleQuestionsController@update')->name('questions.update');
            Route::delete('/{question}','ModuleQuestionsController@destroy')->name('questions.destroy');
        });

        Route::prefix('answers')->group(function(){
            Route::get('/{question}','AnswersController@index')->name('answers.index');
            Route::get('/{question}/create','AnswersController@create')->name('answers.create');
            Route::post('/{question}','AnswersController@store')->name('answers.store');
            Route::get('/{answer}/edit','AnswersController@edit')->name('answers.edit');
            Route::put('/{answer}','AnswersController@update')->name('answers.update');
            Route::delete('/{answer}','AnswersController@destroy')->name('answers.destroy');
        });

        Route::resource('modals','ModalsController');

        Route::get('/modals/deactivate/{id}','ModalsController@deactivate')->name('deactivate_modal');

        Route::prefix('modulevideos')->group(function(){
            Route::get('/{module}','ModuleVideosController@index')->name('modulevideos.index');
            Route::get('/{module}/create','ModuleVideosController@create')->name('modulevideos.create');
            Route::post('/{module}','ModuleVideosController@store')->name('modulevideos.store');
            Route::get('/{modulevideo}/edit','ModuleVideosController@edit')->name('modulevideos.edit');
            Route::delete('/{modulevideo}','ModuleVideosController@destroy')->name('modulevideos.destroy');
            Route::put('/{modulevideo}','ModuleVideosController@update')->name('modulevideos.update');
        });

        Route::resource('{module_id}/pdf','PdfModulesController');
        Route::resource('languages','StringLanguagesController');

        Route::prefix('contentstrings')->group(function(){
            Route::post("/",'ContentStringsController@store')->name('contentstrings.store');
        });

        Route::resource('nextophistory','NextOpHistoryController');

        Route::resource('tickets','TicketsController');
        Route::get('tickets/close/{id}','TicketsController@close')->name('close_ticket');
        Route::get('/tickets/hide/{id}','TicketsController@hide')->name('hide_ticket');

        Route::resource('ticketmessages','TicketMessagesController');

        Route::resource('noticevideos','DailyTipNoticeVideosController');

        Route::get('/noticevideos/deactivate/{id}','DailyTipNoticeVideosController@deactivate')->name('deactivate_noticevideo');

        Route::resource('bigbets','BigBetsController');

        Route::resource('racingtracks','RacingTracksController');

        Route::resource('horses','HorsesController');

        Route::resource('livehorses','LiveHorsesController');

        Route::resource('distances','DistancesController');

        Route::resource('analysis','AnalysisController');

        Route::resource('analysistips','AnalysisTipsController');

        Route::get('/validatetip/{id}','AnalysisTipsController@validateTip')->name('validatetip');

        Route::resource('classes','AnalysisClassesController');

        Route::resource('chats','Chat\ChatsController');

        Route::get('analysisvalidated','AnalysisValidatedController@index')->name('analysisvalidated');

        Route::resource('horsepositions','HorsePositionsController');

        Route::resource('usertests','UserTestsController');

        // Route::get('/{chat_id}/messages','Chat\MessagesController@fetch');
        // Route::post('/{chat_id}/messages','Chat\MessagesController@sentMessage');

        Route::resource('tipstar','TipStarsController');
        Route::resource('racingtracksguides','RacingTrackGuidesController');

        Route::prefix('systeminfo')->group(function(){
            Route::get('/{key}','SystemInfosController@show');
            Route::put('/', 'SystemInfosController@update')->name('systeminfo.update');
        });

        Route::resource('startipvideos','StarTipVideosController');

        Route::resource('avulsevideos','AvulseVideosController');

        Route::resource('startipsprofits', 'StarTipsProfitsController');
    });
});

