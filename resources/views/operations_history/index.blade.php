@extends('layout.panel')

@section('content')

<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <ol class="breadcrumb"><li class="breadcrumb-item"><a href="#">Plataforma do Professor</a></li><li class="breadcrumb-item active">Extrato de Operações</li></ol>
        <a href="{{route('operations_history.create')}}" class="btn btn-fw primary" style="margin-bottom: 40px;"><i class="fa fa-plus"></i> Adicionar nova Operação</a>
        <table id="" class="table v-middle p-0 m-0 box dataTable no-footer" data-plugin="dataTable" role="grid" aria-describedby="datatable_info" style="width: 1094px;">
          <thead>
            <tr>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Titulo</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">URL</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Ação</td>
            </tr>
          </thead>
          <tbody>
            @foreach($operations_history as $operation_history)
              <tr>
                  <td align="center">{{$operation_history->title}}</td>
                  <td align="center">{{$operation_history->url}}</td>
                  <td align="right">
                    <form action="{{route('operations_history.destroy',['id'=>$operation_history->id])}}" method="post">
                      {{csrf_field()}}

                      <input type="hidden" name="_method" value="delete">

                      <a href="{{route('operations_history.edit',['id'=>$operation_history->id])}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>

                      <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>

                    </form>
                  </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  @include('layout.elements.footer')
</div>
@endsection
