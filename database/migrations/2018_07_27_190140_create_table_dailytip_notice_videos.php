<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDailytipNoticeVideos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dailytip_notice_videos',function(Blueprint $table){
            $table->increments('id');
            $table->boolean('active');
            $table->integer('dailytips_types_id')->unsigned()->nullable();
            $table->foreign('dailytips_types_id')->references('id')->on('dailytips_types');
            $table->string('title')->nullable();
            $table->string('msg')->nullable();
            $table->string('url')->nullable();
            $table->string('embed_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dailytip_notice_videos');
    }
}
