<?php

use Illuminate\Database\Seeder;
use App\Model\DailyTip;

class SetResumeOnDailytips extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dailytips = DailyTip::whereNull('resume')->get();
        foreach($dailytips as $dailytip){
            //Chepstow 13:15h / Cavalo Foresee odd
            $dailytip->resume = $dailytip->run ." " . $dailytip->due_date . " / " . $dailytip->horse . " " . $dailytip->odd;
            $dailytip->save();
        }
    }
}
