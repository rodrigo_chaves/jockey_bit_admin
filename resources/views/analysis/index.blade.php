@extends('layout.panel')

@section('content')

<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Plataforma do Admin</a></li>
          <li class="breadcrumb-item active">analises</li>
        </ol>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
          <!-- <a href="{{route('analysis.create')}}" class="btn btn-fw primary" style="margin-bottom: 40px;"><i class="fa fa-plus"></i> Adicionar cavalo</a> -->
          <div class="box">
            <div class="box-body">
              <form action="{{route('analysis.store')}}" method="post" class="form-inline">
                {{ csrf_field() }}
                <div class="form-group">
                  <select class="form-control" name="racing_track_id">
                    @foreach($tracks as $track)
                      <option value="{{$track->id}}">{{$track->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group mx-sm-2">
                  <select class="form-control" name="distance_id">
                    @foreach($distances as $distance)
                      <option value="{{$distance->id}}">{{$distance->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <select class="form-control" name="horse_id">
                    @foreach($horses as $horse)
                      <option value="{{$horse->id}}">{{$horse->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group mx-sm-2">
                  <select class="form-control" name="livehorse_id">
                    @foreach($live_horses as $livehorse)
                      <option value="{{$livehorse->id}}">{{$livehorse->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group mx-sm-2">
                    <select name="classe_id" class="form-control">
                      @foreach($classes as $classe)
                      <option value="{{$classe->id}}">{{$classe->name}}</option>
                      @endforeach
                    </select>
                  </div>
                <div class="form-group">
                  <input type="text" name="odd" placeholder="odd" class="form-control">
                </div>
                

                <div class="form-group mx-sm-2">
                  <button class="btn btn-success">Salvar</button>
                </div>
              </form>
            </div>
          </div>

        <table id="" class="table v-middle p-0 m-0 box dataTable no-footer">
          <thead>
            <tr>
              <!-- <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">#</td> -->
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Data</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Pista</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Distancia</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Cavalo</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Cavalo Live</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">ODD</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Classe</td>
            </tr>
          </thead>
          <tbody>
            @foreach($analysis as $analyse)
              <tr>
                  
                  <td align="center">{{date('d/m H:i',strtotime($analyse->created_at))}}</td>
                  <td align="center">{{$analyse->track->name}}</td>
                  <td align="center">{{$analyse->distance->name}}</td>
                  <td align="center">{{$analyse->horse->name}}</td>
                  <td align="center">{{$analyse->live_horse->name}}</td>
                  <td align="center">{{$analyse->odd}}</td>
                  <td align="center">{{($analyse->classe)? $analyse->classe->name : ''}}</td>
                  <td align="right">
                    <form action="{{route('analysis.destroy',['id'=>$analyse->id])}}" method="post">
                      {{csrf_field()}}

                      <input type="hidden" name="_method" value="delete">
                      <a href="{{route('analysis.edit',['id'=>$analyse->id])}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                      
                      <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>
                    </form>
                  </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        {{$analysis->links()}}
      </div>
    </div>
  </div>
</div>
@endsection
