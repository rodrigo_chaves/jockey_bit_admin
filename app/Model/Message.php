<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'messages';

    protected $casts = ['is_admin'=>'boolean','is_user'=>'boolean'];

    protected $with = ['user','admin'];

    public function admin(){
        return $this->belongsTo('App\Model\Admin','user_id','id');
    }

    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }
}
