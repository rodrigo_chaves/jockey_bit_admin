@extends('layout.panel')

@section('content')

<div class="content-main " id="content-main">
  <div class="padding">
  
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>

    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <h1 class="display-4 l-s-n-1x">

          <a href="{{route('modules.create')}}" class="btn btn-fw primary"><i class="fas fa-plus"></i> Novo Modulo</a>
        </h1>
        <table id="" class="table v-middle p-0 m-0 box dataTable no-footer" data-plugin="dataTable" role="grid" aria-describedby="datatable_info" style="width: 1094px;">
          <thead>
            <tr>
              <td align="center">Titulo</td>
              <!-- <td align="center" width="550">Descrição</td> -->
              <td align="center">Questões</td>
              <td align="center">Ativo</td>
              <td align="center"></td>
            </tr>
          </thead>
          <tbody>
            @foreach($modules as $module)
              <tr>
                  <td align="center">{{$module->title}}</td>
                  <td align="center">{{$module->questions->count()}}</td>
                  <td align="center">
                    @if($module->active)
                      <span class="badge badge-success">Sim</span>
                    @else
                      <span class="badge badge-danger">Não</span>
                    @endif
                  </td>
                  <td align="right">
                    <form action="{{route('modules.destroy',['module'=>$module->id])}}" method="post">
                      <input type="hidden" name="_method" value="delete">
                      {{csrf_field()}}
                      <a href="{{route('modulevideos.index',['module'=>$module->id])}}" class="btn green btn-circle" title="Videos">
                        <i class="fa fa-play"></i>
                      </a>
                      <a href="{{route('pdf.index',['module_id'=>$module->id])}}" title="PDFs" class="btn primary btn-circle">
                        <i class="fas fa-file-pdf"></i>
                      </a>
                      <a href="{{route('questions.index',['module'=>$module->id])}}" class="btn purple btn-circle" title="Prova">
                          <i class="fas fa-list-ul"></i>
                      </a>
                      <a href="{{route('modules.edit',['id'=>$module->id])}}" class="btn btn-primary btn-circle">
                        <i class="fas fa-pen"></i>
                      </a>
                      <button type="submit" class="btn btn-danger btn-circle">
                        <i class="far fa-trash-alt"></i>
                      </button>
                    </form>
                  </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        {{$modules->links()}}
      </div>
    </div>
  </div>
</div>
@include('layout.elements.footer')

@endsection
