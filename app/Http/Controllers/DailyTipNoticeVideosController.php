<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\DailytipType;
use App\Model\DailyTipNoticeVideo;

class DailyTipNoticeVideosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $noticevideos = DailyTipNoticeVideo::paginate(10);
        return View('notice_videos.index')->with('noticevideos',$noticevideos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dailytips_types = DailytipType::get();
        return View('notice_videos.create')->with('types',$dailytips_types);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $noticevideo = new DailyTipNoticeVideo;
        $noticevideo->active = $request->has('active')? true: false;
        $noticevideo->dailytips_types_id = $request->input('dailytips_types_id');
        $noticevideo->title = $request->input('title');
        $noticevideo->msg = $request->input('msg');
        $noticevideo->url = $request->input('url');
        $noticevideo->createEmbedLink($request->input('url'));

        $noticevideo->save();

        return redirect()->route('noticevideos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $noticevideo = DailyTipNoticeVideo::find($id);
        $dailytips_types = DailytipType::get();
        return View('notice_videos.edit')->with('noticevideo',$noticevideo)->with('types',$dailytips_types);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $noticevideo = DailyTipNoticeVideo::find($id);
        $noticevideo->active = $request->has('active')? true: false;
        $noticevideo->dailytips_types_id = $request->input('dailytips_types_id');
        $noticevideo->title = $request->input('title');
        $noticevideo->msg = $request->input('msg');
        $noticevideo->url = $request->input('url');
        $noticevideo->createEmbedLink($request->input('url'));

        $noticevideo->save();

        return redirect()->route('noticevideos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function deactivate($id){
        $notice = DailyTipNoticeVideo::find($id);
        $notice->active = false;
        $notice->save();
  
        return redirect()->route('noticevideos.index')->with('success','Video desativado');
    }
}
