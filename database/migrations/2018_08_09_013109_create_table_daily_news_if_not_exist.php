<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDailyNewsIfNotExist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('daily_news')){
            Schema::create('daily_news',function(Blueprint $table){
                $table->increments('id');
                $table->string('title');
                $table->text('content');
                $table->enum('status',['Active','Inactive']);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('daily_news')){
            Schema::drop('daily_news');
        }
    }
}
