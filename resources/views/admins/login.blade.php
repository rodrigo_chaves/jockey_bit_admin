<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Login</title>
    <meta name="description" content="Responsive, Bootstrap, Jockeybit">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
    <link rel="apple-touch-icon" href="../assets/images/logo.svg">
    <meta name="apple-mobile-web-app-title" content="Admin">
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" sizes="196x196" href="../assets/images/logo.svg">
    <link rel="stylesheet" href="../libs/font-awesome/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="/template/assets/css/app.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <style>
        canvas{ display: block; vertical-align: bottom; }
        #particles-js{ position:absolute; width: 100%; height: 100%; background: linear-gradient(135deg,#b452c3 0,#49cece 100%)!important; background-repeat: no-repeat; background-size: cover; background-position: 50% 50%; }
    </style>
  </head>
  <body>
    
    
    <div class="d-flex flex-column flex">
      <div id="particles-js" style="z-index: 0"></div>
      <div id="content-body" style="z-index:1">
        <div class="text-center w-100" >
          <div class="mx-auto w-xxl w-auto-xs card" style="margin-top:80px">
            <div class="logo-container" style="width: 165px;margin: 0 auto;">
                <img src="https://office.jockeybit.com/img/logo-jockey-interna.png" class="img-fluid">
                <h3 class="hidden-folded d-inline">Admin</h3>
            </div>
              
            <div class="px-3 mt-2">
              @if(isset($error))
                <div class="alert warning">{{$error}}</div>
              @endif
                <form name="form" action="{{route('login')}}" method="post">
                  {{csrf_field()}}
                  <div class="form-group">
                    <input type="email" name="email" class="form-control" placeholder="Email" required>
                  </div>
                  <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Senha" required>
                  </div>
                  <!-- <div class="mb-3">
                    <label class="md-check">
                      <input type="checkbox"><i class="primary"></i> Keep me signed in
                    </label>
                  </div> -->
                  <button type="submit" class="btn primary btn-block">Acessar</button>
                </form>
                <!-- <div class="my-4">
                  <a href="forgot-password.html" class="text-primary _600">Forgot password?</a>
                </div> -->
                <!-- <div>Do not have an account? <a href="signup.html" class="text-primary _600">Sign up</a> -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!-- <script src="/template/js/app.min.js"></script> -->
    <script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
    <script src="https://office.jockeybit.com/index.js"></script>
    </body>
</html>