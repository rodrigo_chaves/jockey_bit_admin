@extends('layout.panel')

@section('content')
<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h2>Editar Cavalo</h2>
          </div>
          <div class="box-divider m-0"></div>
          <div class="box-body">
            <form action="{{route('horses.update',['id'=>$horse->id])}}" method="post">
              <input type="hidden" name="_method" value="put">
                {{ csrf_field() }}
              <div class="form-group">
                <label>Nome</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Nome" value="{{$horse->name}}">
              </div>
              
              <button type="submit" class="btn btn-success">Atualizar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
