require('./bootstrap');
window.Vue = require('vue');
window.moment = require('moment');

Vue.component('message',require('./components/Message.vue'));
Vue.component('sent-message',require('./components/Sent.vue'));

var chat = new Vue({
  el : '#chat',
  props : ['user'],
  data : {
    messages : []
  },
  mounted(){
    this.fetchMessages();
  },
  methods : {
    addMessage(message){
      //console.log(JSON.stringify(message,null,2));
      document.getElementById('message').value = ""
      let data = {
        message : message.message,
        user_id : message.user.id,
        is_admin : message.is_admin,
        is_user : message.is_user
      }

      
      axios.post('/api/1/messages',data).then(response => {
      })
    },
    fetchMessages(){
      axios.get('/api/1/messages').then(response => {
        this.messages = response.data

        $('.chat-list').animate({
          scrollTop: $('.chat-list')[0].scrollHeight}, 
        "fast");
      })
    },
    pushMessage(message){
      this.messages.push(message)
      $('.chat-list').animate({
        scrollTop: $('.chat-list')[0].scrollHeight}, "fast");
    },
    deleteMessage(message, user){
      console.log('enviando delete para o server' + message.id);
      console.log('com o user: '+ user.id);

      axios.delete('/api/messages/' + message.id + '/' + user.id).then(response => {
        console.log(response);
      });
    },
    blockUser(user_id){
      console.log('bloqueando usuario: ' + user_id);
      axios.post('/api/blockuser',{
        user_id : user_id
      }).then(response => {
        console.log(response);
      })
    },
    cleanMessage(id){
      this.messages.forEach(function(item, index){
        if(item.id == id){
          console.log(item)
          item.message = '<i><strong>Mensagem removida pelo admin</strong></i>'
        }
      });
    }
  }
});

//Pusher.logToConsole = true;
var p_key = document.getElementById('pusher_key').value;
var cluster = document.getElementById('pusher_cluster').value;
var p_channel = document.getElementById('pusher_channel').value;
var pusher = new Pusher(p_key, {
  cluster: cluster,
  encrypted: true
});

var channel = pusher.subscribe(p_channel);
channel.bind('MessageSentEvent', function(e) {
  chat.pushMessage(e)
});

channel.bind('EventMessageDeleted',function(e){
  console.log('message deleted');
  console.log(e.id);
  chat.cleanMessage(e.id);
});