@extends('layout.panel')

@section('content')

<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
        @if(Session::has('error'))
          <div class="alert green">
            <b>{{Session::get('error')}}</b>
          </div>
        @endif
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Plataforma do Professor</a></li>
          <li class="breadcrumb-item active">Chats</li>
          </ol>
          <!-- <a href="{{route('chats.create')}}" class="btn btn-fw primary" style="margin-bottom: 40px;"><i class="fa fa-plus"></i> Adicionar chat</a> -->
          <div class="row">
              <div class="col-lg-12">
                  <!-- <a href="{{route('analysis.create')}}" class="btn btn-fw primary" style="margin-bottom: 40px;"><i class="fa fa-plus"></i> Adicionar cavalo</a> -->
                  <div class="box">
                    <div class="box-body">
                      <form action="{{route('chats.store')}}" method="post" class="form-inline">
                        {{ csrf_field() }}
                        <div class="form-check mx-sm-2">
                            <input type="checkbox" name="active" class="form-check-input" id="active">
                            <label class="form-check-label" for="active">Ativo</label>
                          </div>
                        <div class="form-group">
                          <input type="text" id="name" name="name" placeholder="Chat Maneiro" class="form-control">
                        </div>
                        
        
                        <div class="form-group mx-sm-2">
                          <button class="btn btn-success">Salvar</button>
                        </div>
                      </form>
                    </div>
                  </div>
          <table id="" class="table v-middle p-0 m-0 box dataTable no-footer">
            <thead>
              <tr>
                <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">#</td>
                <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Nome</td>
                <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Ativo</td>
                <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Ações</td>
              </tr>
            </thead>
            <tbody>
              @foreach($chats as $chat)
                <tr>
                    <td align="center">{{$chat->id}}</td>
                    <td align="center">{{$chat->name}}</td>
                    <td align="center">{{$chat->active? 'Sim' : 'Não'}}</td>
                    <td align="right">
                      <form action="{{route('chats.destroy',['id'=>$chat->id])}}" method="post">
                        {{csrf_field()}}

                        <input type="hidden" name="_method" value="delete">
                        <a href="{{route('chats.show',['id'=>$chat->id])}}" class="btn btn-warning" title="Conversar"><i class="fa fa-comment"></i></a>
                        <a href="{{route('chats.edit',['id'=>$chat->id])}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                        <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>

                      </form>
                    </td>
                </tr>
              @endforeach
            </tbody>
          </table>
      </div>
    </div>
  </div>
</div>
@endsection
