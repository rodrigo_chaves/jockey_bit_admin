<?php

use Illuminate\Database\Seeder;
use App\Model\SystemInfo;

class SystemInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SystemInfo::updateOrCreate(
            ['key' => 'tipstar'],
            ['val' => 'Tip Star info here']
        );

        SystemInfo::updateOrCreate(
            ['key' => 'avulse_videos'],
            ['val' => 'Avulse info here']
        );
    }
}
