<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8" />
      <title>Jockey Academy - Portal do Professor</title>
      <link rel="icon" type="image/png" href="https://office.jockeybit.com/img/favicon.png"/>
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
      <meta name="apple-mobile-web-app-title" content="Flatkit">
      <meta name="mobile-web-app-capable" content="yes">
      <link rel="shortcut icon" sizes="196x196" href="../assets/images/logo.svg">
      <!-- <link rel="stylesheet" href="/template/libs/font-awesome/css/font-awesome.min.css" type="text/css" /> -->
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
      <!-- <link rel="stylesheet" href="/template/libs/bootstrap/dist/css/bootstrap.min.css" type="text/css" /> -->
      <link rel="stylesheet" href="https://getbootstrap.com/docs/4.0/dist/css/bootstrap.min.css">
      <link rel="stylesheet" href="/template/assets/css/app.css" type="text/css" />
      <link rel="stylesheet" href="/template/assets/css/style.css" type="text/css" />
      <link rel="stylesheet" href="/template/assets/css/countdown.css">
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
      <link rel="stylesheet" href="/css/style.css">
  </head>
  <body>
        <div class="app" id="app">
          @include('layout.elements.aside')

          <div id="content" class="app-content box-shadow-0" role="main">
            @include('layout.elements.header')
            @yield('content')
          </div>
        </div>

        @include('layout.elements.modals')

        <script src="/template/assets/js/countdown.js"></script>
        <script src="/template/libs/jquery/dist/jquery.min.js"></script>
        <script src="/template/libs/popper.js/dist/umd/popper.min.js"></script>
        <script src="/template/libs/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="/template/libs/pace-progress/pace.min.js"></script>
        <script src="/template/libs/pjax/pjax.js"></script>
        <script src="/template/scripts/lazyload.config.js"></script>
        <script src="/template/scripts/lazyload.js"></script>
        <script src="/template/scripts/plugin.js"></script>
        <script src="/template/scripts/nav.js"></script>
        <script src="/template/scripts/scrollto.js"></script>
        <script src="/template/scripts/toggleclass.js"></script>
        <script src="/template/scripts/theme.js"></script>
        <script src="/template/scripts/ajax.js"></script>
        <script src="/template/scripts/app.js"></script>
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script src="/js/content_strings.js"></script>
        <script src="/js/menu.js"></script>
        <script type="text/javascript">
          $(document).ready(function(){
            if($('.datatable').length > 0){
              $('.datatable').DataTable();
            }
              
          });
        </script>
        @yield('js')
  </body>
</html>
