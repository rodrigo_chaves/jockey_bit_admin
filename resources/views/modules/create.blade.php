@extends('layout.panel')

@section('content')
<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h2>Novo modulo</h2>
          </div>
          <div class="box-divider m-0"></div>
          <div class="box-body">
            <form action="{{route('modules.store')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
              <div class="form-group">
                <label>Titulo</label>
                <input type="text" class="form-control" name="title" id="title" placeholder="Titulo">
              </div>

              <div class="form-group">
                <label>Descripção</label>
                <textarea class="form-control" id="msg" name="description"></textarea>
              </div>

              <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" name="active" id="active">
                <label class="form-check-label" for="active">Ativo</label>
              </div>

              <!-- <div class="form-group">
                <label>Imagem</label>
                <div class="form-file">
                  <input type="file" name="picture"> <button class="btn white">Select file ...</button>
                </div>
              </div> -->
              
              <button type="submit" class="btn primary">Salvar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="/template/libs/tinymce/tinymce.min.js"></script>
<script>tinymce.init({ selector:'#msg' });</script>

@endsection
