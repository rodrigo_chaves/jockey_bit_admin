<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Question as ModuleQuestion;

class ModuleQuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($module)
    {
        $questions = ModuleQuestion::byModule($module)->latest()->get();
        return View('questions.index')->with('questions',$questions)->with('module',$module);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($module)
    {
        return View('questions.create')->with('module',$module);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $module)
    {
        $question = new ModuleQuestion;
        $question->module_id = $module;
        $question->title = $request->input('title');
        $question->save();

        return redirect()->route('questions.index',['module'=>$module])->with('success','Pergunta salva');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = ModuleQuestion::find($id);
        return View('questions.edit')->with('question',$question);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $question)
    {
        $question = ModuleQuestion::find($question);
        $question->title = $request->input('title');
        $question->save();

        return redirect()->route('questions.index',['module'=>$question->module->id])->with('success','Pergunta salva');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = ModuleQuestion::find($id);
        ModuleQuestion::destroy($id);
        return redirect()->route('questions.index',['module'=>$question->module->id])->with('success','Pergunta removida');
    }
}
