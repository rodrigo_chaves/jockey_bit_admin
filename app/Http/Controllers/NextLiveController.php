<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\NextLive;

class NextLiveController extends Controller
{
    public function index(){
        $next_live = NextLive::latest()->first();

        return View('next_live.index')->with('next_live',$next_live);
    }

    public function edit($id){
        $next_live = NextLive::find($id);

        return View('next_live.edit')->with('next_live',$next_live);
    }

    public function update(Request $request, $id){
        $next_live = NextLive::find($id);

        $next_live->due_date = $request->input('due_date');

        $next_live->save();

        return redirect()->route('next_live.index');
    }
}
