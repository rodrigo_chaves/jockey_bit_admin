<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Analyze;
use App\Model\AnalyzeTip;
use App\Model\Distance;
use App\Model\RacingTrack;
use App\Model\Horse;
use App\Model\LiveHorse;
use App\Model\AnalyzeClasse;
use Validator;

class AnalysisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $distances = Distance::get();
        $tracks = RacingTrack::get();
        $horses = Horse::get();
        $live_horses = LiveHorse::get();
        $classes = AnalyzeClasse::get();

        $analysis = Analyze::paginate(20);

        return View('analysis.index')
            ->with('menu','analysis')
            ->with('analysis',$analysis)
            ->with('distances',$distances)
            ->with('tracks',$tracks)
            ->with('horses',$horses)
            ->with('classes',$classes)
            ->with('live_horses',$live_horses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'racing_track_id' => 'required',
            'distance_id' => 'required',
            'classe_id' =>'required',
            'odd' => 'required'
        ]);

        if($validation->fails()){
            return redirect()->back()->with('error',$validation->errors()->all()[0]);
        }
        else{
            $analyze = new Analyze;
            $analyze->racing_track_id = $request->input('racing_track_id');
            $analyze->distance_id = $request->input('distance_id');
            $analyze->horse_id = $request->has('horse_id')? $request->input('horse_id') : null;
            $analyze->livehorse_id = $request->has('livehorse_id')? $request->input('livehorse_id') : null;
            $analyze->odd =  $request->input('odd');
            $analyze->classe_id = $request->input('classe_id');
            $analyze->save();

            $analyzeTip = new AnalyzeTip;
            $analyzeTip->racing_track_id = $request->input('racing_track_id');
            $analyzeTip->distance_id = $request->input('distance_id');
            $analyzeTip->horse_id = $request->has('horse_id')? $request->input('horse_id') : null;
            $analyzeTip->livehorse_id = $request->has('livehorse_id')? $request->input('livehorse_id') : null;
            $analyzeTip->odd =  $request->input('odd');
            $analyzeTip->classe_id = $request->input('classe_id');
            $analyzeTip->save();

            return redirect()->route('analysis.index')->with('success','Analise salva');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $analyse = Analyze::find($id);

        $distances = Distance::get();
        $tracks = RacingTrack::get();
        $horses = Horse::get();
        $live_horses = LiveHorse::get();
        $classes = AnalyzeClasse::get();

        return View('analysis.edit')
                ->with('analyze',$analyse)
                ->with('distances',$distances)
                ->with('tracks',$tracks)
                ->with('horses',$horses)
                ->with('classes',$classes)
                ->with('live_horses',$live_horses);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(),[
            'racing_track_id' => 'required',
            'distance_id' => 'required',
            'odd' => 'required'
        ]);

        if($validation->fails()){
            return redirect()->back()->with('error',$validation->errors()->all()[0]);
        }
        else{
            $analyze = Analyze::find($id);
            $analyze->racing_track_id = $request->input('racing_track_id');
            $analyze->distance_id = $request->input('distance_id');
            $analyze->horse_id = $request->has('horse_id')? $request->input('horse_id') : null;
            $analyze->livehorse_id = $request->has('livehorse_id')? $request->input('livehorse_id') : null;
            $analyze->odd =  $request->input('odd');
            $analyze->classe_id = $request->input('classe_id');
            $analyze->save();

            return redirect()->route('analysis.index')->with('success','Analise salva');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Analyze::destroy($id);
        return redirect()->route('analysis.index')->with('success','Analise removida');
    }
}
