@extends('layout.panel')

@section('content')

<div class="content-main " id="content-main">
  <div class="padding">
  
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>

    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <h1 class="display-4 l-s-n-1x">

          <a href="{{route('racingtracksguides.create')}}" class="btn btn-fw primary">Novo Guia</a>
        </h1>
        <table id="" class="table v-middle p-0 m-0 box dataTable no-footer" data-plugin="dataTable" role="grid" aria-describedby="datatable_info" style="width: 1094px;">
          <thead>
            <tr>
              <th width="60px"></th>
              <th >Titulo</th>
              <th align="center"></th>
            </tr>
          </thead>
          <tbody>
            @foreach($guides as $guide)
              <tr>
                <td align="center">
                  <img src="{{$guide->img}}" class="img-fluid">
                </td>
                <td >{{$guide->title}}</td>
                <td align="right">
                  <form action="{{route('racingtracksguides.destroy',['racingtracksguide'=>$guide->id])}}" method="post">
                    <input type="hidden" name="_method" value="delete">
                    {{csrf_field()}}
                    <!-- <a href="{{route('racingtracksguides.edit',['racingtracksguide'=>$guide->id])}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a> -->
                    <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>
                  </form>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  @include('layout.elements.footer')
</div>
@endsection
