@extends('layout.panel')

@section('content')

<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <ol class="breadcrumb"><li class="breadcrumb-item"><a href="#">Plataforma do Professor</a></li><li class="breadcrumb-item active">Videos de Tips</li></ol>
        <a href="{{route('noticevideos.create')}}" class="btn btn-fw primary" style="margin-bottom: 40px;"><i class="fa fa-plus"></i> Adicionar novo video</a>
        <table id="" class="table v-middle p-0 m-0 box dataTable no-footer">
          <thead>
            <tr>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Titulo</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Msg</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">URL</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Ações</td>
              
            </tr>
          </thead>
          <tbody>
                @foreach($noticevideos as $noticevideo)
                <tr>
                  <td align="center">{{$noticevideo->title}}</td>
                  <td align="center"><?php echo html_entity_decode($noticevideo->msg);?></td>
                  <td align="center">{{$noticevideo->url}}</td>
                  <td align="center">{{$noticevideo->active? 'Ativo' : 'Inativo'}}</td>
                  <td align="right">
                    
                    <form action="{{route('noticevideos.destroy',['id'=>$noticevideo->id])}}" method="post">
                      {{csrf_field()}}

                      <input type="hidden" name="_method" value="delete">
                      
                      @if($noticevideo->active)
                        <a class="btn btn-warning" href="{{route('deactivate_noticevideo',['id'=>$noticevideo->id])}}" style="color:#fff"><i class="fa fa-power-off"></i></a>
                      @endif


                      <a href="{{route('noticevideos.edit',['id'=>$noticevideo->id])}}" class="btn btn-primary" style="color:#fff"><i class="fa fa-pencil"></i></a>

                      <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>

                      
                    </form>
                  </td>
                </tr>
                @endforeach
          </tbody>
        </table>

      </div>
    </div>
  </div>
  @include('layout.elements.footer')
</div>
@endsection
