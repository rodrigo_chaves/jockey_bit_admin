<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DailyTipNotice extends Model
{
    protected $table = 'dailytips_notice';

    public function type(){
        return $this->belongsTo('App\Model\DailytipType','dailytips_types_id', 'id');
    }

    public function scopeByType($query, $type){
        return $query->where('dailytips_types_id', $type);
    }

    public function scopeActive($query){
        return $query->where('active',true);
    }

    public function createEmbedLink($url){
        //https://vimeo.com/29885705
       // $parts = explode('?v=',$url);
       // $this->embed = 'https://www.youtube.com/embed/' . $parts[count($parts)-1] . '?rel=0';
       if($this->isVimeo($url)){
           $parts = explode('vimeo.com/',$url);
           $this->embed_url = 'https://player.vimeo.com/video/' . $parts[count($parts)-1] . '';
       }
       else if($this->isYoutube($url)){
           $parts = explode('?v=',$url);
           $this->embed_url = 'https://www.youtube.com/embed/' . $parts[count($parts)-1] . '';
       }
       
     }

    public function isVimeo($url){
        return strpos($url,'vimeo.com/');
    }

    public function isYoutube($url){
        return strpos($url,'?v=');
    }
}
