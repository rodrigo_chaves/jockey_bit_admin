<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeEadHistoricTips extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ead_historic_tips',function(Blueprint $table){
            $table->string('horse')->nullable()->change();
            $table->datetime('due_date')->nullable()->change();
            $table->string('run')->nullable()->change();
            $table->string('odd')->nullable()->change();
            $table->string('resume')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ead_historic_tips',function(Blueprint $table){
            $table->dropColumn('resume');
        });
    }
}
