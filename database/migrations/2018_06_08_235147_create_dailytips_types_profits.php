<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailytipsTypesProfits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dailytips_types_profits',function(Blueprint $table){
            $table->increments('id');
            $table->integer('dailytips_types_id')->unsigned();
            $table->foreign('dailytips_types_id')->references('id')->on('dailytips_types');
            $table->decimal('profit',10,2);
            $table->date('due_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dailytips_types_profits');
    }
}
