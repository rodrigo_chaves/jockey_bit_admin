<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToDailytips extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('daily_tips',function(Blueprint $table){
            $table->string('result')->nullable();
            $table->boolean('win_lose')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('daily_tips',function(Blueprint $table){
            $table->dropColumn('result');
            $table->dropColumn('win_lose');
        });
    }
}
