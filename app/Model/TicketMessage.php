<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TicketMessage extends Model
{
    protected $table = 'tickets_messages';

    public function ticket(){
        return $this->belongsTo('App\Model\Ticket','ticket_id','id');
    }

    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }
    public function admin(){
        return $this->belongsTo('App\Model\Admin','admin_id','id');
    }
}
