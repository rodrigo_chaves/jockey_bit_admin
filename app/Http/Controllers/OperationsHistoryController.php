<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\OperationHistory;

class OperationsHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $operations_history = OperationHistory::orderBy('created_at','DESC')->paginate(10);

        return View('operations_history.index')->with('operations_history',$operations_history);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('operations_history.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $operation_history = new OperationHistory;
        $operation_history->title = $request->input('title');
        $operation_history->url = $request->input('url');
        $operation_history->createEmbedLink($request->input('url'));
        $operation_history->picture = $request->input('picture');
        $operation_history->active = $request->has('active') ? true : false;
        $operation_history->save();

        return redirect()->route('operations_history.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $operation_history = OperationHistory::find($id);

        return View('operations_history.edit')->with('operation_history',$operation_history);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $operation_history = OperationHistory::find($id);
        $operation_history->title = $request->input('title');
        $operation_history->url = $request->input('url');
        $operation_history->createEmbedLink($request->input('url'));
        $operation_history->picture = $request->input('picture');
        $operation_history->active = $request->has('active') ? true : false;
        $operation_history->save();

        return redirect()->route('operations_history.index')->with('success','Histórico atualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        OperationHistory::destroy($id);
        return redirect()->route('operations_history.index')->with('success','Histórico removido');
    }
}
