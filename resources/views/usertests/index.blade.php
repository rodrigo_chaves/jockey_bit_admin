@extends('layout.panel')

@section('content')

<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Plataforma do Professor</a></li>
          <li class="breadcrumb-item active">Provas</li>
          </ol>
        <table id="" class="table v-middle p-0 m-0 box no-footer">
          <thead>
            <tr>
              <th>Usuario</th>
              <th class="text-center">Módulo</th>
              <th class="text-center">Nota</th>
              <th class="text-center">Aprovado</th>
              <th class="text-center">Ações</th>
            </tr>
          </thead>
          <tbody>
            @foreach($tests as $test)
              <tr>
                <td>{{$test->user->name}}</td>
                <td class="text-center">{{$test->module->title}}</td>
                <td class="text-center">{{$test->decimalGrade()}}</td>
                <td class="text-center">{!!$test->approved? '<span class="badge badge-success">Sim</span>' : '<span class="badge badge-danger">Não</span>'!!}</td>
                <td class="text-center">
                  <form action="{{route('usertests.destroy',['id'=>$test->id])}}" method="post">
                    {{csrf_field()}}

                    <input type="hidden" name="_method" value="delete">
                    <button type="submit" class="btn btn-danger btn-circle" title="Remover este registro">
                      <i class="fa fa-times"></i>
                    </button>
                  </form>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-lg-12">
        {{$tests->links()}}
      </div>
    </div>
  </div>
</div>
@endsection
