<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'module_questions';

    public function module(){
        return $this->belongsTo('App\Model\Module');
    }


    public function scopeByModule($query, $module_id){
        return $query->where('module_id',$module_id);
    }
}
