<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DailyNewsController extends Controller
{
	public function edit($id){
		$data=\DB::table('daily_news')->where("id",$id)->first();

		return view('daily_news.edit',compact('data'));
	}
	public function update($id,Request $request){


		$data=\DB::table('daily_news')->where("id",$id)->update(['title'=>$request->title,'status'=>$request->status,'content'=>$request->content]);

echo "<center><h1>Dados atualizados com sucesso.</h1></center>";

	}

}
