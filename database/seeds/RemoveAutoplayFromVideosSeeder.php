<?php

use Illuminate\Database\Seeder;
use App\Model\LiveTrading;
use App\Model\OperationHistory;

class RemoveAutoplayFromVideosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $find = array('?autoplay=1','?rel=0');

        $live_tradings = LiveTrading::all();
        foreach($live_tradings as $live){
            $live->url = str_replace($find, '', $live->url);
            $live->save();
        }

        $operation_history = OperationHistory::all();
        foreach($operation_history as $operation){
            $operation->embed_url = str_replace($find,'',$operation->embed_url);
            $operation->save();
        }
    }
}
