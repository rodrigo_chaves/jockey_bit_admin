@extends('layout.panel')

@section('content')

<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-sm-12">
          <ol class="breadcrumb"><li class="breadcrumb-item"><a href="#">Plataforma do Professor</a></li><li class="breadcrumb-item active">Histórico de Tips</li></ol>
          <a href="{{route('create_operation')}}" class="btn btn-fw primary" style="margin-bottom: 40px;"><i class="fa fa-plus"></i> Adicionar nova Tips no histórico</a>
        <table id="" class="table v-middle p-0 m-0 box dataTable no-footer" data-plugin="dataTable" role="grid" aria-describedby="datatable_info" style="width: 1094px;">
          <thead>
            <tr>
              <!-- <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Data</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Cavalo</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Hora / Corrida</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">ODD</td> -->
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Resumo</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Resultado</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Win/Lose</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Ações</td>
            </tr>
          </thead>
          <tbody>
            @foreach($operations as $operation)
              <tr>
                  
                  <td align="center">{{$operation->resume}}</td>
                  <td align="center">{{$operation->result}}</td>
                  <td align="center">
                    @if($operation->win_lose == 1)
                      <span class="badge badge-pill success">WIN</span>
                    @else
                      <span class="badge badge-pill danger">LOSE</span>
                    @endif
                  </td>
                  <td align="right">
                    <form action="{{route('operations.destroy',['id'=>$operation->id])}}" method="post">
                      {{csrf_field()}}

                      <input type="hidden" name="_method" value="delete">

                      <a href="{{route('operations.edit',['id'=>$operation->id])}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>

                      <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>
                    </form>
                  </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
