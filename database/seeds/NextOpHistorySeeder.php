<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Model\NextOpHistory;

class NextOpHistorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $carbonTomorrow = Carbon::now()->addDay();;
        
        NextOpHistory::create(['due_date'=>$carbonTomorrow->toDatetimeString()]);
    }
}
