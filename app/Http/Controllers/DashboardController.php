<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Module;
use App\Model\Ticket;
use App\User;
use App\Model\LiveTrading;
use App\Model\UserTest;
use App\Model\TipStar;

class DashboardController extends Controller
{
    public function index(){
        $active_modules = Module::active()->count();
        $total_modules = Module::count();
        $open_tickets = Ticket::open()->count();
        $total_tickets = Ticket::count();
        $last_tickets = Ticket::orderBy('id','DESC')->limit(5)->get();
        $total_users = User::count();
        $last_live_trading = LiveTrading::latest()->first();
        $users_on_modules = Module::usersOnModules();
        $last_tests = UserTest::latest()->limit(7)->get();
        $tipstars = TipStar::fromToday()->count();
        $total_tipstars = TipStar::count();
        
        return View('dashboard.index')
                ->with('active_modules',$active_modules)
                ->with('total_modules',$total_modules)
                ->with('open_tickets',$open_tickets)
                ->with('total_tickets',$total_tickets)
                ->with('last_tickets',$last_tickets)
                ->with('total_users', $total_users)
                ->with('last_live_trading',$last_live_trading)
                ->with('users_on_modules', $users_on_modules)
                ->with('last_tests', $last_tests)
                ->with('tipstars' , $tipstars)
                ->with('total_tipstars', $total_tipstars);
    }
}
