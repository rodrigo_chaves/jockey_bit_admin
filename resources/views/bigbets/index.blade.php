@extends('layout.panel')

@section('content')

<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <ol class="breadcrumb"><li class="breadcrumb-item"><a href="#">Plataforma do Professor</a></li><li class="breadcrumb-item active">Bolão</li></ol>
        <!-- <a href="{{route('big_week_class.create')}}" class="btn btn-fw primary" style="margin-bottom: 40px;"><i class="fa fa-plus"></i> Adicionar aulão semanal</a> -->
        <table id="" class="table v-middle p-0 m-0 box dataTable no-footer">
          <thead>
            <tr>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Usuario</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">email</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Resumo</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">ODD</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Tipo</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Hora</td>

            </tr>
          </thead>
          <tbody>
            @foreach($bigbets as $bigbet)
              <tr>
                  <td align="center">{{$bigbet->user->name}}</td>
                  <td align="center">{{$bigbet->user->email}}</td>
                  <td align="center">{{$bigbet->resume}}</td>
                  <td align="center">{{$bigbet->odd}}</td>
                  <td align="center">{{$bigbet->type->name}}</td>
                  <td align="center">{{date('H:i', strtotime($bigbet->created_at))}}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
	{{$bigbets->links()}}
      </div>
    </div>
  </div>
  @include('layout.elements.footer')
</div>
@endsection
