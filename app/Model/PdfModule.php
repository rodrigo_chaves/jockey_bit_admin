<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PdfModule extends Model
{
    protected $table = 'pdf_modules';

    public function module(){
        return $this->belongsTo('App\Model\Module');
    }

    public function scopeByModule($query, $module_id){
        return $query->where('module_id',$module_id);
    }
}
