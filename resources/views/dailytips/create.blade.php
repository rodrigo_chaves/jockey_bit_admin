@extends('layout.panel')

@section('content')

<link href="https://tempusdominus.github.io/bootstrap-4/theme/css/tempusdominus-bootstrap-4.css">

<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-md-12">
        @if(Session::has('errors'))
          <div class="alert alert-danger">
            @foreach(Session::get('errors') as $error)
              <?php echo $error . '<br>' ?>
            @endforeach
        </div>
        @endif
        <div class="box">
          <div class="box-header">
            <h2>Nova Tip</h2>
          </div>
          <div class="box-divider m-0"></div>
          <div class="box-body">
            <form action="{{route('dailytips.store')}}" method="post">
                {{ csrf_field() }}
              <!-- <div class="form-group">
                <label>Cavalo</label>
                <input type="text" class="form-control" name="horse" id="horse" placeholder="Nome do cavalo">
              </div>
              <div class="form-group">
                <label>Corrida</label>
                <input type="text" class="form-control" name="run" id="run" placeholder="Corrida">
              </div>


              <div class="form-group">
                <label>ODD</label>
                <input type="text" class="form-control" name="odd" id="odd">
              </div>

              <div class="form-group">

                <label>Data / Hora</label>
                <input type="datetime" class="form-control" name="due_date" id="due_date" placeholder="EX: 2017/01/31 20:35">

              </div> -->
              <div class="form-group">
              <label>Resumo</label>
                <input type="text" name="resume" class="form-control" placeholder="Resumo">
              </div>

              <div class="form-group">
                <label>Tipo</label>
                <select name="dailytips_types_id" class="form-control">
                  @foreach($types as $type)
                    <option value="{{$type->id}}">{{$type->name}}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                  <label>Data</label>
                  <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                      <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker1" name="cdate"/>
                      <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                          <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                      </div>
                  </div>
              </div>
              
              <button type="submit" class="btn primary">Salvar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
<script src="/template/libs/moment/moment.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>
 <script type="text/javascript">
  $(function () {
      $('#datetimepicker1').datetimepicker({format:'YYYY-MM-DD H:mm:ss'});
  });
</script>
@endsection
