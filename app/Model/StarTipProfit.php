<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StarTipProfit extends Model
{
    protected $table = 'startips_profits';
}
