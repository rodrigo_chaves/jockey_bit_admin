@extends('layout.panel')

@section('content')
<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h2>Novo Video de tipos</h2>
          </div>
          <div class="box-divider m-0"></div>
          <div class="box-body">
            <form action="{{route('noticevideos.update',['noticevideo'=>$noticevideo->id])}}" method="post">
                <input type="hidden" name="_method" value="put">
                {{ csrf_field() }}
              
              <div class="checkbox"><label class="ui-check ui-check-md"><input type="checkbox" name="active" id="active" {{($noticevideo->active)? 'checked' : ''}}> <i class="dark-white"></i> Ativo</label></div>
              
              <div class="form-group">
                <label>Titulo</label>
                <input type="text" name="title" class="form-control" placeholder="" value="{{$noticevideo->title}}">
              </div>

              <div class="form-group">
                <label>Msg</label>
                <input type="text" name="msg" class="form-control" placeholder="msg" value="{{$noticevideo->msg}}">
              </div>

              <div class="form-group">
                <label>Video</label>
                <input type="text" name="url" class="form-control" placeholder="https://vimeo.com/29885705" value="{{$noticevideo->url}}">
              </div>

              <div class="form-group">
                <label>Tipo</label>
                <select name="dailytips_types_id" class="form-control">
                  @foreach($types as $type)
                    <option value="{{$type->id}}" {{($type->id==$noticevideo->dailytips_types_id)? 'checked' : ''}}>{{$type->name}}</option>
                  @endforeach
                </select>
              </div>

            
              <button type="submit" class="btn primary">Salvar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
