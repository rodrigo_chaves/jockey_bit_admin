<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\LiveTrading;

class LiveTradingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $livetradings = LiveTrading::orderBy('created_at','DESC')->paginate(10);
        return View('livetradings.index')->with('livetradings',$livetradings);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('livetradings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $live_trading = new LiveTrading;
        $live_trading->title = $request->input('title');
        $live_trading->createEmbedLink($request->input('url'));
        $live_trading->picture = $request->input('picture');
        $live_trading->active = $request->has('active') ? true : false;
        $live_trading->active = $request->has('active_on_office') ? true : false;
        $live_trading->save();

        return redirect()->route('livetradings.index')->with('success','Live salva');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $live_trading = LiveTrading::find($id);

        return View('livetradings.edit')->with('live_trading',$live_trading);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $live_trading = LiveTrading::find($id);
        $live_trading->title = $request->input('title');
        $live_trading->createEmbedLink($request->input('url'));
        $live_trading->picture = $request->input('picture');
        $live_trading->active = $request->has('active') ? true : false;
        $live_trading->active = $request->has('active_on_office') ? true : false;
        $live_trading->save();

        return redirect()->route('livetradings.index')->with('success','Live Atualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        LiveTrading::destroy($id);

        return redirect()->route('livetradings.index')->with('success','Live removida');
    }
}
