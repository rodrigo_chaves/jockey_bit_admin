<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DailytipsProfit extends Model
{
    protected $table = 'dailytips_types_profits';

    public function type(){
        return $this->belongsTo('App\Model\DailytipType','dailytips_types_id', 'id');
    }
}
