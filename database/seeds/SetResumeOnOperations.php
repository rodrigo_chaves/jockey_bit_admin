<?php

use Illuminate\Database\Seeder;
use App\Model\Operation;

class SetResumeOnOperations extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $operations = Operation::whereNull('resume')->get();
        foreach($operations as $operation){
            //Chepstow 13:15h / Cavalo Foresee odd
            $operation->resume = $operation->run ." " . $operation->due_date . " / " . $operation->horse . " " . $operation->odd;
            $operation->save();
        }
    }
}
