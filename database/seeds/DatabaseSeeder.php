<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        //$this->call(AdminSeeder::class);
        //$this->call(NextOpHistorySeeder::class);
        $this->call(SetResumeOnDailytips::class);
        $this->call(SetResumeOnOperations::class);
        $this->call(RemoveAutoplayFromVideosSeeder::class);
        $this->call(SystemInfoSeeder::class);
    }
}
