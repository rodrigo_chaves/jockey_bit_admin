@extends('layout.panel')

@section('content')

<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <ol class="breadcrumb"><li class="breadcrumb-item"><a href="#">Plataforma do Professor</a></li><li class="breadcrumb-item active">Aulas semanais</li></ol>
        <a href="{{route('week_class.create')}}" class="btn btn-fw primary" style="margin-bottom: 40px;"><i class="fa fa-plus"></i> Adicionar aulão semanal</a>
        <table id="" class="table v-middle p-0 m-0 box dataTable no-footer">
          <thead>
            <tr>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Título</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">URL</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Data</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Ações</td>
            </tr>
          </thead>
          <tbody>
            @foreach($week_classes as $week_class)
              <tr>
                  <td align="center">{{$week_class->title}}</td>
                  <td align="center">{{$week_class->url}}</td>
                  <td align="center">{{$week_class->due_date}}</td>
                  <td align="right">
                    <form action="{{route('week_class.destroy',['id'=>$week_class->id])}}" method="post">
                      {{csrf_field()}}

                      <input type="hidden" name="_method" value="delete">

                      <a href="{{route('week_class.edit',['id'=>$week_class->id])}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>

                      <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>

                    </form>
                  </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  @include('layout.elements.footer')
</div>
@endsection
