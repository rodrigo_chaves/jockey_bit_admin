<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OfficialNotice extends Model
{
    protected $table = 'official_notice';
}
