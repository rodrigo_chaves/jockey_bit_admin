<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Operation extends Model
{
    protected $table = 'ead_historic_tips';


    /** methods */

    public function timeFormated(){
        $carbon = Carbon::createFromFormat('Y-m-d H:i:s', $this->due_date);
        return $carbon->format('H:i');
    }

    public function dateFormated(){
        $carbon = Carbon::createFromFormat('Y-m-d H:i:s', $this->due_date);
        return $carbon->format('d/m/Y');
    }

    
    /** scopes */

    public function scopeByMonth($query, $month){
        return $query->whereMonth('due_date',$month);
    }

    public function scopeByYear($query, $year){
        return $query->whereYear('due_date',$year);
    }
}
