<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Analyze extends Model
{
    protected $table = 'race_analysis';

    public function track(){
        return $this->belongsTo('App\Model\RacingTrack','racing_track_id','id');
    }

    public function distance(){
        return $this->belongsTo('App\Model\Distance','distance_id','id');
    }

    public function horse(){
        return $this->belongsTo('App\Model\Horse','horse_id','id');
    }

    public function live_horse(){
        return $this->belongsTo('App\Model\LiveHorse','livehorse_id','id');
    }

    public function classe(){
        return $this->belongsTo('App\Model\AnalyzeClasse','classe_id','id');
    }
}
