@extends('layout.panel')

@section('content')

<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Plataforma do Admin</a></li>
          <li class="breadcrumb-item active">analises</li>
        </ol>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
          <div class="box">
            <div class="box-body">
              <form action="{{route('analysistips.update',['id'=>$analyze_tip->id])}}" method="post">
                <input type="hidden" name="_method" value="put">
                {{ csrf_field() }}
                <div class="row">
                  <div class="col-lg-2">
                    <div class="form-group">
                      <select class="form-control" name="racing_track_id" disabled>
                        @foreach($tracks as $track)
                          <option value="{{$track->id}}">{{$track->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group">
                      <select class="form-control" name="distance_id" disabled>
                        @foreach($distances as $distance)
                          <option value="{{$distance->id}}" {{($analyze_tip->distance_id == $distance->id)? 'selected' : ''}}>{{$distance->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group">
                      <select class="form-control" name="horse_id" disabled>
                        @foreach($horses as $horse)
                          <option value="{{$horse->id}}" {{($analyze_tip->horse_id == $horse->id)? 'selected' : ''}}>{{$horse->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group">
                      <select class="form-control" name="livehorse_id" disabled>
                        @foreach($live_horses as $livehorse)
                          <option value="{{$livehorse->id}}" {{($analyze_tip->livehorse_id == $livehorse->id)? 'selected' : ''}}>{{$livehorse->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group">
                      <input type="text" name="odd" placeholder="odd" class="form-control" value="{{$analyze_tip->odd}}" disabled>
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group">
                      <select name="classe_id" class="form-control" disabled>
                        @foreach($classes as $classe)
                          <option value="{{$classe->id}}" {{($analyze_tip->classe_id == $classe->id)? 'selected' : ''}}>{{$classe->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                      
                  <div class="col-lg-2">
                    <div class="form-group">
                      <input type="text" name="back_odd" class="form-control" placeholder="Back" value="{{$analyze_tip->back_odd}}">
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group">
                      <input type="text" name="high_price" class="form-control" placeholder="Valor Alto : 0.00" value="{{$analyze_tip->high_price}}">
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group">
                      <input type="text" name="percent" class="form-control" placeholder="Porcentagem" value="{{$analyze_tip->percent}}">
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group">
                      <input type="text" name="racingpost" class="form-control" placeholder="racingpost" value="{{$analyze_tip->racingpost}}">
                     </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group">
                      <input type="text" name="position_odd" placeholder="Posição" class="form-control" value="{{$analyze_tip->position_odd}}">
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group">
                      <input type="text" name="lay_odd" placeholder="Lay" class="form-control" value="{{$analyze_tip->lay_odd}}">
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group">
                      <input type="text" name="challenge_odd" placeholder="Challenge" class="form-control" value="{{$analyze_tip->challenge_odd}}">
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group">
                      <button class="btn btn-success">Salvar</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
      </div>
    </div>
    </div>
  </div>
</div>
@endsection
