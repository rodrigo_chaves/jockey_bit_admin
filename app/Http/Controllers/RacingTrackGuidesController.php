<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\RacingTrackGuide;
use Validator;

class RacingTrackGuidesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $guides = RacingTrackGuide::latest()->paginate(10);

        return View('guides.index')->with('guides',$guides);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('guides.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'title' => 'required'
        ]);

        if($validation->fails()){
            return redirect()->back()->with("error", $validation->errors()->all()[0]);
        }
        else{
            $guide = new RacingTrackGuide;
            $guide->title = $request->input('title');
            $guide->latitude = $request->input('latitude');
            $guide->longitude = $request->input('longitude');
            $guide->img = $request->input('storage_file');
            $guide->timeform = $request->input('timeform');
            $guide->save();

            return redirect()->route('racingtracksguides.index')->with('success', 'Guia salvo');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $guide = RacingTrackGuide::find($id);
        if($guide){
            RacingTrackGuide::destroy($id);
            return redirect()->route('racingtracksguides.index')->with('success', 'Guia removida');
        }
    }
}
