<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\WeekClass;

class WeekClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $week_classes = WeekClass::paginate(10);

        return View('week_class.index')->with('week_classes',$week_classes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('week_class.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $week_class = new WeekClass;

        $week_class->title = $request->input('title');
        $week_class->url = $request->input('url');
        $week_class->due_date = $request->input('due_date');
        $week_class->createEmbedLink($week_class->url);

        $week_class->save();

        return redirect()->route('week_class.index')->with('success','Aula semanal salva');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $week_class = WeekClass::find($id);

        return View('week_class.edit')->with('week_class',$week_class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $week_class = WeekClass::find($id);
        $week_class->title = $request->input('title');
        $week_class->url = $request->input('url');
        $week_class->due_date = $request->input('due_date');
        $week_class->createEmbedLink($week_class->url);

        $week_class->save();

        return redirect()->route('week_class.index')->with('success','Aula semanal atualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        WeekClass::destroy($id);
        return redirect()->route('week_class.index')->with('success','Aula semanal removida');
    }
}
