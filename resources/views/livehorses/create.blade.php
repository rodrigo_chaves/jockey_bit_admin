@extends('layout.panel')

@section('content')
<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h2>Novo Cavalo</h2>
          </div>
          <div class="box-divider m-0"></div>
          <div class="box-body">
            <form action="{{route('livehorses.store')}}" method="post">
                {{ csrf_field() }}
              <div class="form-group">
                <label>Nome</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Nome">
              </div>
              
              <button type="submit" class="btn primary">Salvar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
