<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BigBet extends Model
{
    protected $table = 'big_bets';

    public function type(){
        return $this->belongsTo('App\Model\DailytipType','dailytips_types_id', 'id');
    }

    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }

    public function scopeFromToday($query, $today){
        //return $query->whereDate('due_date',$today);
        return $query->whereDate('created_at',$today);
    }
}
