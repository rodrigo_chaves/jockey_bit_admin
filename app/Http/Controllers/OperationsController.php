<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Operation;

class OperationsController extends Controller
{
    public function index(){
        $operations = Operation::orderBy('created_at','DESC')
                                ->paginate(10000);

        return View('operations.index')
                ->with('operations',$operations)
                ->with('menu','operations');
    }

    /**
     * stand by
     */
    public function filter(){
        $operations = Operation::byMonth($month)
                                ->byYear($year)
                                ->orderBy('created_at','DESC')
                                ->paginate(10000);

        return View('operations.index')
                ->with('operations',$operations)
                ->with('month',$month)
                ->with('year',$year);
    }

    public function create(){
        return View('operations.create');
    }

    public function store(Request $request){
        $operation = new Operation;

        $operation->horse = $request->input('horse');
        $operation->run = $request->input('run');
        $operation->result = $request->input('result');
        $operation->due_date = $request->input('due_date');
        $operation->odd = $request->input('odd');
        $operation->win_lose = $request->input('win_lose');

        $operation->save();

        return redirect()->route('operations');
    }

    public function edit($id){
        $operation = Operation::find($id);
        return View('operations.edit')->with('operation',$operation);
    }

    public function update(Request $request, $id){
        $operation = Operation::find($id);

        $operation->horse = $request->input('horse');
        $operation->run = $request->input('run');
        $operation->result = $request->input('result');
        $operation->due_date = $request->input('due_date');
        $operation->odd = $request->input('odd');
        $operation->win_lose = $request->input('win_lose');

        $operation->save();

        return redirect()->route('operations')->with('success','Atualizado');
    }

    public function destroy($id){
        Operation::destroy($id);
        return redirect()->route('operations')->with('success','Operação removida');        
    }
}
