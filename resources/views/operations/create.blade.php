@extends('layout.panel')

@section('content')
<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">

<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h2>Nova operação</h2>
          </div>
          <div class="box-divider m-0"></div>
          <div class="box-body">
            <form action="{{route('store_operation')}}" method="post">
                {{ csrf_field() }}
              <div class="form-group">
                <label>Cavalo</label>
                <input type="text" class="form-control" name="horse" id="horse" placeholder="Nome do cavalo">
              </div>
              <div class="form-group">
                <label>Corrida</label>
                <input type="text" class="form-control" name="run" id="run" placeholder="Corrida">
              </div>
              <div class="form-group">
                <label>Resultado</label>
                <input type="text" class="form-control" name="result" id="result" placeholder="Corrida">
              </div>

              <div class="form-group">
                <label>Win/Lose</label>
                <select class="form-control" name="win_lose">
                  <option value="0">LOSE</option>
                  <option value="1">WIN</option>
                </select>
              </div>

              <div class="form-group">
                <label>ODD</label>
                <input type="text" class="form-control" name="odd" id="odd">
              </div>

              <div class="form-group">

                <label>Data / Hora</label>
                <input type="datetime" class="form-control" name="due_date" id="due_date" placeholder="EX: 2017/01/31 20:35">

              </div>
              
              <button type="submit" class="btn primary">Salvar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.13/jquery.mask.js"></script>
<script type="text/javascript">
  $(function () {
      $('input[type=datetime]').mask('0000-00-00 00:00');
  });
</script>

@endsection
