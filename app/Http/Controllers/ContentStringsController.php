<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Model\ContentString;
use App\Model\StringLanguage;

class ContentStringsController extends Controller
{
    public function index($text){
        $key = ContentString::byText($text)->first();
        $json = ['success' => true, 'data' => [], 'error' => __('error.not_found')];
        if($key){
          $content_strings = ContentString::byKey($key->key)->with('language')->get();
          if($content_strings){
              $json['success'] = true;
              $json['data'] = $content_strings;
              $json['error'] = null;
          }
        }
        

        return response()->json($json);
    }

    public function store(Request $request){
        $validation = Validator::make($request->all(),[
            'text' => 'required',
            'language_id' => 'required'
        ]);

        if($validation->fails()){
            return redirect()->back()->withErrors($validation->errors()->all());
        }
        else{
            $lang = StringLanguage::find($request->input('language_id'));
            $content_string = new ContentString;
            $content_string->language_id = $lang->id;
            $content_string->text = $request->input('text');
            if($request->has('key') && $request->input('key')!=null){
                $content_string->key = $request->input('key');
            }
            else{
                $content_string->create_key();
            }

            $content_string->save();

            return redirect()->back();
        }
    }

    public function show($lang, $key){
        $content_string = ContentString::byKey($key)->byLang($lang)->first();
        $json = ['success' => false, 'data' => null, 'error' => __('error.not_found')];
        if($content_string){
            $json['success'] = true;
            $json['data'] = $content_string;
            $json['error'] = null;
        }

        return response()->json($json);
        
    }
}
