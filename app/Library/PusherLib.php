<?php

namespace App\Library;

use Pusher\Pusher;

class PusherLib{

  public static function instanciate(){
    $options = array(
      'cluster' => env('PUSHER_CLUSTER'),
      'encrypted' => true,
      'useTLS' => true
    );

    return new Pusher(env('PUSHER_APP_KEY'),env('PUSHER_APP_SECRET'),env('PUSHER_APP_ID'),$options);
  }
}