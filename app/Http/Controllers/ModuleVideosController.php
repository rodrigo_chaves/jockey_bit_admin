<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\ModuleVideo;
use Validator;

class ModuleVideosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($module)
    {
        $videos = ModuleVideo::byModule($module)->orderBy('position','ASC')->paginate(10);

        return View('modulevideos.index')
                            ->with('videos',$videos)
                            ->with('module',$module);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($module)
    {
        return View('modulevideos.create')->with('module',$module);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $module)
    {
        $video = new ModuleVideo;
        $video->title = $request->input('title');
        $video->url = $request->input('url');
        $video->createEmbedLink($request->input('url'));
        $video->position = $request->input('position');
        $video->picture = $request->has('storage_file')? $request->input('storage_file') : null;
        $video->module_id = $module;
        $video->save();

        return redirect()->route('modulevideos.index',['module'=>$module])->with('success','Live salva');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($modulevideo)
    {
        $modulevideo = ModuleVideo::find($modulevideo);

        return View('modulevideos.edit')->with('modulevideo',$modulevideo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $modulevideo)
    {
        $video = ModuleVideo::find($modulevideo);

        $video->title = $request->input('title');
        $video->url = $request->input('url');
        $video->createEmbedLink($request->input('url'));
        $video->position = $request->input('position');
        // $video->picture = $request->input('picture');
        $video->picture = $request->has('storage_file')? $request->input('storage_file') : null;
        $video->save();

        return redirect()->route('modulevideos.index',['module'=>$video->module_id])->with('success','video salvo');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($modulevideo)
    {
        ModuleVideo::destroy($modulevideo);
        return redirect()->route('modulevideos.index')->with('success','Video removido');
    }
}
