@extends('layout.panel')

@section('content')
<div class="content-main " id="content-main">
  <div class="padding">
    @if(Session::has('error'))
      <div class="row">
        <div class="col-lg-12">
          <div class="alert alert-danger">{{Session::get('error')}}</div>
        </div>
      </div>
    @endif
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h2>Nova Lucro de tip</h2>
          </div>
          <div class="box-divider m-0"></div>
          <div class="box-body">
            <form action="{{route('dailytipsprofits.store')}}" method="post">
                {{ csrf_field() }}
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label>Dia</label>
                    <input type="date" class="form-control" name="due_date" id="due_date" placeholder="2018-03-12">
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label>Tipo</label>
                    <select class="form-control" name="dailytips_types_id">
                    @foreach($types as $type)
                      <option value="{{$type->id}}">{{$type->name}}</option>
                    @endforeach
                    </select>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label>Porcentagem (%)</label>
                    <input type="text" class="form-control" name="profit" id="profit" placeholder="12">
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label>Lucro acumulado (%)</label>
                    <input type="text" class="form-control" name="accumulated_profit" id="accumulated_profit" placeholder="12">
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-4">
                  <div class="form-group">
                    <label>Tips distribuidas</label>
                    <input type="text" class="form-control" name="sent_tips" id="sent_tips" placeholder="1">
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="form-group">
                    <label>Tips vencedoras</label>
                    <input type="text" class="form-control" name="winner_tips" id="winner_tips" placeholder="1">
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="form-group">
                    <label>Tips perderoras</label>
                    <input type="text" class="form-control" name="loose_tips" id="loose_tips" placeholder="1">
                  </div>
                </div>
              </div>

              

              

              
              
              <button type="submit" class="btn primary">Salvar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
