@extends('layout.panel')

@section('content')

<style>
  
</style>
<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/">Plataforma do Professor</a></li>
          <li class="breadcrumb-item active">Tip Star Videos</li>
        </ol>

          <div class="box">
            <div class="box-header">
              <h3>Tips Star Videos</h3>
            </div>
            <div class="box-tool p-0" style="top: 5px !important;">
                <a href="{{route('startipvideos.create')}}" class="btn btn-circle primary m-0"><i class="fa fa-plus"></i></a>
            </div>
            <div class="box-body p-0">
                <table id="" class="table v-middle p-0 m-0 box dataTable no-footer">
                    <thead>
                      <tr>
                        <tr>
                          <th align="center">Titulo</th>
                          <th align="center">Msg</th>
                          <th align="center">URL</th>
                          <th align="center" width="70px">Ações</th>
                        </tr>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($videos as $video)
                        <tr>
                            <td>{{$video->title}}</td>
                            <td>{{$video->msg}}</td>
                            <td>{{$video->url}}</td>
                            <td align="center">
                              <form action="{{route('startipvideos.destroy',['startipvideo'=>$video->id])}}" method="post">
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="delete">
                                <a href="{{route('startipvideos.edit',['startipvideo' => $video->id])}}" class="btn btn-primary btn-circle"><i class="fas fa-pencil-alt"></i></a>
                                <button type="submit" class="btn btn-danger btn-circle"><i class="fa fa-times"></i></button>
                              </form>
                            </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
            </div>
          </div> 
        
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-lg-12">
        {{$videos->links()}}
      </div>
    </div>
  </div>
</div>

@include('layout.elements.footer')
@endsection

@section('js')

<script>
    
</script>
@endsection
