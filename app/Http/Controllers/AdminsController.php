<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminsController extends Controller
{
    public function login(Request $request){
        if($request->isMethod('post')){
            $admin = \App\Model\Admin::where('email','LIKE',$request->input('email'))->first();
            if($admin && \Illuminate\Support\Facades\Hash::check($request->input('password'), $admin->password)){
                $admin->pasword = null;
                $request->session()->put('admin',$admin);
                return redirect()->route('dashboard');
            }
            else{
                return View('admins.login')->with('error','Email ou senha inválidos');
            }
        }
        else{
            return View('admins.login');
        }
        
    }
}
