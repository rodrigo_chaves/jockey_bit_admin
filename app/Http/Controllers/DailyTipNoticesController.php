<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\DailyTipNotice;
use App\Model\DailytipType;

class DailyTipNoticesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notices = DailyTipNotice::paginate(10);

        return View('notices.index')->with('notices', $notices);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dailytips_types = DailytipType::get();
        return View('notices.create')->with('types',$dailytips_types);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $notice = new DailyTipNotice;
        $notice->msg = $request->input('msg');
        $notice->dailytips_types_id = $request->input('dailytips_types_id');
        $notice->active = $request->has('active')? true: false;
        // if($request->has('video_url')){
        //     $notice->createEmbedLink($request->input('video_url'));
        // }
        
        $notice->save();

        return redirect()->route('dailytips_notice.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notice = DailyTipNotice::find($id);
        $dailytips_types = DailytipType::get();
        return View('notices.edit')->with('notice',$notice)->with('types',$dailytips_types);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $notice = DailyTipNotice::find($id);
        $notice->msg = $request->input('msg');
        $notice->dailytips_types_id = $request->input('dailytips_types_id');
        $notice->active = $request->has('active')? true: false;

        $notice->save();

        return redirect()->route('dailytips_notice.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function deactivate($id){
        $notice = DailyTipNotice::find($id);
        $notice->active = false;
        $notice->save();
  
        return redirect()->route('dailytips_notice.index')->with('success','Comunicado desativado');
      }
}
