<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\SystemInfo;
use Validator;

class SystemInfosController extends Controller
{

    public function show($key){
        $system_info = SystemInfo::byKey($key)->first();
        $json = ['success' => false, 'data'=> null, 'error' => 'not_found'];

        if($system_info){
            $json['success'] = true;
            $json['data'] = $system_info;
            $json['error'] = null;
        }

        return response()->json($json);
    }

    public function update(Request $request){

        $json = ['success' => false, 'data'=> null, 'error' => null];

        $validation = Validator::make($request->all(),[
            'val'=> 'required',
            'key' => 'required'
        ]);

        if($validation->fails()){
            $json['error'] = $validation->errors()->all()[0];
        }
        else{
            $system_info = SystemInfo::byKey($request->input('key'))->first();
            $system_info->val = $request->input('val');
            $system_info->save();

            $json['success'] = true;
            $json['data'] = $system_info;
        }

        return response()->json($json);
    }
}
