<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AnalysisTips extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analysis_tips',function(Blueprint $table){
            $table->increments('id');
            $table->string('back_odd')->nullable();
            $table->decimal('high_price',10,2)->nullable();
            $table->integer('percent')->nullable();
            $table->string('racingpost')->nullable();
            $table->string('position_odd')->nullable();
            $table->string('lay_odd')->nullable();
            $table->string('challenge_odd')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('analysis_tips');
    }
}
