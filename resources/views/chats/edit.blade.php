@extends('layout.panel')

@section('content')

<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
        @if(Session::has('error'))
          <div class="alert green">
            <b>{{Session::get('error')}}</b>
          </div>
        @endif
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Plataforma do Professor</a></li>
          <li class="breadcrumb-item active">Chats</li>
        </ol>
        <div class="row">
          <div class="col-lg-12">
            <div class="box">
              <div class="box-body">
                <form action="{{route('chats.update',['id'=>$chat->id])}}" method="post" class="form-inline">
                  <input type="hidden" name="_method" value="put">
                  {{ csrf_field() }}
                  <div class="form-check mx-sm-2">
                      <input type="checkbox" name="active" class="form-check-input" id="active" {{$chat->active? 'checked' : ''}}>
                      <label class="form-check-label" for="active">Ativo</label>
                    </div>
                  <div class="form-group">
                    <input type="text" id="name" name="name" placeholder="Chat Maneiro" class="form-control" value="{{$chat->name}}">
                  </div>
                  <div class="form-group mx-sm-2">
                    <button class="btn btn-success">Atualizar</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
