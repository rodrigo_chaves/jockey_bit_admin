@extends('layout.panel')

@section('content')

<div class="content-main " id="content-main">
  <div class="padding">
  
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>

    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <h1 class="display-4 l-s-n-1x">

          <a href="{{route('pdf.create',['module_id'=>$module_id])}}" class="btn btn-fw primary">Novo Pdf</a>
        </h1>
        <table id="" class="table v-middle p-0 m-0 box dataTable no-footer" data-plugin="dataTable" role="grid" aria-describedby="datatable_info" style="width: 1094px;">
          <thead>
            <tr>
              <td align="center">Titulo</td>
              <td align="center">Descrição</td>
              <td align="center">Link</td>
              <td align="center"></td>
            </tr>
          </thead>
          <tbody>
            @foreach($pdfs as $pdf)
              <tr>
                  <td align="center">{{$pdf->title}}</td>
                  <td align="center">{{$pdf->description}}</td>
                  <td align="center"><a href="{{$pdf->filename}}">{{$pdf->title}}</a></td>
                  <td align="right">
                    <form action="{{route('pdf.destroy',['module_id'=>$module_id,'pdf'=>$pdf->id])}}" method="post">
                      <input type="hidden" name="_method" value="delete">
                      {{csrf_field()}}
                      <!-- <a href="{{route('pdf.edit',['module_id'=>$module_id,'pdf'=>$pdf->id])}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a> -->
                      <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>
                    </form>
                  </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  @include('layout.elements.footer')
</div>
@endsection
