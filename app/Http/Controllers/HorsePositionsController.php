<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\HorsePosition;
use App\Model\RacingTrack;
use App\Model\Horse;
use Validator;

class HorsePositionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $horse_positions = HorsePosition::latest()->paginate(20);

        return View('horsepositions.index')->with('horse_positions', $horse_positions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tracks = RacingTrack::get();
        $horses = Horse::get();

        return View('horsepositions.create')
                    ->with('tracks',$tracks)
                    ->with('horses',$horses);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'date' => 'required',
            'time' => 'required',
            'horse_id' => 'required',
            'racing_track_id' => 'required'
        ]);

        if($validation->fails()){
            return redirect()->back()->with('error',$validation->errors()->all()[0]);
        }
        else{
            $horse_position = new HorsePosition;
            $horse_position->date = $request->input('date');
            $horse_position->time = $request->input('time');
            $horse_position->racing_track_id = $request->input('racing_track_id');
            $horse_position->horse_id = $request->input('horse_id');
            $horse_position->front = $request->has('front')? $request->input('front') : null;
            $horse_position->stalker = $request->has('stalker')? $request->input('stalker') : null;

            $horse_position->save();

            return redirect()->route('horsepositions.index')->with('success','Posição salva');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $horse_position = HorsePosition::find($id);
        $tracks = RacingTrack::get();
        $horses = Horse::get();
        return View('horsepositions.edit')
            ->with('horse_position', $horse_position)
            ->with('tracks',$tracks)
            ->with('horses',$horses);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(),[
            'date' => 'required',
            'time' => 'required',
            'horse_id' => 'required',
            'racing_track_id' => 'required'
        ]);

        if($validation->fails()){
            return redirect()->back()->with('error',$validation->errors()->all()[0]);
        }
        else{
            $horse_position = HorsePosition::find($id);
            $horse_position->date = $request->input('date');
            $horse_position->time = $request->input('time');
            $horse_position->racing_track_id = $request->input('racing_track_id');
            $horse_position->horse_id = $request->input('horse_id');
            $horse_position->front = $request->has('front')? $request->input('front') : null;
            $horse_position->stalker = $request->has('stalker')? $request->input('stalker') : null;

            $horse_position->save();

            return redirect()->route('horsepositions.index')->with('success','Posição atualizada');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        HorsePosition::destroy($id);

        return redirect()->route('horsepositions.index')->with('success','Horse Position removida');
    }
}
