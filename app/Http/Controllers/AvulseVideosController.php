<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\AvulseVideo;

class AvulseVideosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = AvulseVideo::latest()->paginate(10);

        return View('avulse_videos.index')->with('videos', $videos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('avulse_videos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $avulsevideo = new AvulseVideo;
        $avulsevideo->active = $request->has('active')? true: false;
        $avulsevideo->title = $request->input('title');
        $avulsevideo->msg = $request->input('msg');
        $avulsevideo->url = $request->input('url');
        $avulsevideo->createEmbedLink($request->input('url'));

        $avulsevideo->save();

        return redirect()->route('avulsevideos.index')->with('success','Video salvo');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $avulsevideo = AvulseVideo::find($id);

        return View('avulse_videos.edit')->with('video',$avulsevideo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $avulsevideo = AvulseVideo::find($id);
        $avulsevideo->active = $request->has('active')? true: false;
        $avulsevideo->title = $request->input('title');
        $avulsevideo->msg = $request->input('msg');
        $avulsevideo->url = $request->input('url');
        $avulsevideo->createEmbedLink($request->input('url'));

        $avulsevideo->save();

        return redirect()->route('avulsevideos.index')->with('success','Video atualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $video = AvulseVideo::find($id);
        if($video){
            AvulseVideo::destroy($id);
        }
  
        return redirect()->route('avulsevideos.index')->with('success','Video removido');
    }
}
