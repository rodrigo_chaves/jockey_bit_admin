<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeClasseFormat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('race_analysis',function(Blueprint $table){
            $table->dropColumn('classe');
            $table->integer('classe_id')->unsigned()->nullable();
            $table->foreign('classe_id')->references('id')->on('analysis_classes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('race_analysis',function(Blueprint $table){
            $table->dropForeign(['classe_id']);
            $table->string('classe')->nullable();
        });
    }
}
