@extends('layout.panel')

@section('content')

<div class="content-main " id="content-main">
  <div class="padding">
  
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>

    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <h1 class="display-4 l-s-n-1x">

          <a href="{{route('questions.create',['module'=>$module])}}" class="btn btn-fw primary">Nova Pergunta</a>
        </h1>
        <table id="" class="table v-middle p-0 m-0 box dataTable no-footer" data-plugin="dataTable" role="grid" aria-describedby="datatable_info" style="width: 1094px;">
          <thead>
            <tr>
              <td>#</td>
              <td align="center">Titulo</td>
              <td align="center"></td>
            </tr>
          </thead>
          <tbody>
            @foreach($questions as $question)
              <tr>
                  <td>{{$question->id}}</td>
                  <td align="center">{{$question->title}}</td>
                  <td align="right">
                    <form action="{{route('questions.destroy',['question'=>$question->id])}}" method="post">
                      <input type="hidden" name="_method" value="delete">
                      {{csrf_field()}}
                      <a href="{{route('answers.index',['id'=>$question->id])}}" class="btn purple btn-circle" title="Respostas">
                          <i class="fas fa-list-alt"></i>
                      </a>
                      <a href="{{route('questions.edit',['id'=>$question->id])}}" class="btn btn-primary btn-circle">
                          <i class="fas fa-pen"></i>
                      </a>
                      <button type="submit" class="btn btn-danger btn-circle">
                        <i class="far fa-trash-alt"></i>
                      </button>
                    </form>
                  </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
