<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

class StringLanguagesController extends Controller
{
    public function index_json(){
        $languages = \App\Model\StringLanguage::get();
        $json = ['success' => false, 'data' => null, 'error' => __('error.not_found')];
        if($languages){
            $json['success'] = true;
            $json['data'] = $languages;
            $json['error'] = null;
        }
        return response()->json($json);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $languages = \App\Model\StringLanguage::paginate(10);

        return View('languages.index')->with('languages',$languages);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('languages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'name' => 'required',
            'nickname' => 'required|unique:string_languages'
        ]);

        if($validation->fails()){
            return redirect()->back()->withErrors($validation->errors()->all());
        }
        else{
            $string_language = new \App\Model\StringLanguage;
            $string_language->name = $request->input('name');
            $string_language->nickname = $request->input('nickname');
            $string_language->icon = $request->has('icon')? $request->input('icon') : null;
            $string_language->save();

            return redirect()->route('languages.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
