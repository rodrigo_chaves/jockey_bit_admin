@extends('layout.panel')

@section('content')

<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <ol class="breadcrumb"><li class="breadcrumb-item"><a href="#">Plataforma do Professor</a></li><li class="breadcrumb-item active">Comunicados de Tipos</li></ol>
        <a href="{{route('dailytips_notice.create')}}" class="btn btn-fw primary" style="margin-bottom: 40px;"><i class="fa fa-plus"></i> Adicionar novo comunicado</a>
        <table id="" class="table v-middle p-0 m-0 box dataTable no-footer">
          <thead>
            <tr>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Conteudo do comunicado</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Tipo</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Data e horario</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Status</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Ações</td>
              
            </tr>
          </thead>
          <tbody>
                @foreach($notices as $dailytips_notice)
                <tr>
                  <td align="center"><?php echo html_entity_decode($dailytips_notice->msg);?></td>
                  <td align="center">{{$dailytips_notice->type->name}}</td>
                  <td align="center">{{$dailytips_notice->created_at}}</td>
                  <td align="center">{{$dailytips_notice->active? 'Ativo' : 'Inativo'}}</td>
                  <td align="right">
                    
                    <form action="{{route('dailytips_notice.destroy',['id'=>$dailytips_notice->id])}}" method="post">
                      {{csrf_field()}}

                      <input type="hidden" name="_method" value="delete">
                      
                      @if($dailytips_notice->active)
                        <a class="btn btn-warning" href="{{route('deactivate_dailytips_notice',['id'=>$dailytips_notice->id])}}" style="color:#fff"><i class="fa fa-power-off"></i></a>
                      @endif


                      <a href="{{route('dailytips_notice.edit',['id'=>$dailytips_notice->id])}}" class="btn btn-primary" style="color:#fff"><i class="fa fa-pencil"></i></a>

                      <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>

                      
                    </form>
                  </td>
                </tr>
                @endforeach
          </tbody>
        </table>

      </div>
    </div>
  </div>
  @include('layout.elements.footer')
</div>
@endsection
