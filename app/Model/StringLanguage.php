<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StringLanguage extends Model
{
    protected $table = 'string_languages';

    protected $fillable = ['name','nickname','icon'];
}
