<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToDailytipsTypesProfitsAgain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dailytips_types_profits',function(Blueprint $table){
            $table->integer('sent_tips')->nullable();
            $table->integer('winner_tips')->nullable();
            $table->integer('loose_tips')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dailytips_types_profits',function(Blueprint $table){
            $table->dropColumn('sent_tips');
            $table->dropColumn('winner_tips');
            $table->dropColumn('loose_tips');
        });
    }
}
