<?php

namespace App\Http\Controllers\Chat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Message;
use App\Events\MessageSentEvent;
use App\Model\Admin;
use App\User;
// use Pusher\Pusher;
use Illuminate\Support\Facades\Log;
use App\Library\PusherLib;

class MessagesController extends Controller
{
    public function fetch(Request $request, $chat_id){
        return Message::with('user','admin')->get();
    }

    public function admin_last(){
        return Message::latest()->where('is_admin',true)->first();
    }

    public function sentMessage(Request $request, $chat_id){
        // $admin = $request->session()->get('admin');
        Log::info(json_encode($request->all()));
        $user_id = $request->input('user_id');
        $is_admin = $request->has('is_admin')? $request->input('is_admin') : false;
        $is_user = ($is_admin)? false : true;
        if($is_admin){
            $is_user = false;
            $user = Admin::find($user_id);
        }
        else if($is_user){
            $is_admin = false;
            $user = User::find($user_id);
        }

        Log::info(json_encode($request->all()));
        
        $message = new Message;
        $message->is_admin = $is_admin;
        $message->is_user = $is_user;
        $message->user_id = $user_id;
        $message->message = $request->input('message');
        $message->save();

        $message = Message::where('id',$message->id)->with('user','admin')->first();

        $pusher = PusherLib::instanciate();
        
        $pusher->trigger(env('CHAT_CHANNEL') , 'MessageSentEvent', $message);

        if($message->is_admin){
            $pusher->trigger(env('CHAT_CHANNEL') , 'AdminMessageEvent', $message);
        }
        //broadcast(new MessageSentEvent($admin,$message));
    }

    public function destroy($id, $user_id){
        $admin = Admin::find($user_id);
        $json = ['status' => false, 'error' => 'not authorized'];
        if($admin){
          //Message::destroy($id);
          $message = Message::find($id);
          $message->message = '<i><strong>Essa mensagem foi removida pelo admin</strong></i>';
          $message->save();
          $json['status'] = true;
          $json['error'] = null;

          $data['id'] = $id;

          $pusher = PusherLib::instanciate();

          $pusher->trigger(env('CHAT_CHANNEL') , 'EventMessageDeleted', $data);
        }
        

        return response()->json($json);
    }
}
