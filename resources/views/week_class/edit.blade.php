@extends('layout.panel')

@section('content')
<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h2>Nova Aula semanal</h2>
          </div>
          <div class="box-divider m-0"></div>
          <div class="box-body">
            <form action="{{route('week_class.update',['id'=>$week_class->id])}}" method="post">
              <input type="hidden" name="_method" value="put">
                {{ csrf_field() }}
              <div class="form-group">
                <label>Titulo</label>
                <input type="text" class="form-control translate" name="title" id="title" placeholder="Titulo" value="{{$week_class->title}}">
              </div>

              <div class="form-group">
                <label>Url</label>
                <input type="text" class="form-control" name="url" id="url" placeholder="url" value="{{$week_class->url}}">
              </div>

              <div class="form-group">
                <label>Data</label>
                <input type="text" class="form-control" name="due_date" id="due_date" placeholder="Data, Ex: YYYY-MM-DD" value="{{$week_class->due_date}}">
              </div>
              
              <button type="submit" class="btn green">Atualizar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.13/jquery.mask.js"></script>
<script type="text/javascript">
  $(function () {
      $('#due_date').mask('0000-00-00');
  });
</script>

@endsection
