<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToModuleTest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_has_tests',function(Blueprint $table){
            $table->time('time_to_execute')->nullable();
            $table->boolean('approved')->default(false)->after('time_to_execute');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_has_tests',function(Blueprint $table){
            $table->dropColumn('status');
            $table->dropColumn('approved');
        });
    }
}
