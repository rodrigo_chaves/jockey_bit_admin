<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDailytipsNotice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dailytips_notice',function(Blueprint $table){
            $table->increments('id');
            $table->integer('dailytips_types_id')->unsigned()->nullable();
            $table->foreign('dailytips_types_id')->references('id')->on('dailytips_types');
            $table->string('msg');
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dailytips_notice');
    }
}
