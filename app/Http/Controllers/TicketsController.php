<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Ticket;
use App\Model\TicketStatus;

class TicketsController extends Controller
{
    public function index(){
        $tickets = Ticket::notHidden()->orderBy('id','DESC')->paginate(20);
        return View('tickets.index')->with('tickets',$tickets);
    }

    public function show($id){
        $ticket = Ticket::where('id',$id)->with('messages')->first();
        return View('tickets.show')->with('ticket',$ticket);
    }

    public function close($id){
        $ticket = Ticket::find($id);
        // $status = TicketStatus::where('title','Closed')->first();

        // $ticket->status_id = $status->id;
        // $ticket->save();
        $ticket->changeStatus('Closed');

        return redirect()->back()->with('success','Ticket fechado, só o chuck norris poderá reabri-lo');
    }

    public function hide($id){
        $ticket = Ticket::find($id);
        $ticket->hidden_for_admin = true;

        $ticket->save();

        return redirect()->back()->with('success','Ticket ocultado');
    }
}
