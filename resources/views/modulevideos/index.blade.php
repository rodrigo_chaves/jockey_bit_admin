@extends('layout.panel')

@section('content')

<div class="content-main " id="content-main">
  <div class="padding">
  
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>

    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Plataforma do Professor</a></li>
          <li class="breadcrumb-item active">Videos do Modulo</li>
        </ol>
        <a href="{{route('modulevideos.create',['module' =>$module])}}" class="btn btn-fw primary" style="margin-bottom: 40px;"><i class="fa fa-plus"></i> Adicionar Novo Video</a>
        <table id="" class="table v-middle p-0 m-0 box dataTable no-footer" data-plugin="dataTable" role="grid" aria-describedby="datatable_info" style="width: 1094px;">
          <thead>
            <tr>
              <td>Posiçao</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Título</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">URL</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Ação</td>
            </tr>
          </thead>
          <tbody>
            @foreach($videos as $video)
              <tr>
                  <td>{{$video->position}}</td>
                  <td align="center">{{$video->title}}</td>
                  <td align="center">{{$video->url}}</td>
                  <td align="right">
                    <form action="{{route('modulevideos.destroy',['video'=>$video->id])}}" method="post">
                      <input type="hidden" name="_method" value="delete">
                      {{csrf_field()}}
                      <a href="{{route('modulevideos.edit',['video'=>$video->id])}}" class="btn btn-primary btn-circle">
                          <i class="fas fa-pen"></i>
                      </a>
                      <button type="submit" class="btn btn-danger btn-circle">
                        <i class="far fa-trash-alt"></i>
                      </button>
                    </form>
                  </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        {{$videos->links()}}
      </div>
    </div>
  </div>
  @include('layout.elements.footer')
</div>
@endsection
