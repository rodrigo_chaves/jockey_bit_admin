@extends(env("TEMPLATE_PATH")."external_layout")

@section('custom_css')

@endsection
@section('main-content')



<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>{{Lang::get('shared.erro500')}} | Academy</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link rel="stylesheet" href="{{url('404')}}/css/base.css">
        <link rel="stylesheet" href="{{url('404')}}/css/main.css">
        <link rel="stylesheet" href="{{url('404')}}/css/vendor.css">
        <script src="{{url('404')}}/js/modernizr.js"></script>
    </head>

    <body>
        <main id="main-404-content" class="main-content-static">
            <div class="content-wrap">
                <div class="shadow-overlay"></div>
                <div class="main-content">
                    <div class="row">
                        <div class="col-twelve">
                            <h1 class="kern-this">404.</h1>
                            <p>{{Lang::get("shared.pagina_nao_encontrada")}}<a href="javascript:window.history.back(-1)" style="color: #70c4ef"><br>{{Lang::get("shared.voltar_pagina_anterior")}}</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>
</html>

@endsection