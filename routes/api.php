<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('stringlanguages')->group(function(){
    Route::get('/','StringLanguagesController@index_json');
});

Route::prefix('contentstrings')->group(function(){
    Route::get('/{text}','ContentStringsController@index');
});

Route::prefix('dailytips')->group(function(){
    Route::put('/{dailytip}','DailyTipsController@update_api');
});

// Route::group(['middleware'=>'chat'],function(){
    Route::get('amdminlast','Chat\MessagesController@admin_last');
    Route::get('/{chat_id}/messages','Chat\MessagesController@fetch');
    Route::post('{chat_id}/messages','Chat\MessagesController@sentMessage');

    Route::delete('/messages/{id}/{user_id}','Chat\MessagesController@destroy');

    Route::post('/blockuser','Chat\ChatsController@block_user');
// });
