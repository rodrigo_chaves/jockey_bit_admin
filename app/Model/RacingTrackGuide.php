<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RacingTrackGuide extends Model
{
    protected $table = 'racing_tracks_guides';
}
