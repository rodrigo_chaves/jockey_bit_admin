<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMapsRacingTracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('racing_tracks_guides',function(Blueprint $table){
            $table->increments('id');
            $table->string('title');
            $table->text('img');
            $table->text('timeform')->nullable();
            $table->decimal('latitude',10,8);
            $table->decimal('longitude',10,8);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('racing_tracks_guides');
    }
}
