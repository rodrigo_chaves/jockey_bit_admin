<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProvaModulo1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('prova_modulo_1')) {

            Schema::create('prova_modulo_1',function(Blueprint $table){
                $table->increments('id');
                $table->string('pergunta');
                $table->string('opcao_a');
                $table->string('opcao_b');
                $table->string('opcao_c');
                $table->string('opcao_d');
                $table->string('resposta_certa');
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('prova_modulo_1')) {
            Schema::drop('prova_modulo_1');
        }
    }
}
