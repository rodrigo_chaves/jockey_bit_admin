@extends('layout.panel')

@section('content')

<style>
  
</style>
<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/">Plataforma do Professor</a></li>
          <li class="breadcrumb-item active">Videos Avulsos</li>
        </ol>

        <div class="box">
          <div class="box-header">
              <h3>Avulse Video Info</h3>
          </div>
          <div class="box-body">
            <form  action="{{route('systeminfo.update')}}" method="post" id="system_info" data-key="avulse_videos">
              {{csrf_field()}}
              <div class="row">
                <div class="col-lg-11">
                  <div class="form-group">
                    <input type="hidden" name="key" value="">
                    <textarea class="form-control" name="val" placeholder="Info"></textarea>
                  </div>
                </div>
                <div class="col-lg-1">
                  <button class="btn btn-success btn-circle">
                    <i class="fas fa-check"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>

          <div class="box">
            <div class="box-header">
              <h3>Videos Avulsos</h3>
            </div>
            <div class="box-tool p-0" style="top: 5px !important;">
                <a href="{{route('avulsevideos.create')}}" class="btn btn-circle primary m-0"><i class="fa fa-plus"></i></a>
            </div>
            <div class="box-body p-0">
                <table id="" class="table v-middle p-0 m-0 box dataTable no-footer">
                    <thead>
                      <tr>
                        <tr>
                          <th align="center">Titulo</th>
                          <th align="center">Msg</th>
                          <th align="center">URL</th>
                          <th align="center" width="70px">Ações</th>
                        </tr>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($videos as $video)
                        <tr>
                            <td>{{$video->title}}</td>
                            <td>{{$video->msg}}</td>
                            <td>{{$video->url}}</td>
                            <td align="center">
                              <form action="{{route('avulsevideos.destroy',['avulsevideo'=>$video->id])}}" method="post">
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="delete">
                                <a href="{{route('avulsevideos.edit',['avulsevideo' => $video->id])}}" class="btn btn-primary btn-circle"><i class="fas fa-pencil-alt"></i></a>
                                <button type="submit" class="btn btn-danger btn-circle"><i class="fa fa-times"></i></button>
                              </form>
                            </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
            </div>
          </div> 
        
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-lg-12">
        {{$videos->links()}}
      </div>
    </div>
  </div>
</div>

@include('layout.elements.footer')
@endsection

@section('js')

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.all.min.js"></script>

<script>
    $(document).ready(function(){
      var key = $('form#system_info').data('key');
      $.get('/systeminfo/' + key, function(data){
        
        if(data.success){
          $('input[name=key]').val(data.data.key);
          $('textarea[name=val]').val(data.data.val);
        }
      });

      $('form#system_info').submit(function(evt){
        evt.preventDefault();
        console.log('salvando');
        $.ajax({
          'url' : $('form#system_info').attr('action'),
          'type' : 'PUT',
          'data' : $('form#system_info').serialize()
        })
        .done(function(result){
          console.log(result);
          if(result.success){
            Swal(
              'Informativo salvo!',
              '',
              'success'
            )
          }
        });
      });

    });
</script>
@endsection
