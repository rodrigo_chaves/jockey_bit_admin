<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new \App\Model\Admin;
        $admin->email = 'adm@jockeybit.com';
        $admin->password = \Illuminate\Support\Facades\Hash::make('123456');
        $admin->active = true;
        $admin->save();
    }
}
