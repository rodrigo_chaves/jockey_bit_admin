<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SystemInfo extends Model
{
    protected $table = 'system_infos';

    public function scopeByKey($query, $key){
        return $query->where('key', $key);
    }
}
