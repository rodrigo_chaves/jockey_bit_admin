<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModuleQuestionsAnswers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_questions_answers',function(Blueprint $table){
            $table->increments('id');
            $table->integer('module_question_id')->unsigned();
            $table->foreign('module_question_id')->references('id')->on('module_questions');
            $table->string('title');
            $table->boolean('correct')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('module_questions_answers');
    }
}
