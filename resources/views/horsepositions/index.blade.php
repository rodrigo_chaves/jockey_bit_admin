@extends('layout.panel')

@section('content')

<div class="content-main " id="content-main">
  <div class="padding">
  
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>

    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Plataforma do Professor</a></li>
          <li class="breadcrumb-item active">Trading Largada</li></ol>
        <a href="{{route('horsepositions.create')}}" class="btn btn-fw primary" style="margin-bottom: 40px;">
          <i class="fa fa-plus"></i> Adicionar Horse Position</a>
        <table id="" class="table v-middle p-0 m-0 box dataTable no-footer" data-plugin="dataTable" role="grid" aria-describedby="datatable_info">
          <thead>
            <tr>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Data</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Hora</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Pista</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Cavalo</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Front</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Stalker</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;"></td>
            </tr>
          </thead>
          <tbody>
            @foreach($horse_positions as $horseposition)
              <tr>
                  <td align="center">{{$horseposition->date}}</td>
                  <td align="center">{{$horseposition->time}}</td>
                  <td align="center">{{$horseposition->track->name}}</td>
                  <td align="center">{{$horseposition->horse->name}}</td>
                  <td align="center">{{$horseposition->front}}</td>
                  <td align="center">{{$horseposition->stalker}}</td>
                  <td align="right">
                    <form action="{{route('horsepositions.destroy',['horseposition'=>$horseposition->id])}}" method="post">
                      <input type="hidden" name="_method" value="delete">
                      {{csrf_field()}}
                      <a href="{{route('horsepositions.edit',['id'=>$horseposition->id])}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                      <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>
                    </form>
                  </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        {{$horse_positions->links()}}
      </div>
    </div>
  </div>
</div>
@endsection
