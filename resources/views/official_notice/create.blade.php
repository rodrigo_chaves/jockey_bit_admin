@extends('layout.panel')

@section('content')
<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h2>Nova Aula semanal</h2>
          </div>
          <div class="box-divider m-0"></div>
          <div class="box-body">
            <form action="{{route('official_notice.store')}}" method="post">
                {{ csrf_field() }}
              
              <div class="checkbox"><label class="ui-check ui-check-md"><input type="checkbox" name="active" id="active"> <i class="dark-white"></i> Ativo</label></div>

              <div class="form-group">
                <label>Novo Comunicado</label>
                <textarea id="msg" name="msg" class="form-control"></textarea>
              </div>

            
              <button type="submit" class="btn primary">Salvar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="/template/libs/tinymce/tinymce.min.js"></script>
<script>tinymce.init({ selector:'#msg' });</script>

@endsection
