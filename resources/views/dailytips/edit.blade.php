@extends('layout.panel')

@section('content')
<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">

<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h2>Nova Tip</h2>
          </div>
          <div class="box-divider m-0"></div>
          <div class="box-body">
            <form action="{{route('dailytips.update',['id'=>$dailytip->id])}}" method="post">
              <input type="hidden" name="_method" value="put">
                {{ csrf_field() }}
              <div class="form-group">
                <label>Cavalo</label>
                <input type="text" class="form-control" name="horse" id="horse" placeholder="Nome do cavalo" value="{{$dailytip->horse}}">
              </div>
              <div class="form-group">
                <label>Corrida</label>
                <input type="text" class="form-control" name="run" id="run" placeholder="Corrida" value="{{$dailytip->run}}">
              </div>
              <div class="form-group">
                <label>Resultado</label>
                <input type="text" class="form-control" name="result" id="result" placeholder="Corrida" value="{{$dailytip->result}}">
              </div>


              <div class="form-group">
                <label>ODD</label>
                <input type="text" class="form-control" name="odd" id="odd" value="{{$dailytip->odd}}">
              </div>

              <div class="form-group">

                <label>Data / Hora</label>
                <input type="datetime" class="form-control" name="due_date" id="due_date" placeholder="EX: 2017/01/31 20:35" value="{{$dailytip->due_date}}">

              </div>
              
              <button type="submit" class="btn green">Atualizar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.13/jquery.mask.js"></script>
<script type="text/javascript">
  $(function () {
      $('input[type=datetime]').mask('0000-00-00 00:00');
  });
</script>

@endsection
