@extends('layout.panel')

@section('content')

<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Plataforma do Admin</a></li>
          <li class="breadcrumb-item active">analises</li>
        </ol>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
          <!-- <div class="box">
            <div class="box-body">
              <form action="{{route('analysistips.store')}}" method="post">
                {{ csrf_field() }}
                <div class="row">
                  <div class="col-lg-2">
                    <div class="form-group">
                      <input type="text" name="back_odd" class="form-control" placeholder="Back">
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group">
                      <input type="text" name="high_price" class="form-control" placeholder="Valor Alto : 0.00">
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group">
                      <input type="text" name="percent" class="form-control" placeholder="Porcentagem">
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group">
                      <input type="text" name="racingpost" class="form-control" placeholder="racingpost">
                     </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group">
                      <input type="text" name="position_odd" placeholder="Posição" class="form-control">
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group mx-sm-2">
                      <input type="text" name="lay_odd" placeholder="Lay" class="form-control">
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group mx-sm-2">
                      <input type="text" name="challenge_odd" placeholder="Challenge" class="form-control">
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <div class="form-group">
                      <button class="btn btn-success">Salvar</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div> -->

        <table id="" class="table v-middle p-0 m-0 box dataTable no-footer">
          <thead>
            <tr>
              <!-- <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">#</td> -->
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Data</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Back odd</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Valor Alto</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Porcentagem</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Racingpost</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Posição</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Lay</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Challenge</td>
            </tr>
          </thead>
          <tbody>
            @if($analysis_tips)
              @foreach($analysis_tips as $analysis_tip)
                <tr>
                    
                    <td align="center">{{date('d/m H:i',strtotime($analysis_tip->created_at))}}</td>
                    <td align="center">{{$analysis_tip->back_odd}}</td>
                    <td align="center">{{$analysis_tip->high_price}}</td>
                    <td align="center">{{$analysis_tip->percent}}</td>
                    <td align="center">{{$analysis_tip->racingpost}}</td>
                    <td align="center">{{$analysis_tip->position_odd}}</td>
                    <td align="center">{{$analysis_tip->lay_odd}}</td>
                    <td align="center">{{$analysis_tip->challenge_odd}}</td>
                    <td align="right">
                      <form action="{{route('analysistips.destroy',['id'=>$analysis_tip->id])}}" method="post">
                        {{csrf_field()}}

                        <input type="hidden" name="_method" value="delete">
                        @if(!$analysis_tip->validated)
                          <a title="Validar" href="{{route('validatetip',['id'=>$analysis_tip->id])}}" class="btn btn-success"><i class="fa fa-check"></i></a>
                        @endif
                        <a href="{{route('analysistips.edit',['id'=>$analysis_tip->id])}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                        <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>
                      </form>
                    </td>
                </tr>
              @endforeach
            @endif
          </tbody>
        </table>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        {{$analysis_tips->links()}}
      </div>
    </div>
  </div>
</div>
@endsection
