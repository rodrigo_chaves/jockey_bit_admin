<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NextOpHistory extends Model
{
    protected $table = 'next_operation_history';

    public function scopeAfterToday($query){
        $today = Carbon::now();
        //$carbon = Carbon::createFromFormat('Y-m-d H:i:s', $today)->toDateTimeString();

        return $query->whereDate('due_date','>=',$today->format('Y-m-d'));
    }
}
