<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\PdfModule;

class PdfModulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($module_id)
    {
        $pdfs = PdfModule::byModule($module_id)->latest()->paginate(10);

        return View('pdfmodules.index')->with('pdfs',$pdfs)->with('module_id',$module_id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($module_id)
    {
        return View('pdfmodules.create')->with('module_id',$module_id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $module_id)
    {
        $pdf = new PdfModule;
        $pdf->module_id = $module_id;
        $pdf->title = $request->input('title');
        $pdf->description = $request->input('description');
        // $ext = $request->picture->getClientOriginalExtension();
        // $newName = time() . rand(11111, 99999) .'.'. $ext;
        // $request->picture->move(public_path('/storage/img'),$newName);
        $pdf->filename = $request->input('storage_file');

        $pdf->save();

        return redirect()->route('pdf.index',['module_id'=>$module_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($module_id, $pdf)
    {
        
        PdfModule::destroy($pdf);

        return redirect()->route('pdf.index',['module_id'=>$module_id]);
    }
}
