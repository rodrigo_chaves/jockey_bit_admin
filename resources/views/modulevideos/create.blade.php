@extends('layout.panel')

@section('content')
<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h2>Novo Video</h2>
          </div>
          <div class="box-divider m-0"></div>
          <div class="box-body">
            <form action="{{route('modulevideos.store',['module'=>$module])}}" method="post">
                {{ csrf_field() }}
              <div class="form-group">
                <label>Titulo</label>
                <input type="text" class="form-control" name="title" id="horse" placeholder="Titulo" required>
              </div>

              <div class="form-group">
                <label>Url</label>
                <input type="text" class="form-control" name="url" id="run" placeholder="url" required>
              </div>

              <div class="form-group">
                <label>Posiçao</label>
                <input type="number" class="form-control" name="position" id="position" min="0" required>
              </div>

              <!-- <div class="form-group">
                <label>Imagem</label>
                <input type="text" class="form-control" name="picture" id="result" placeholder="Imagem">
              </div> -->
              <div class="form-group">
                <label>Imagem</label>
                <i id="uploading-image" class="fa fa-circle-o-notch fa-spin"></i>
                <input type="file" name="img" class="form-control" accept=".jpg, .JPG, .JPEG, .jpeg, .png, .PNG, .GIF, .gif">
                <input type="hidden" name="storage_file">
                
              </div>

              <!-- <div class="form-group">
                <label><input type="checkbox" name="active" checked> Ativo</label>
                
              </div> -->
              
              <button type="submit" id="btn_save" class="btn primary">Salvar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')

<script src="https://www.gstatic.com/firebasejs/4.10.1/firebase.js"></script>

<script type="text/javascript">
// Set the configuration for your app
  $(document).ready(function(){
    $("#uploading-image").fadeOut();
    var config = {
      apiKey: "AIzaSyCpEP6yWsE1fv8vnKfd-ELoAdB_mFJctHQ",
      authDomain: "academy-jockeybit.firebaseapp.com",
      databaseURL: "https://<DATABASE_NAME>.firebaseio.com",
      storageBucket: "gs://academy-jockeybit.appspot.com/",
      messagingSenderId: "<SENDER_ID>",
    };
    firebase.initializeApp(config);

    // Get a reference to the storage service, which is used to create references in your storage bucket
    var storage = firebase.storage().ref();
    var timestamp = Number(new Date());
    var moduleVideosRef = storage.child('modulevideos/' + timestamp + ".jpg");

    $('input[name=img]').change(function(){
      $('#btn_save').attr('disabled','disabled');
      $('#uploading-image').fadeIn();
      var file_data = $('input[name=img]').prop('files')[0];
      moduleVideosRef.put(file_data).then(function(snapshot) {
        // console.log('Uploaded a blob or file!');
        // console.log(snapshot);
        $('input[name=storage_file]').val(snapshot.downloadURL);
        console.log(snapshot.downloadURL);
        //$('button[type=submit]').removeAttr('disabled');
        $('#btn_save').removeAttr('disabled');
        $('#uploading-image').fadeOut();
      });
    });

  });
  
</script>
@endsection
