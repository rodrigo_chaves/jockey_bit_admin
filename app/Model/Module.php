<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Module extends Model
{
    protected $table = 'modules';

    public function questions(){
        return $this->hasMany('App\Model\Question');
    }

    public function scopeActive($query){
        return $query->where('active',true);
    }

    public static function usersOnModules(){
        // SELECT count(users.id) as amount, modules.title 
        // from users inner join modules on users.current_module = modules.id 
        //group by modules.title 
        $results = DB::table('users')
                    ->join('modules','users.current_module','=','modules.id')
                    ->selectRaw('modules.title as module, '. DB::raw('count("users.id") as amount'))
                    ->where('modules.active',true)
                    ->groupBy(DB::raw('module'))
                    ->get();

        return $results;
    }
}
