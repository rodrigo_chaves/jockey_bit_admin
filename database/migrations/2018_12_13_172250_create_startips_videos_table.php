<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStartipsVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('startips_videos',function(Blueprint $table){
            $table->increments('id');
            $table->boolean('active');
            $table->string('title')->nullable();
            $table->string('msg')->nullable();
            $table->string('url')->nullable();
            $table->string('embed_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('startips_videos');
    }
}
