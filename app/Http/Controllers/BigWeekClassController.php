<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\BigWeekClass;

class BigWeekClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $big_week_classes = BigWeekClass::paginate(10);

        return View('big_week_class.index')->with('big_week_classes',$big_week_classes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('big_week_class.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $week_class = new BigWeekClass;

        $week_class->title = $request->input('title');
        $week_class->url = $request->input('url');
        $week_class->due_date = $request->input('due_date');
        $week_class->createEmbedLink($request->input('url'));

        $week_class->save();

        return redirect()->route('big_week_class.index')->with('success','Aulão semanal salva');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $big_week_class = BigWeekClass::find($id);

        return View('big_week_class.edit')->with('big_week_class',$big_week_class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $week_class = BigWeekClass::find($id);
        $week_class->title = $request->input('title');
        $week_class->url = $request->input('url');
        $week_class->due_date = $request->input('due_date');
        $week_class->createEmbedLink($week_class->url);

        $week_class->save();

        return redirect()->route('big_week_class.index')->with('success','Aulão semanal atualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BigWeekClass::destroy($id);
        return redirect()->route('big_week_class.index')->with('success','Aulão semanal removido');
    }
}
