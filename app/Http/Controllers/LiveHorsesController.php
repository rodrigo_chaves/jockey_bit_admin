<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\LiveHorse;
use Validator;

class LiveHorsesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $livehorses = LiveHorse::paginate(20);

        return View('livehorses.index')->with('livehorses',$livehorses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('livehorses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'name' => 'required'
        ]);

        if($validation->fails()){
            return redirect()->back()->with('error',$validation->errors()->all()[0]);
        }
        else{
            $horse = new LiveHorse;
            $horse->active = true;
            $horse->name = $request->input('name');
            $horse->save();

            return redirect()->route('livehorses.index')->with('success','Cavalo salvo');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $horse = LiveHorse::find($id);

        return View('livehorses.edit')->with('horse',$horse);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $horse = LiveHorse::find($id);
        $horse->name = $request->input('name');
        $horse->save();

        return redirect()->route('livehorses.index')->with('success','Cavalo salvo');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        LiveHorse::destroy($id);
        return redirect()->route('horses.index')->with('success','Cavalo removido');
    }
}
