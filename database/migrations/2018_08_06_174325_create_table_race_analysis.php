<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRaceAnalysis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('race_analysis',function(Blueprint $table){
            $table->increments('id');
            $table->integer('racing_track_id')->unsigned();
            $table->foreign('racing_track_id')->references('id')->on('racing_tracks');
            $table->integer('distance_id')->unsigned()->nullable();
            $table->foreign('distance_id')->references('id')->on('distances');
            $table->integer('horse_id')->unsigned()->nullable();
            $table->foreign('horse_id')->references('id')->on('horses');
            $table->integer('livehorse_id')->unsigned()->nullable();
            $table->foreign('livehorse_id')->references('id')->on('live_horses');
            $table->string('classe')->nullable();
            $table->string('odd')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('race_analysis');
    }
}
