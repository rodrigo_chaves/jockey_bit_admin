<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\Log;

class Chat
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Log::info($request->ip());
        header('Access-Control-Allow-Origin: *');
        return $next($request);
    }
}
