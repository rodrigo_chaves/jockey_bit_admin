@extends('layout.panel')

@section('content')
<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h2>Editar Video de Tips Star</h2>
          </div>
          <div class="box-divider m-0"></div>
          <div class="box-body">
            <form action="{{route('startipvideos.update',['startipvideo' => $video->id])}}" method="post">
                {{ csrf_field() }}

                <input type="hidden" name="_method" value="put">
              
              <div class="checkbox">
                <label class="ui-check ui-check-md">
                  <input type="checkbox" name="active" id="active" {{$video->active? 'checked' : ''}}> <i class="dark-white"></i> Ativo
                </label>
              </div>
              
              <div class="form-group">
                <label>Titulo</label>
                <input type="text" name="title" class="form-control" placeholder="" value="{{$video->title}}">
              </div>

              <div class="form-group">
                <label>Msg</label>
                <input type="text" name="msg" class="form-control" placeholder="msg" value="{{$video->msg}}">
              </div>

              <div class="form-group">
                <label>Video</label>
                <input type="text" name="url" class="form-control" placeholder="https://vimeo.com/29885705" value="{{$video->url}}"">
              </div>
            
              <button type="submit" class="btn primary">Salvar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
