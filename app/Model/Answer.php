<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = 'module_questions_answers';

    public function question(){
        return $this->belongsTo('App\Model\Question');
    }

    public function scopeByQuestion($query, $question){
        return $query->where('module_question_id',$question);
    }
}
