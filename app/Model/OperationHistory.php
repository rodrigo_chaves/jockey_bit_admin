<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OperationHistory extends Model
{
    protected $table = 'operations_history';

    /** scopes */
    public function scopeActive($query){
        return $query->where('active',true)->orderBy('created_at','DESC');
    }

    public function createEmbedLink($url){
        if($this->isVimeo($url)){
        	$parts = explode('vimeo.com/',$url);
            // $this->embed_url = 'https://player.vimeo.com/video/' . $parts[count($parts)-1] . '?autoplay=1';
            $this->embed_url = 'https://player.vimeo.com/video/' . $parts[count($parts)-1];
        }
        else if($this->isYoutube()){
        	$parts = explode('?v=',$url);
            // $this->embed_url = 'https://www.youtube.com/embed/' . $parts[count($parts)-1] . '?rel=0';
            $this->embed_url = 'https://www.youtube.com/embed/' . $parts[count($parts)-1];
        }
    }

    public function isVimeo($url){
			return strpos($url,'vimeo.com/');
    }

    public function isYoutube($url){
    	return strpos($url,'?v=', $url);
    }
}
