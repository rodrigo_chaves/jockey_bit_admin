<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOperations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ead_historic_tips',function(Blueprint $table){
            $table->increments('id');
            $table->string('horse');
            $table->datetime('due_date');
            $table->string('run');
            $table->decimal('odd',3,2);
            $table->string('result');
            $table->boolean('win_lose');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ead_historic_tips');
    }
}
