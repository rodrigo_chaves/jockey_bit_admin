<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RacingTrack extends Model
{
    protected $table = 'racing_tracks';
}
