@extends('layout.panel')

@section('content')

<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Plataforma do Professor</a></li>
          <li class="breadcrumb-item active">Tickets</li>
          </ol>
        <table id="" class="table v-middle p-0 m-0 box dataTable no-footer">
          <thead>
            <tr>
                <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">#</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Usuario</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Titulo</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Status</td>
              <td align="center" style="font-weight: 900; border-bottom: 1px solid #11111124;">Ações</td>
            </tr>
          </thead>
          <tbody>
            @foreach($tickets as $ticket)
              <tr>
                  <td>{{$ticket->id}}</td>
                  <td align="center">{{$ticket->user->name}}</td>
                  <td align="center">{{$ticket->title}}</td>
                  <td align="center">{{$ticket->status->title}}</td>
                  <td align="right">
                    <!-- <form action="{{route('week_class.destroy',['id'=>$ticket->id])}}" method="post">
                      {{csrf_field()}}

                      <input type="hidden" name="_method" value="delete"> -->
                      @if($ticket->mayHide())
                      <a href="{{route('hide_ticket',['id'=>$ticket->id])}}" class="btn btn-warning" title="Ocultar ticket"><i class="fa fa-eye-slash"></i></a>
                      @endif

                      <a href="{{route('tickets.show',['id'=>$ticket->id])}}" class="btn btn-primary"><i class="fa fa-comments"></i></a>

                      <!-- <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>

                    </form> -->
                  </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-lg-12">
        {{$tickets->links()}}
      </div>
    </div>
  </div>
  @include('layout.elements.footer')
</div>
@endsection
