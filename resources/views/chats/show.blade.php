@extends('layout.panel')

@section('content')
<style>
  .chat-list{
    max-height: 65vh;
    overflow-y: auto;
  }
  .chat-content{
    padding: 6px 7px 8px 9px !important;
    font-size: 14px !important;
    border-radius: 20px !important;
  }
</style>
<link rel="stylesheet" href="/css/chat.css">

<div class="content-main " id="chat">
    <div class="d-flex flex-column flex" id="chat-list">
        <div class="scrollable hover">
            <div class="navbar flex-nowrap white lt box-shadow">
                <a data-toggle="modal" data-target="#content-aside" data-modal="" class="mr-1 d-md-none">
                  <span class="btn btn-sm btn-icon primary"><i class="fa fa-th"></i> 
                  </span>
                </a>
                <span class="text-md text-ellipsis flex">{{$chat->name}}</span>
                <!-- <ul class="nav nav-xs flex-row no-border"><li class="nav-item"><a class="nav-link" data-toggle="modal" data-target="#profile" data-modal=""><span class="btn btn-sm btn-icon btn-rounded success"><i class="fa fa-user"></i></span></a></li><li class="nav-item"><a class="nav-link" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Phone Call"><span class="btn btn-sm btn-icon btn-rounded white"><i class="fa fa-phone"></i></span></a></li><li class="nav-item"><a class="nav-link" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Face Call"><span class="btn btn-sm btn-icon btn-rounded white"><i class="fa fa-video-camera"></i></span></a></li></ul> -->
            </div>
          <div class="p-3">
            <div class="chat-list">
                <message :messages="messages" v-on:delete_message="deleteMessage" v-on:block_user="blockUser" :user="{{Session::get('admin')}}"></message>
            </div>
          </div>
        </div>
        <sent-message v-on:messagesent="addMessage" :user="{{Session::get('admin')}}"></sent-message>
      </div>
      
</div>

<meta name="csrf-token" content="{{ csrf_token() }}">

<input type="hidden" id="pusher_key" value="{{env('PUSHER_APP_KEY')}}">
<input type="hidden" id="pusher_cluster" value="{{env('PUSHER_CLUSTER')}}">
<input type="hidden" id="pusher_channel" value="{{env('CHAT_CHANNEL')}}">

@endsection

@section('js')
<script src="https://js.pusher.com/4.3/pusher.min.js"></script>
<!-- <script src="/js/bootstrap.js"></script> -->
<script src="/js/manifest.js"></script>
<script src="/js/vendor.js"></script>
<script src="/js/chat.js"></script>
@endsection
