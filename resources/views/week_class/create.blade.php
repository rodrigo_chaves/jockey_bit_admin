@extends('layout.panel')

@section('content')
<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h2>Nova Aula semanal</h2>
          </div>
          <div class="box-divider m-0"></div>
          <div class="box-body">
            <form action="{{route('week_class.store')}}" method="post">
                {{ csrf_field() }}
              <div class="form-group">
                <label>Titulo</label>
                <input type="text" class="form-control" name="title" id="title" placeholder="Titulo">
              </div>

              <div class="form-group">
                <label>Url</label>
                <input type="text" class="form-control" name="url" id="url" placeholder="url">
              </div>

              <div class="form-group">
                <label>Data</label>
                <input type="text" class="form-control" name="due_date" id="due_date" placeholder="Data, Ex: YYYY-MM-DD">
              </div>
              
              <button type="submit" class="btn primary">Salvar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.13/jquery.mask.js"></script>
<script type="text/javascript">
  $(function () {
      $('#due_date').mask('0000-00-00');
  });
</script>

@endsection
