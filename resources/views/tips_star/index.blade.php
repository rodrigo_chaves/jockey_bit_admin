@extends('layout.panel')

@section('content')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.min.css">

<style>
  table thead tr td {
    font-weight: 900 !important; border-bottom: 1px solid #11111124 !important;
  }
</style>
<div class="content-main " id="content-main">
  <div class="padding">
    <div class="row">
      <div class="col-lg-12">
        @if(Session::has('success'))
          <div class="alert green">
            <b>{{Session::get('success')}}</b>
          </div>
        @endif
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-sm-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/">Plataforma do Professor</a></li>
          <li class="breadcrumb-item active">Tip Stars</li>
        </ol>
        <div class="box">
          <div class="box-header">
              <h3>Tips Star Info</h3>
          </div>
          <div class="box-body">
            <form  action="{{route('systeminfo.update')}}" method="post" id="system_info" data-key="tipstar">
              {{csrf_field()}}
              <div class="row">
                <div class="col-lg-11">
                  <div class="form-group">
                    <input type="hidden" name="key" value="">
                    <textarea class="form-control" name="val" placeholder="Info"></textarea>
                  </div>
                </div>
                <div class="col-lg-1">
                  <button class="btn btn-success btn-circle">
                    <i class="fas fa-check"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>

          <div class="box">
            <div class="box-header">
              <h3>Tips Star</h3>
            </div>
            <div class="box-tool p-0" style="top: 5px !important;">
                <a href="{{route('tipstar.create')}}" class="btn btn-circle primary m-0"><i class="fa fa-plus"></i></a>
            </div>
            <div class="box-body p-0">
                <table id="" class="table v-middle p-0 m-0 box dataTable no-footer">
                    <thead>
                      <tr>
                        <th>Data</th>
                        <th>Hora</th>
                        <th>Corrida</th>
                        <th align="center" width="60px">Img</th>
                        <th align="center">Cavalo</th>
                        <th align="center">Nível de confiança</th>
                        <th>Resultado</th>
                        <th>Video</th>
                        <th align="center" width="70px">Ações</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($tips as $tip)
                        <tr>
                            
                            <td>{{$tip->date}}</td>
                            <td>{{$tip->hour}}</td>
                            <td>{{$tip->run}}</td>
                            <td><img src="{{$tip->img}}" class="img-fluid img-thumbnail"></td>
                            <td align="center">{{$tip->horse}}</td>
                            <td align="center">
                              @for($i=0; $i < 5; $i++)
                                @if ($i >= $tip->trust_level)
                                  <span class="fa fa-star text-muted"></span>
                                @else
                                  <span class="fa fa-star text-success"></span>
                                @endif
                              @endfor
                            </td>
                            <td>{{$tip->result}}</td>
                            <td>
                              @if($tip->url)
                                <a data-toggle="modal" data-target="#{{$tip->id}}">
                                  <div class="callout callout-info" style=" background: #fff; border: none; border-radius: 3px;">
                                    <img src="/img/animations/video-icon.gif" style="width:59px; float: left; margin: 2px 10px 0px -5px">
                                  </div>
                                </a>
                              
                                <div id="{{$tip->id}}" class="modal fade" role="dialog">
                                  <div class="modal-dialog modal-lg">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      </div>
                                      <div class="modal-body">
                                        <div class='embed-container'>
                                          <iframe src='{{$tip->embed_url}}' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              @endif
                            </td>
                            <td align="center">
                              <form action="{{route('tipstar.destroy',['id'=>$tip->id])}}" method="post">
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="delete">
                                <a href="{{route('tipstar.edit',['tipstar' => $tip->id])}}" class="btn btn-primary btn-circle"><i class="fas fa-pencil-alt"></i></a>
                                <button type="submit" class="btn btn-danger btn-circle"><i class="fa fa-times"></i></button>
                              </form>
                            </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
            </div>
          </div> 
        
      </div>
    </div>
    <div class="row align-items-stretch">
      <div class="col-lg-12">
        {{$tips->links()}}
      </div>
    </div>
  </div>
</div>

@include('layout.elements.footer')
@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.all.min.js"></script>

<script>
    $(document).ready(function(){
      var key = $('form#system_info').data('key');
      $.get('/systeminfo/' + key, function(data){
        
        if(data.success){
          $('input[name=key]').val(data.data.key);
          $('textarea[name=val]').val(data.data.val);
        }
      });

      $('form#system_info').submit(function(evt){
        evt.preventDefault();
        console.log('salvando');
        $.ajax({
          'url' : $('form#system_info').attr('action'),
          'type' : 'PUT',
          'data' : $('form#system_info').serialize()
        })
        .done(function(result){
          console.log(result);
          if(result.success){
            Swal(
              'Informativo salvo!',
              '',
              'success'
            )
          }
        });
      });

    });
</script>
@endsection
