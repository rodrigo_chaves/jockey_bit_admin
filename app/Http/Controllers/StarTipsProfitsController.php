<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\StarTipProfit;
use Validator;

class StarTipsProfitsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profits = StartipProfit::latest()->paginate(15);

        return View('startipsprofits.index')->with('profits',$profits);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('startipsprofits.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'profit' => 'required',
            'due_date' => 'required'
        ]);

        if($validation->fails()){
            return back()->with('error', $validation->errors()->all()[0]);
        }
        else{
            $dailytip_profit = new StarTipProfit;
            $dailytip_profit->due_date = $request->input('due_date');
            $dailytip_profit->profit = $request->input('profit');
            $dailytip_profit->accumulated_profit = $request->has('accumulated_profit') ? $request->input('accumulated_profit') : null;
            $dailytip_profit->sent_tips = $request->has('sent_tips')? $request->input('sent_tips') : 0;
            $dailytip_profit->winner_tips = $request->has('winner_tips')? $request->input('winner_tips') : 0;
            $dailytip_profit->loose_tips = $request->has('loose_tips')? $request->input('loose_tips') : 0;

            $dailytip_profit->save();

            return redirect()->route('startipsprofits.index')->with('success','Lucro de star tip salvo');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $startipprofit = StarTipProfit::find($id);

        return View('startipsprofits.edit')->with('profit', $startipprofit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(),[
            'profit' => 'required',
            'due_date' => 'required'
        ]);

        if($validation->fails()){
            return back()->with('error', $validation->errors()->all()[0]);
        }
        else{
            $dailytip_profit = StarTipProfit::find($id);
            $dailytip_profit->due_date = $request->input('due_date');
            $dailytip_profit->profit = $request->input('profit');
            $dailytip_profit->accumulated_profit = $request->input('accumulated_profit');
            $dailytip_profit->sent_tips = $request->has('sent_tips')? $request->input('sent_tips') : 0;
            $dailytip_profit->winner_tips = $request->has('winner_tips')? $request->input('winner_tips') : 0;
            $dailytip_profit->loose_tips = $request->has('loose_tips')? $request->input('loose_tips') : 0;
            $dailytip_profit->save();

            return redirect()->route('startipsprofits.index')->with('success','Lucro de tip salvo');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        StarTipProfit::destroy($id);
        return redirect()->route('startipsprofits.index')->with('success','Lucro de star tip removido');
    }
}
